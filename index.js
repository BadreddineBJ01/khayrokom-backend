// FileName: index.js
// Import express
let express = require("express");
// Import Body parser
let bodyParser = require("body-parser");

// usage of passport

// Import Mongoose
let mongoose = require("mongoose");
// Initialize the app
let app = express();
// Setup server port
var port = process.env.PORT || 5001;

var path = require('path');


var cors = require('cors')
// Configure bodyparser to handle post requests
app.use(
  bodyParser.urlencoded({
    extended: true
  })
);
app.use(bodyParser.json());

app.use(cors())


//app.use(passport.session());

// database connection
let DBconfig = require("./config/DBConfig.json");
mongoose.connect(DBconfig.database, { useNewUrlParser: true, useUnifiedTopology:true });
mongoose.set('useCreateIndex', true)
var db = mongoose.connection;
// Added check for DB connection
if (!db) console.log("Error connecting db");
else console.log("Db connected successfully");


// Send message for default URL
require('./Routes/routes')(app);

const fileDirectory = path.resolve(__dirname, "./public")

app.use(express.static(fileDirectory));

app.get("*", (req, res) => {
  res.sendFile("index.html", { root: fileDirectory }, (err) => {
    res.end();

    if (err) throw err;
  });
});
// Launch app to listen to specified port
app.listen(port, function() {
  console.log("Running Khayrokom app backend on port " + port);
});
