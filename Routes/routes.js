var express = require('express');
var app = express();
var bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}));
var auth  = require('../services/auth');
var passport = require('passport');
require('../config/passport')(passport);


module.exports= function(app){

    // institut routes 
    var institute_ctrl = require('../controllers/instituteController')
    app.get('/institute',passport.authenticate('jwt', { session: false}) ,institute_ctrl.getAllInstitutes)
       .post('/institute',institute_ctrl.addInstitute);

    app.get('/institute/:id',institute_ctrl.getInstituteById)
       .put('/institute/:id',institute_ctrl.updateInstitute)
       .delete('/institute/:id',institute_ctrl.deleteInstitute);
      
    // students routes
    var students_ctrl = require('../controllers/studentController')
    app.get('/students/halaqa/:id',students_ctrl.getAllHalaqastudents)
      .get('/student/class/:halaqaId/:classId', students_ctrl.getStudentsByClass)
       .post('/student',passport.authenticate('jwt', { session: false}), students_ctrl.addStudent);

    app.get('/student/:id',students_ctrl.getStudentById)
       .put('/student/:id',students_ctrl.updateStudent)
       .delete('/student/:id',students_ctrl.deleteStudent);
    
    // achivements routes
    var achivement_ctrl = require('../controllers/achievementController')
    app.get('/achivement/student/:id',achivement_ctrl.getAllStudentAchievement)
       .get('/achivement/student/:id/filter', achivement_ctrl.getAchivementByprogram)
       .post('/achivement',achivement_ctrl.addAchievement);

    app.get('/achivement/:id',achivement_ctrl.getAchievementById)
       .put('/achivement/:id',achivement_ctrl.updateAchievement)
       .delete('/achivement/:id',achivement_ctrl.deleteAchievement);
    
   // class routes
   var class_ctrl = require('../controllers/classeController')
   app.get('/class',class_ctrl.getAllClasses)
      .get('/classes/institut/:id',class_ctrl.getAllInstitutClassesById)
      .post('/class',class_ctrl.addClasse);

   app.get('/class/:id',class_ctrl.getClassbyId)
      .put('/class/:id',class_ctrl.updateClasse)
      .delete('/class/:id',class_ctrl.deleteclasse);
  

   // halaqa routes
   var halaqa_ctrl = require('../controllers/halaqaController')
   app.get('/halaqa',halaqa_ctrl.getAllHalaqat)
      .get('/halaqats/:InstitutId/:CheikhId',halaqa_ctrl.getAllInstitutAndCheickHalaqatById)
      .post('/halaqa',halaqa_ctrl.addHalaqa);

   app.get('/halaqa/:id',halaqa_ctrl.gethalaqabyId)
      .put('/halaqa/:id',halaqa_ctrl.updateHalaqa)
      .delete('/halaqa/:id',halaqa_ctrl.deleteHalaqa)
 /// 
   // users routes
   var users_ctrl = require('../controllers/UsersController')
   app.get('/users', users_ctrl.getAllusers)
      .post('/users',users_ctrl.addUser)
      .post('/users/req-reset-password', users_ctrl.ResetPassword)
      .post('/users/new-password', users_ctrl.NewPassword)
      .post('/users/valid-password-token', users_ctrl.ValidPasswordToken);
      
   app.get('/users/:id',users_ctrl.getUserById)
      .put('/users/:id',users_ctrl.updateUser)
      .delete('/users/:id',users_ctrl.deleteUser);   

   app.post('/users/login', users_ctrl.loginUser);
   app.post('/users/register', users_ctrl.registerUser);
   
   app.get('/globalSearch',passport.authenticate('jwt', { session: false}) ,institute_ctrl.search);  
}
