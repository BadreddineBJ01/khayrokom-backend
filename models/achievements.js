var mongoose=require('mongoose');
var achievementSchema = new mongoose.Schema({
  program: String,
  surah: String,
  fromVerse: Number,
  toVerse: Number,
  evaluation: String, // change to enum if needed
  notes: String,
  achievDate: {type: Date, default: new Date()},
  idstudent: { type: String, required: true },
  created_at: Date
});
    achievementSchema.pre('save', function (next) {
        var achievement = this;
        // get the current date
        var currentDate = new Date();
        // if created_at doesn't exist, add to that field
        if (!achievement.created_at) {
            achievement.created_at = currentDate;
        }
        next();
});
module.exports=mongoose.model('Achievements',achievementSchema);