var mongoose = require("mongoose");

var classSchema = new mongoose.Schema({
    Classname: {type: String, required: true},
    institutId: {type: String, required: true},
    created_at: Date
});


classSchema.pre("save", function(next) {
    var ClassObject = this;
    // get the current date
    var currentDate = new Date();
    // if created_at doesn't exist, add to that field
    if (!ClassObject.created_at) {
        ClassObject.created_at = currentDate;
    }
    next();
  });

  // search
  classSchema.index({
    Classname:'text'
  });

  
  module.exports = mongoose.model("Classes", classSchema);
  