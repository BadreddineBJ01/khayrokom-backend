var mongoose = require("mongoose");

var studentSchema = new mongoose.Schema({
  firstname: {
    type: String,
    required: true
  },
  lastname: {
    type: String,
    required: true
  },
  gender: String, // change to enum
  status: String, // change to enum
  roomNumber: Number,
  level: String,
  entryDate: {type:Date,default: new Date()},
  exitDate:  {type: Date, default: new Date()},
  created_at: Date,
  halaqaId:{type:String},
  classId: {type:String},
  createdBy: {type: String}
});
studentSchema.pre("save", function(next) {
  var student = this;
  // get the current date
  var currentDate = new Date();
  // if created_at doesn't exist, add to that field
  if (!student.created_at) {
    student.created_at = currentDate;    
  }
  next();
});

studentSchema.index({
  firstname: 'text',
  lastname: 'text'
});


module.exports = mongoose.model("Students", studentSchema);
