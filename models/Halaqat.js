var mongoose = require("mongoose");

var HalaqaSchema = new mongoose.Schema({
    Halaqaname: {type: String, required: true},
    InstitutId: {type: String, required: true},
    CheikhId: {type: String, required: true},
    created_at: Date,
    created_by: {type: String}
});


HalaqaSchema.pre("save", function(next) {
    var halaqa = this;
    // get the current date
    var currentDate = new Date();
    // if created_at doesn't exist, add to that field
    if (!halaqa.created_at) {
        halaqa.created_at = currentDate;
    }
    next();
  });

  // search
  HalaqaSchema.index({
    Halaqaname: 'text'
  });


  module.exports = mongoose.model("Halaqat", HalaqaSchema);
  