var mongoose = require("mongoose");
var instituteSchema = new mongoose.Schema({
  InstituteName: { type: String, required: true},
  country: { type: String},
  city: { type: String},
  created_at: Date
});

instituteSchema.pre("save", function(next) {
  var institute = this;
  // get the current date
  var currentDate = new Date();
  // if created_at doesn't exist, add to that field
  if (!institute.created_at) {
    institute.created_at = currentDate;
  }
  next();
});

// search
instituteSchema.index({
  InstituteName: 'text',
  country: 'text',
  city: 'text',
});


module.exports = mongoose.model("Institutes", instituteSchema);
