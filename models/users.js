var mongoose = require("mongoose");
var bcrypt = require('bcryptjs');
var UserSchema = new mongoose.Schema({
  firstname: {
    type: String,
    required: true
  },
  lastname: {
    type: String,
    required: true
  },
  email: {
    type: String,
    unique: true,
    required: true,
    lowercase: true,
    trim: true
  },
  phone: {
    type: String,
    required: false
  },
  password: {
    type: String,
    required: true
  },
  institutesids: [{type: String}],
  role : {type: String},
  created_at: {
    type: Date
  }
});

UserSchema.pre("save", function(next) {
  var user = this;
  // get the current date
  var currentDate = new Date();
  // if created_at doesn't exist, add to that field
  if (!user.created_at) {
    user.created_at = currentDate;
  }

  if (!user.role) {
    user.role = 'guest';
  }


  if (!user.isModified("password")) return next();
  bcrypt.genSalt(10, function(err, salt) {
    if (err) return next(err);
    if(user.password)
    {
       console.log('user password', user.password);
      bcrypt.hash(user.password,10,function(err, hash) {
        if (err) return next(err);
        user.password = hash;
        console.log('hashed password', user.password);
        next();
      });
    }  
  });

});

UserSchema.methods.comparePassword = function(candidatePassword, cb) {
  bcrypt.compare(candidatePassword, this.password, (err, isMatch) => {
    if (err) return cb(err);
    cb(null, isMatch);
  });
};

module.exports = mongoose.model("Users", UserSchema);
