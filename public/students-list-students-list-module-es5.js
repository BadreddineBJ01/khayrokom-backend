function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["students-list-students-list-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/students/students-list/students-list.page.html":
  /*!******************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/students/students-list/students-list.page.html ***!
    \******************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppStudentsStudentsListStudentsListPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\r\n  <ion-toolbar>\r\n    <ion-title>Students list</ion-title>\r\n    <ion-buttons slot=\"start\">\r\n      <ion-menu-button autoHide=\"false\"></ion-menu-button>\r\n      <ion-back-button></ion-back-button>\r\n    </ion-buttons>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content padding>\r\n  <ion-button\r\n    shape=\"round\"\r\n    *ngIf=\"connectedUserRole == 'cheick' || connectedUserRole == 'admin'\" \r\n    expand=\"block\"\r\n    (click)='goToAddStudent()'\r\n    routerDirection=\"root\">\r\n    <ion-icon slot=\"icon-only\" name=\"add\"></ion-icon>\r\n    Add student\r\n  </ion-button>\r\n  <div>\r\n  <form [formGroup]=\"StudentFilterForm\">\r\n       <ion-select \r\n       formControlName=\"Relatedclass\" \r\n       placeholder=\"Filter by class\" \r\n       (ionChange)=\"SelectClass($event)\" required>\r\n       <ion-select-option\r\n       *ngFor=\"let class of classList\"\r\n       value=\"{{class._id}}\">{{class.Classname}}</ion-select-option>\r\n        <ion-select-option value=\"All\">All</ion-select-option>\r\n      </ion-select>\r\n    </form>\r\n  </div>    \r\n  <ion-card *ngIf=\"!nodataFound\">\r\n  <ion-item-sliding *ngFor=\"let student of studentsList\">\r\n    <ion-item (click)=\"AddStudentAchievement(student._id)\">\r\n      <ion-label>\r\n        {{student.firstname}} {{student.lastname}}\r\n      </ion-label>\r\n    </ion-item>\r\n    <ion-item-options>\r\n      <ion-item-option \r\n       color=\"primary\" (click)=\"Studentdetails(student._id)\">\r\n        <ion-icon slot=\"top\" name=\"more\"></ion-icon>\r\n        details\r\n      </ion-item-option>\r\n      <ion-item-option \r\n        color=\"secondary\"\r\n        *ngIf=\"connectedUserRole == 'cheick' || connectedUserRole == 'admin'\"  \r\n        (click)=\"editStudent(student._id)\">\r\n        <ion-icon slot=\"top\" name=\"create\"></ion-icon>\r\n        edit\r\n      </ion-item-option>\r\n      <ion-item-option \r\n        color=\"success\"\r\n        *ngIf=\"connectedUserRole == 'cheick' || connectedUserRole == 'admin'\"  \r\n        (click)=\"createStudentAccount(student._id)\">\r\n        <ion-icon slot=\"top\" name=\"person\"></ion-icon>\r\n        account\r\n      </ion-item-option>\r\n      <ion-item-option\r\n        *ngIf=\"connectedUserRole == 'cheick' || connectedUserRole == 'admin'\" \r\n        color=\"danger\"\r\n        (click)=\"presentConfirmDelete(student._id)\">\r\n        <ion-icon slot=\"top\" name=\"trash\"></ion-icon>\r\n        trash\r\n      </ion-item-option>\r\n    </ion-item-options>\r\n  </ion-item-sliding>\r\n  </ion-card>\r\n  <ion-card *ngIf=\"nodataFound\">\r\n  <img src=\"../../../assets/images/iconfinder_stack_1287510.svg\">\r\n  <ion-text color=\"primary\" style=\"text-align: center;\"><h1>Empty student list</h1></ion-text>\r\n  </ion-card>\r\n</ion-content>\r\n";
    /***/
  },

  /***/
  "./src/app/students/students-list/students-list-routing.module.ts":
  /*!************************************************************************!*\
    !*** ./src/app/students/students-list/students-list-routing.module.ts ***!
    \************************************************************************/

  /*! exports provided: StudentsListPageRoutingModule */

  /***/
  function srcAppStudentsStudentsListStudentsListRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "StudentsListPageRoutingModule", function () {
      return StudentsListPageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _students_list_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./students-list.page */
    "./src/app/students/students-list/students-list.page.ts");

    var routes = [{
      path: '',
      component: _students_list_page__WEBPACK_IMPORTED_MODULE_3__["StudentsListPage"]
    }];

    var StudentsListPageRoutingModule = function StudentsListPageRoutingModule() {
      _classCallCheck(this, StudentsListPageRoutingModule);
    };

    StudentsListPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], StudentsListPageRoutingModule);
    /***/
  },

  /***/
  "./src/app/students/students-list/students-list.module.ts":
  /*!****************************************************************!*\
    !*** ./src/app/students/students-list/students-list.module.ts ***!
    \****************************************************************/

  /*! exports provided: StudentsListPageModule */

  /***/
  function srcAppStudentsStudentsListStudentsListModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "StudentsListPageModule", function () {
      return StudentsListPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _students_list_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./students-list-routing.module */
    "./src/app/students/students-list/students-list-routing.module.ts");
    /* harmony import */


    var _students_list_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./students-list.page */
    "./src/app/students/students-list/students-list.page.ts");
    /* harmony import */


    var src_app_shared_module_shared_module_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! src/app/shared-module/shared-module.module */
    "./src/app/shared-module/shared-module.module.ts");

    var StudentsListPageModule = function StudentsListPageModule() {
      _classCallCheck(this, StudentsListPageModule);
    };

    StudentsListPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], src_app_shared_module_shared_module_module__WEBPACK_IMPORTED_MODULE_7__["SharedModuleModule"], _students_list_routing_module__WEBPACK_IMPORTED_MODULE_5__["StudentsListPageRoutingModule"]],
      declarations: [_students_list_page__WEBPACK_IMPORTED_MODULE_6__["StudentsListPage"]]
    })], StudentsListPageModule);
    /***/
  },

  /***/
  "./src/app/students/students-list/students-list.page.scss":
  /*!****************************************************************!*\
    !*** ./src/app/students/students-list/students-list.page.scss ***!
    \****************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppStudentsStudentsListStudentsListPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3N0dWRlbnRzL3N0dWRlbnRzLWxpc3Qvc3R1ZGVudHMtbGlzdC5wYWdlLnNjc3MifQ== */";
    /***/
  },

  /***/
  "./src/app/students/students-list/students-list.page.ts":
  /*!**************************************************************!*\
    !*** ./src/app/students/students-list/students-list.page.ts ***!
    \**************************************************************/

  /*! exports provided: StudentsListPage */

  /***/
  function srcAppStudentsStudentsListStudentsListPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "StudentsListPage", function () {
      return StudentsListPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var src_app_services_Students_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! src/app/services/Students.service */
    "./src/app/services/Students.service.ts");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var src_app_services_communication_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! src/app/services/communication.service */
    "./src/app/services/communication.service.ts");
    /* harmony import */


    var src_app_services_users_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! src/app/services/users.service */
    "./src/app/services/users.service.ts");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var src_app_services_classes_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! src/app/services/classes.service */
    "./src/app/services/classes.service.ts");
    /* harmony import */


    var src_app_services_search_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! src/app/services/search.service */
    "./src/app/services/search.service.ts");

    var StudentsListPage = /*#__PURE__*/function () {
      function StudentsListPage(studentsService, router, userService, activatedRoute, alertCtrl, fb, searchService, classService, modalController, communicationService, loadingController) {
        _classCallCheck(this, StudentsListPage);

        this.studentsService = studentsService;
        this.router = router;
        this.userService = userService;
        this.activatedRoute = activatedRoute;
        this.alertCtrl = alertCtrl;
        this.fb = fb;
        this.searchService = searchService;
        this.classService = classService;
        this.modalController = modalController;
        this.communicationService = communicationService;
        this.loadingController = loadingController;
        this.classList = [];
        this.nodataFound = false;
      }

      _createClass(StudentsListPage, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          var _this = this;

          this.InstitutId = this.communicationService.InstitutId;
          console.log('institutId', this.InstitutId);
          this.activatedRoute.params.subscribe(function (params) {
            if (params !== {} && params.id !== undefined) {
              _this.HalaqaId = _this.communicationService.HalaqaId = params.id;
            }
          });
          this.router.events.subscribe(function (res) {
            if (res instanceof _angular_router__WEBPACK_IMPORTED_MODULE_3__["NavigationEnd"]) {
              _this.getAllHalaqaStudents();

              _this.setUserRole();
            }
          });
          this.initForm();
          this.getAllInstitutClassesById();
        }
      }, {
        key: "initForm",
        value: function initForm() {
          this.StudentFilterForm = this.fb.group({
            Relatedclass: ['']
          });
        }
      }, {
        key: "SelectClass",
        value: function SelectClass(event) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
            var _this2 = this;

            var loading, classId;
            return regeneratorRuntime.wrap(function _callee$(_context) {
              while (1) {
                switch (_context.prev = _context.next) {
                  case 0:
                    _context.next = 2;
                    return this.loadingController.create({
                      message: 'Loading...'
                    });

                  case 2:
                    loading = _context.sent;
                    _context.next = 5;
                    return loading.present();

                  case 5:
                    console.log('event ---->', event);
                    classId = event.detail.value;

                    if (classId != 'All') {
                      this.searchService.StudentByClass(this.HalaqaId, classId).subscribe(function (res) {
                        if (res.code == "0000") {
                          _this2.studentsList = res.content;

                          if (_this2.studentsList.length == 0) {
                            _this2.nodataFound = true;
                          } else {
                            _this2.nodataFound = false;
                          }

                          loading.dismiss();
                        } else {
                          console.log('error');
                          loading.dismiss();
                        }
                      }, function (err) {
                        console.log('error');
                        loading.dismiss();
                      });
                    } else if (classId === 'All') {
                      this.getAllHalaqaStudents();
                      loading.dismiss();
                    } else {
                      console.log('error');
                      loading.dismiss();
                    }

                  case 8:
                  case "end":
                    return _context.stop();
                }
              }
            }, _callee, this);
          }));
        }
      }, {
        key: "getAllInstitutClassesById",
        value: function getAllInstitutClassesById() {
          var _this3 = this;

          this.classService.getAllInstitutClassesById(this.InstitutId).subscribe(function (data) {
            if (data.code === '0000') {
              _this3.classList = data.content;
              console.log("class list --> ", _this3.classList);
            } else {
              console.log('error');
            }
          }, function (err) {
            console.log('subscription error', err);
          });
        }
      }, {
        key: "createStudentAccount",
        value: function createStudentAccount(studentId) {
          this.router.navigate(['/students/account/' + studentId]);
        }
      }, {
        key: "setUserRole",
        value: function setUserRole() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
            return regeneratorRuntime.wrap(function _callee2$(_context2) {
              while (1) {
                switch (_context2.prev = _context2.next) {
                  case 0:
                    _context2.next = 2;
                    return this.userService.getUserRole();

                  case 2:
                    this.connectedUserRole = _context2.sent;

                  case 3:
                  case "end":
                    return _context2.stop();
                }
              }
            }, _callee2, this);
          }));
        }
      }, {
        key: "goToAddStudent",
        value: function goToAddStudent() {
          this.router.navigate(['/students/add-student/' + this.HalaqaId]);
        }
      }, {
        key: "AddStudentAchievement",
        value: function AddStudentAchievement(studentId) {
          this.router.navigate(['/achievements/achivement-list/' + studentId]);
        }
      }, {
        key: "getAllHalaqaStudents",
        value: function getAllHalaqaStudents() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
            var _this4 = this;

            return regeneratorRuntime.wrap(function _callee3$(_context3) {
              while (1) {
                switch (_context3.prev = _context3.next) {
                  case 0:
                    this.studentsService.getAllHalqaStudents(this.HalaqaId).subscribe(function (data) {
                      if (data.code === '0000') {
                        _this4.studentsList = data.content;

                        if (_this4.studentsList.length == 0) {
                          _this4.nodataFound = true;
                        } else {
                          _this4.nodataFound = false;
                        }
                      } else {}
                    }, function (err) {
                      console.log('subscription error', err);
                    });

                  case 1:
                  case "end":
                    return _context3.stop();
                }
              }
            }, _callee3, this);
          }));
        }
      }, {
        key: "editStudent",
        value: function editStudent(studentId) {
          this.router.navigate(['students/edit-student/' + studentId]);
        }
      }, {
        key: "delete",
        value: function _delete(studentId) {
          this.studentsService.deleteStudent(studentId).subscribe(function (data) {
            if (data.code === '0000') {
              console.log('delete data', data);
            } else {
              console.log('error');
            }
          }, function (err) {
            console.log('error');
          });
        }
      }, {
        key: "Studentdetails",
        value: function Studentdetails(studentId) {
          this.router.navigate(['students/student-details/' + studentId]);
        }
      }, {
        key: "presentConfirmDelete",
        value: function presentConfirmDelete(studentId) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee4() {
            var _this5 = this;

            var alert;
            return regeneratorRuntime.wrap(function _callee4$(_context4) {
              while (1) {
                switch (_context4.prev = _context4.next) {
                  case 0:
                    alert = this.alertCtrl.create({
                      message: 'Do you want to delet that student',
                      buttons: [{
                        text: 'Cancel',
                        role: 'cancel',
                        handler: function handler() {
                          console.log('Cancel clicked');
                        }
                      }, {
                        text: 'Confirm',
                        handler: function handler() {
                          _this5["delete"](studentId);

                          _this5.ReloadList();
                        }
                      }]
                    });
                    _context4.next = 3;
                    return alert;

                  case 3:
                    _context4.sent.present();

                  case 4:
                  case "end":
                    return _context4.stop();
                }
              }
            }, _callee4, this);
          }));
        }
      }, {
        key: "ReloadList",
        value: function ReloadList() {
          this.getAllHalaqaStudents();
        }
      }]);

      return StudentsListPage;
    }();

    StudentsListPage.ctorParameters = function () {
      return [{
        type: src_app_services_Students_service__WEBPACK_IMPORTED_MODULE_2__["StudentsService"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]
      }, {
        type: src_app_services_users_service__WEBPACK_IMPORTED_MODULE_6__["UserService"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"]
      }, {
        type: _angular_forms__WEBPACK_IMPORTED_MODULE_7__["FormBuilder"]
      }, {
        type: src_app_services_search_service__WEBPACK_IMPORTED_MODULE_9__["SearchService"]
      }, {
        type: src_app_services_classes_service__WEBPACK_IMPORTED_MODULE_8__["ClassesService"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"]
      }, {
        type: src_app_services_communication_service__WEBPACK_IMPORTED_MODULE_5__["CommunicationService"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"]
      }];
    };

    StudentsListPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-students-list',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./students-list.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/students/students-list/students-list.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./students-list.page.scss */
      "./src/app/students/students-list/students-list.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_Students_service__WEBPACK_IMPORTED_MODULE_2__["StudentsService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], src_app_services_users_service__WEBPACK_IMPORTED_MODULE_6__["UserService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"], _angular_forms__WEBPACK_IMPORTED_MODULE_7__["FormBuilder"], src_app_services_search_service__WEBPACK_IMPORTED_MODULE_9__["SearchService"], src_app_services_classes_service__WEBPACK_IMPORTED_MODULE_8__["ClassesService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"], src_app_services_communication_service__WEBPACK_IMPORTED_MODULE_5__["CommunicationService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"]])], StudentsListPage);
    /***/
  }
}]);
//# sourceMappingURL=students-list-students-list-module-es5.js.map