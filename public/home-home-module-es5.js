function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["home-home-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/home/home.page.html":
  /*!***************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/home/home.page.html ***!
    \***************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppHomeHomePageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\r\n    <ion-toolbar>\r\n      <ion-title>Home</ion-title>\r\n      <ion-buttons slot=\"start\">\r\n        <ion-menu-button autoHide=\"false\"></ion-menu-button>\r\n      </ion-buttons>\r\n    </ion-toolbar>\r\n  </ion-header>\r\n\r\n<ion-content>\r\n  <div>\r\n    <ion-searchbar  \r\n        showCancelButton=\"true\"\r\n         id=\"searchBar\" \r\n        (keyup)=\"onSearch($event)\">\r\n      </ion-searchbar>\r\n\r\n        <ion-item-group *ngIf=\"!nodataFound\">\r\n          <ion-item-divider  \r\n          *ngIf=\"HalqaList.length!=0\" \r\n          color=\"medium\">Halaqats</ion-item-divider>\r\n                <ion-card *ngFor=\"let halaqa of HalqaList\">\r\n          <ion-item  style=\"cursor: pointer;\"\r\n          (click)=\"GoToHalqa(halaqa._id)\">\r\n              <ion-label>\r\n                  {{ halaqa.Halaqaname }}\r\n                </ion-label>\r\n          </ion-item>     \r\n            </ion-card>\r\n\r\n  <ion-item-divider \r\n  *ngIf=\"InstitutesList.length!=0\" \r\n  color=\"medium\">Institutes</ion-item-divider>\r\n\r\n        <ion-card *ngFor=\"let Institut of InstitutesList\">\r\n  <ion-item (click)=\"InstitutRedirection(Institut._id)\">\r\n  <ion-label>\r\n    {{ Institut.InstituteName }} {{ getinstitutName(Institut) }} {{ Institut.city }}\r\n  </ion-label>\r\n  </ion-item>\r\n        </ion-card>\r\n\r\n<!-- <ion-item-divider \r\n*ngIf=\"ClassesList.length!=0 \" \r\ncolor=\"medium\">Classes</ion-item-divider>\r\n\r\n      <ion-card *ngFor=\"let class of ClassesList\">\r\n<ion-item (click)=\"GoToClass(class._id)\">\r\n<ion-label>\r\n  {{ class.Classname }}\r\n</ion-label>\r\n</ion-item>\r\n</ion-card> -->\r\n\r\n<ion-item-divider \r\n*ngIf=\"StudentsList.length!=0\" \r\ncolor=\"medium\">Students</ion-item-divider>\r\n\r\n      <ion-card *ngFor=\"let student of StudentsList\">\r\n<ion-item  (click)=\"GoToStudent(student._id)\">\r\n<ion-label>\r\n  {{ student.firstname }} {{ student.lastname }}\r\n</ion-label>\r\n</ion-item>\r\n</ion-card>\r\n        </ion-item-group>\r\n    </div>\r\n    <img *ngIf=\"nodataFound\" src=\"../../assets/images/homeLogo.png\" style=\"vertical-align: center;width:100% ; height:100%\"> \r\n</ion-content>\r\n\r\n";
    /***/
  },

  /***/
  "./src/app/home/home.module.ts":
  /*!*************************************!*\
    !*** ./src/app/home/home.module.ts ***!
    \*************************************/

  /*! exports provided: HomePageModule */

  /***/
  function srcAppHomeHomeModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "HomePageModule", function () {
      return HomePageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _home_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./home.page */
    "./src/app/home/home.page.ts");

    var HomePageModule = function HomePageModule() {
      _classCallCheck(this, HomePageModule);
    };

    HomePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _angular_router__WEBPACK_IMPORTED_MODULE_5__["RouterModule"].forChild([{
        path: '',
        component: _home_page__WEBPACK_IMPORTED_MODULE_6__["HomePage"]
      }])],
      declarations: [_home_page__WEBPACK_IMPORTED_MODULE_6__["HomePage"]]
    })], HomePageModule);
    /***/
  },

  /***/
  "./src/app/home/home.page.scss":
  /*!*************************************!*\
    !*** ./src/app/home/home.page.scss ***!
    \*************************************/

  /*! exports provided: default */

  /***/
  function srcAppHomeHomePageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".welcome-card {\n  max-height: 35vh;\n  overflow: hidden;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvaG9tZS9DOlxcVXNlcnNcXHVpZDEwMjNcXERlc2t0b3BcXGFwcFxca2hheXJva29tLWZyb250ZW5kL3NyY1xcYXBwXFxob21lXFxob21lLnBhZ2Uuc2NzcyIsInNyYy9hcHAvaG9tZS9ob21lLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGdCQUFBO0VBQ0EsZ0JBQUE7QUNDRiIsImZpbGUiOiJzcmMvYXBwL2hvbWUvaG9tZS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIud2VsY29tZS1jYXJke1xyXG4gIG1heC1oZWlnaHQ6IDM1dmg7XHJcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcclxufVxyXG5cclxuXHJcbiIsIi53ZWxjb21lLWNhcmQge1xuICBtYXgtaGVpZ2h0OiAzNXZoO1xuICBvdmVyZmxvdzogaGlkZGVuO1xufSJdfQ== */";
    /***/
  },

  /***/
  "./src/app/home/home.page.ts":
  /*!***********************************!*\
    !*** ./src/app/home/home.page.ts ***!
    \***********************************/

  /*! exports provided: HomePage */

  /***/
  function srcAppHomeHomePageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "HomePage", function () {
      return HomePage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _services_search_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../services/search.service */
    "./src/app/services/search.service.ts");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _services_communication_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ../services/communication.service */
    "./src/app/services/communication.service.ts");
    /* harmony import */


    var _services_users_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ../services/users.service */
    "./src/app/services/users.service.ts");
    /* harmony import */


    var _ionic_storage__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! @ionic/storage */
    "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
    /* harmony import */


    var _institutes_models_Countries__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! ../institutes/models/Countries */
    "./src/app/institutes/models/Countries.ts");

    var HomePage = /*#__PURE__*/function () {
      function HomePage(menuCtrl, communicationService, SearchService, storage, userService, router) {
        _classCallCheck(this, HomePage);

        this.menuCtrl = menuCtrl;
        this.communicationService = communicationService;
        this.SearchService = SearchService;
        this.storage = storage;
        this.userService = userService;
        this.router = router;
        this.HalqaList = [];
        this.InstitutesList = [];
        this.ClassesList = [];
        this.StudentsList = [];
        this.nodataFound = true;
        this.countryArray = _institutes_models_Countries__WEBPACK_IMPORTED_MODULE_8__["countries"];
        this.menuCtrl.enable(true, 'first');
      }

      _createClass(HomePage, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          this.getDataStorage();
        }
      }, {
        key: "onSearch",
        value: function onSearch(event) {
          var _this = this;

          console.log('event', event.target.value);

          if (event.target.value === '') {
            this.HalqaList.length = 0;
            this.InstitutesList.length = 0;
            this.ClassesList.length = 0;
            this.StudentsList.length = 0;
            this.nodataFound = true;
          } else {
            this.nodataFound = false;
            this.SearchService.Search(event.target.value).subscribe(function (data) {
              if (data[0]) {
                _this.HalqaList = data[0];
              }

              if (data[1]) {
                _this.InstitutesList = data[1];
              }

              if (data[2]) {
                _this.ClassesList = data[2];
              }

              if (data[3]) {
                _this.StudentsList = data[3];
              }
            });
          }
        }
      }, {
        key: "getinstitutName",
        value: function getinstitutName(institut) {
          var country = _institutes_models_Countries__WEBPACK_IMPORTED_MODULE_8__["countries"].find(function (el) {
            return el.id.toString() == institut.country;
          });

          return country ? country.name : '';
        }
      }, {
        key: "getDataStorage",
        value: function getDataStorage() {
          this.storage.get('ACCESSTOKEN').then(function (data) {});
        }
      }, {
        key: "GoToHalqa",
        value: function GoToHalqa(halaqaId) {
          this.router.navigate(['/students/students-list/' + halaqaId]);
        }
      }, {
        key: "GoToInstitut",
        value: function GoToInstitut(institutId) {
          this.router.navigate(['/institutes/institut-details/' + institutId]);
        }
      }, {
        key: "GoToClass",
        value: function GoToClass(classID) {
          this.router.navigate(['/classes/class-details/' + classID]);
        }
      }, {
        key: "GoToStudent",
        value: function GoToStudent(studentID) {
          this.router.navigate(['/achievements/achivement-list/' + studentID]);
        }
      }, {
        key: "InstitutRedirection",
        value: function InstitutRedirection(institutId) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
            var userRole;
            return regeneratorRuntime.wrap(function _callee$(_context) {
              while (1) {
                switch (_context.prev = _context.next) {
                  case 0:
                    this.communicationService.InstitutId = institutId;
                    _context.next = 3;
                    return this.userService.getUserRole();

                  case 3:
                    userRole = _context.sent;
                    _context.t0 = userRole;
                    _context.next = _context.t0 === 'admin' ? 7 : _context.t0 === 'cheick' ? 9 : 11;
                    break;

                  case 7:
                    this.router.navigate(['classes/classe-list/' + institutId]);
                    return _context.abrupt("break", 12);

                  case 9:
                    this.router.navigate(['/halaqat/halaqat-list/' + institutId]);
                    return _context.abrupt("break", 12);

                  case 11:
                    console.log('user role is not setted');

                  case 12:
                  case "end":
                    return _context.stop();
                }
              }
            }, _callee, this);
          }));
        }
      }]);

      return HomePage;
    }();

    HomePage.ctorParameters = function () {
      return [{
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["MenuController"]
      }, {
        type: _services_communication_service__WEBPACK_IMPORTED_MODULE_5__["CommunicationService"]
      }, {
        type: _services_search_service__WEBPACK_IMPORTED_MODULE_3__["SearchService"]
      }, {
        type: _ionic_storage__WEBPACK_IMPORTED_MODULE_7__["Storage"]
      }, {
        type: _services_users_service__WEBPACK_IMPORTED_MODULE_6__["UserService"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]
      }];
    };

    HomePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-home',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./home.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/home/home.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./home.page.scss */
      "./src/app/home/home.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["MenuController"], _services_communication_service__WEBPACK_IMPORTED_MODULE_5__["CommunicationService"], _services_search_service__WEBPACK_IMPORTED_MODULE_3__["SearchService"], _ionic_storage__WEBPACK_IMPORTED_MODULE_7__["Storage"], _services_users_service__WEBPACK_IMPORTED_MODULE_6__["UserService"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]])], HomePage);
    /***/
  }
}]);
//# sourceMappingURL=home-home-module-es5.js.map