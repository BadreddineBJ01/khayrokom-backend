(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["add-user-add-user-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/users/add-user/add-user.page.html":
/*!*****************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/users/add-user/add-user.page.html ***!
  \*****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\r\n  <ion-toolbar>\r\n    <ion-title *ngIf='!EditUserInfo'>Add User</ion-title>\r\n    <ion-title *ngIf='EditUserInfo'>Edit User</ion-title>\r\n    <ion-buttons slot=\"start\">\r\n    <ion-menu-button autoHide=\"false\"></ion-menu-button>\r\n    <ion-back-button></ion-back-button>\r\n  </ion-buttons>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <ion-card>\r\n  <form  [formGroup]=\"UsersForm\">\r\n    <ion-item lines=\"inset\">\r\n      <ion-input \r\n      formControlName=\"firstname\" \r\n      placeholder='first name' required></ion-input>\r\n    </ion-item>\r\n    <div>\r\n      <ng-container *ngFor=\"let error of error_messages.firstname\">\r\n        <ion-text class=\"ion-padding\" color=\"danger\" *ngIf=\"UsersForm.get('firstname').hasError(error.type) && (UsersForm.get('firstname').dirty || UsersForm.get('firstname').touched)\">\r\n          {{ error.message }}\r\n        </ion-text>\r\n      </ng-container>\r\n    </div>\r\n    <ion-item lines=\"inset\">\r\n      <ion-input formControlName=\"lastname\" placeholder='lastname' required>\r\n      </ion-input>\r\n    </ion-item>\r\n    <div>\r\n      <ng-container *ngFor=\"let error of error_messages.lastname\">\r\n        <ion-text class=\"ion-padding\" color=\"danger\" *ngIf=\"UsersForm.get('lastname').hasError(error.type) && (UsersForm.get('lastname').dirty || UsersForm.get('lastname').touched)\">\r\n          {{ error.message }}\r\n        </ion-text>\r\n      </ng-container>\r\n    </div>\r\n    <ion-item lines=\"inset\">\r\n      <ion-input formControlName=\"email\" placeholder='email'required>\r\n      </ion-input>\r\n    </ion-item>\r\n    <div>\r\n      <ng-container *ngFor=\"let error of error_messages.email\">\r\n        <ion-text class=\"ion-padding\" color=\"danger\" *ngIf=\"UsersForm.get('email').hasError(error.type) && (UsersForm.get('email').dirty || UsersForm.get('email').touched)\">\r\n          {{ error.message }}\r\n        </ion-text>\r\n      </ng-container>\r\n    </div>\r\n    <ion-item lines=\"inset\" \r\n      *ngIf='!EditUserInfo'>\r\n      <ion-input formControlName='password' placeholder='password' [type]=\"showPassword ? 'text' : 'password'\"></ion-input>\r\n      <ion-icon slot=\"end\" [name]=\"PasswordToggleIcon\" (click)=\"togglePassword()\"></ion-icon>\r\n    </ion-item>\r\n    <div>\r\n      <ng-container *ngFor=\"let error of error_messages.password\">\r\n        <ion-text class=\"ion-padding\" color=\"danger\" *ngIf=\"UsersForm.get('password').hasError(error.type) && (UsersForm.get('password').dirty || UsersForm.get('password').touched)\">\r\n          {{ error.message }}\r\n        </ion-text>\r\n      </ng-container>\r\n    </div>\r\n    <ion-item lines=\"inset\" \r\n      *ngIf='!EditUserInfo'>\r\n      <ion-input formControlName='confirmpassword' placeholder='confirmpassword' [type]=\"showPassword ? 'text' : 'password'\"></ion-input>\r\n      <ion-icon slot=\"end\" [name]=\"PasswordToggleIcon\" (click)=\"togglePassword()\"></ion-icon>\r\n    </ion-item>\r\n    <div>\r\n      <ng-container *ngFor=\"let error of error_messages.confirmpassword\">\r\n        <ion-text class=\"ion-padding\" color=\"danger\" *ngIf=\"UsersForm.get('confirmpassword').hasError(error.type) && (UsersForm.get('confirmpassword').dirty || UsersForm.get('confirmpassword').touched)\">\r\n          {{ error.message }}\r\n        </ion-text>\r\n      </ng-container>\r\n    </div>\r\n    <div  class=\"ion-padding\" \r\n          color=\"danger\" \r\n          *ngIf=\"!UsersForm.get('confirmpassword').errors && UsersForm.hasError('passwordNotMatch') && (UsersForm.get('confirmpassword').dirty || UsersForm.get('confirmpassword').touched)\">\r\n      Password and Confirm Password fields should match\r\n    </div>\r\n    <ion-item lines=\"inset\">\r\n      <ion-input formControlName=\"phone\" placeholder='phone'>\r\n      </ion-input>\r\n    </ion-item>\r\n    <ion-item lines=\"inset\">\r\n      <!-- <ion-input formControlName=\"role\" placeholder='role'>\r\n      </ion-input> -->\r\n      <ion-select  lines=\"inset\" formControlName=\"role\" placeholder=\"role\" required>\r\n        <ion-select-option value=\"admin\">admin</ion-select-option>\r\n        <ion-select-option value=\"cheick\">cheikh</ion-select-option>\r\n        <!-- <ion-select-option value=\"guest\">Exam</ion-select-option> -->\r\n      </ion-select>\r\n\r\n    </ion-item>\r\n      <ion-select lines=\"inset\"\r\n       multiple=\"true\" \r\n       cancelText=\"No\" \r\n       okText=\"Yes\"\r\n       formControlName=\"institutesids\" \r\n       placeholder='Institutes'>\r\n        <ion-select-option *ngFor=\"let Institut of listInstitutes\" value={{Institut._id}}>{{Institut.InstituteName}}</ion-select-option>\r\n      </ion-select>\r\n  </form>\r\n  <ion-button  \r\n  shape=\"round\"\r\n  expand=\"block\" \r\n  *ngIf='!EditUserInfo' \r\n  (click)='addUser(UsersForm)'\r\n   [disabled]='!UsersForm.valid'>submit</ion-button>\r\n  <ion-button  \r\n   shape=\"round\"\r\n   expand=\"block\"  \r\n  *ngIf='EditUserInfo' \r\n  (click)='UpdateUser(UsersForm)'>Update</ion-button>  \r\n</ion-card>\r\n</ion-content>\r\n\r\n");

/***/ }),

/***/ "./src/app/users/add-user/add-user-routing.module.ts":
/*!***********************************************************!*\
  !*** ./src/app/users/add-user/add-user-routing.module.ts ***!
  \***********************************************************/
/*! exports provided: AddUserPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddUserPageRoutingModule", function() { return AddUserPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _add_user_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./add-user.page */ "./src/app/users/add-user/add-user.page.ts");




const routes = [
    {
        path: '',
        component: _add_user_page__WEBPACK_IMPORTED_MODULE_3__["AddUserPage"]
    }
];
let AddUserPageRoutingModule = class AddUserPageRoutingModule {
};
AddUserPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], AddUserPageRoutingModule);



/***/ }),

/***/ "./src/app/users/add-user/add-user.module.ts":
/*!***************************************************!*\
  !*** ./src/app/users/add-user/add-user.module.ts ***!
  \***************************************************/
/*! exports provided: AddUserPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddUserPageModule", function() { return AddUserPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _add_user_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./add-user-routing.module */ "./src/app/users/add-user/add-user-routing.module.ts");
/* harmony import */ var _add_user_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./add-user.page */ "./src/app/users/add-user/add-user.page.ts");
/* harmony import */ var src_app_shared_module_shared_module_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/shared-module/shared-module.module */ "./src/app/shared-module/shared-module.module.ts");








let AddUserPageModule = class AddUserPageModule {
};
AddUserPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            src_app_shared_module_shared_module_module__WEBPACK_IMPORTED_MODULE_7__["SharedModuleModule"],
            _add_user_routing_module__WEBPACK_IMPORTED_MODULE_5__["AddUserPageRoutingModule"]
        ],
        declarations: [_add_user_page__WEBPACK_IMPORTED_MODULE_6__["AddUserPage"]]
    })
], AddUserPageModule);



/***/ }),

/***/ "./src/app/users/add-user/add-user.page.scss":
/*!***************************************************!*\
  !*** ./src/app/users/add-user/add-user.page.scss ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3VzZXJzL2FkZC11c2VyL2FkZC11c2VyLnBhZ2Uuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/users/add-user/add-user.page.ts":
/*!*************************************************!*\
  !*** ./src/app/users/add-user/add-user.page.ts ***!
  \*************************************************/
/*! exports provided: AddUserPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddUserPage", function() { return AddUserPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var src_app_services_users_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/users.service */ "./src/app/services/users.service.ts");
/* harmony import */ var _models_users_models__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../models/users.models */ "./src/app/users/models/users.models.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_app_services_institut_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/services/institut.service */ "./src/app/services/institut.service.ts");








let AddUserPage = class AddUserPage {
    constructor(formBuilder, router, userService, institutService, toastController, activatedRoute) {
        this.formBuilder = formBuilder;
        this.router = router;
        this.userService = userService;
        this.institutService = institutService;
        this.toastController = toastController;
        this.activatedRoute = activatedRoute;
        this.UserToEdit = new _models_users_models__WEBPACK_IMPORTED_MODULE_5__["UsersModel"]();
        this.EditUserInfo = false;
        this.listInstitutes = [];
        this.showPassword = false;
        this.showConfirmPassword = false;
        this.PasswordToggleIcon = 'eye';
        this.ConfirmPasswordToggleIcon = 'eye';
        // Login Forms
        this.error_messages = {
            firstname: [{ type: 'required', message: 'First Name is required.' }],
            lastname: [{ type: 'required', message: 'Last Name is required.' }],
            email: [
                { type: 'required', message: 'Email is required.' },
                { type: 'minlength', message: 'Email length.' },
                { type: 'maxlength', message: 'Email length.' },
                { type: 'required', message: 'please enter a valid email address.' },
            ],
            password: [
                { type: 'required', message: 'password is required.' },
                { type: 'minlength', message: 'password length.' },
                { type: 'maxlength', message: 'password length.' },
            ],
            confirmpassword: [
                { type: 'required', message: 'password is required.' },
                { type: 'minlength', message: 'password length.' },
                { type: 'maxlength', message: 'password length.' },
            ],
        };
    }
    ngOnInit() {
        this.getAllInstitutes();
        this.initForm();
        this.activatedRoute.params.subscribe((params) => {
            console.log('params', params);
            if (params !== {} && params.id !== undefined) {
                this.GetUserById(params.id);
                this.userId = params.id;
                this.EditUserInfo = true;
            }
            else {
                this.EditUserInfo = false;
                this.initForm();
            }
        });
        this.getConnectedUserRole();
    }
    getConnectedUserRole() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            this.connectedUserRole = yield this.userService.getUserRole();
        });
    }
    getAllInstitutes() {
        this.institutService.getAllInstitutes().subscribe((data) => {
            if (data.code === '0000') {
                console.log('data', data);
                this.listInstitutes = data.content;
            }
            else {
                console.log('error');
            }
        }, (err) => {
            console.log('subscription error', err);
        });
    }
    GetUserById(userId) {
        console.log('params --->', userId);
        this.userService.getUserById(userId).subscribe((data) => {
            if (data.code === '0000') {
                console.log('data content', data.content[0]);
                this.UserToEdit = new _models_users_models__WEBPACK_IMPORTED_MODULE_5__["UsersModel"](data.content[0]);
                console.log('this.InstitutToEdit', this.UserToEdit);
                this.initForm();
            }
            else {
                console.log('error', data);
            }
        }, (err) => {
            console.log('error', err);
        });
    }
    initForm() {
        this.UsersForm = this.formBuilder.group({
            firstname: [
                this.UserToEdit.firstname,
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            ],
            lastname: [
                this.UserToEdit.lastname,
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            ],
            email: [
                this.UserToEdit.email,
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required,
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(6),
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(30),
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$'),
                ]),
            ],
            phone: [this.UserToEdit.phone],
            role: [this.UserToEdit.role],
            institutesids: [this.UserToEdit.institutesids],
            password: [
                '',
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required,
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(6),
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(30),
                ]),
            ],
            confirmpassword: [
                '',
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required,
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(6),
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(30),
                ]),
            ],
        }, {
            validators: this.password.bind(this),
        });
    }
    password(formGroup) {
        const { value: password } = formGroup.get('password');
        const { value: confirmPassword } = formGroup.get('confirmpassword');
        return password === confirmPassword ? null : { passwordNotMatch: true };
    }
    addUser(UsersForm) {
        console.log('ngForm', UsersForm.value);
        const usertoSend = Object.assign({}, UsersForm.value);
        delete usertoSend.confirmpassword;
        console.log('usertosend', usertoSend);
        this.userService.addUser(UsersForm.value).subscribe((data) => {
            if (data.code === '0000') {
                this.presentToast(data.message, 'success');
                console.log('user added with success');
                this.GoBackToList();
            }
            else {
                this.presentToast(data.message, 'error');
                console.log('error');
            }
        }, (err) => {
            this.presentToast(err, 'error');
            console.log('error', err);
        });
    }
    UpdateUser(UsersForm) {
        console.log('users.value', UsersForm.value, this.userId);
        const usertoUpdate = Object.assign({}, UsersForm.value);
        delete usertoUpdate.confirmpassword;
        delete usertoUpdate.password;
        console.log('usertoUpdate', usertoUpdate);
        this.userService.updateUser(this.userId, usertoUpdate).subscribe((data) => {
            if (data.code === '0000') {
                this.presentToast(data.message, 'success');
                console.log('user updated with success');
                this.GoBackToList();
            }
            else {
                this.presentToast(data.message, 'error');
                console.log('error update');
            }
        }, (err) => {
            this.presentToast(err, 'error');
            console.log('error', err);
        });
    }
    presentToast(msg, messagetype) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const toast = yield this.toastController.create({
                message: msg,
                duration: 3000,
                position: 'bottom',
                color: messagetype === 'success' ? 'success' : 'danger',
            });
            toast.present();
        });
    }
    GoBackToList() {
        this.router.navigate(['/users/users-list']);
    }
    togglePassword() {
        this.showPassword = !this.showPassword;
        if (this.PasswordToggleIcon == 'eye') {
            this.PasswordToggleIcon = 'eye-off';
        }
        else {
            this.PasswordToggleIcon = 'eye';
        }
    }
};
AddUserPage.ctorParameters = () => [
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
    { type: src_app_services_users_service__WEBPACK_IMPORTED_MODULE_4__["UserService"] },
    { type: src_app_services_institut_service__WEBPACK_IMPORTED_MODULE_7__["InstitutService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["ToastController"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"] }
];
AddUserPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-add-user',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./add-user.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/users/add-user/add-user.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./add-user.page.scss */ "./src/app/users/add-user/add-user.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
        _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
        src_app_services_users_service__WEBPACK_IMPORTED_MODULE_4__["UserService"],
        src_app_services_institut_service__WEBPACK_IMPORTED_MODULE_7__["InstitutService"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["ToastController"],
        _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"]])
], AddUserPage);



/***/ }),

/***/ "./src/app/users/models/users.models.ts":
/*!**********************************************!*\
  !*** ./src/app/users/models/users.models.ts ***!
  \**********************************************/
/*! exports provided: UsersModel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UsersModel", function() { return UsersModel; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _sharedModels_sharedModel__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../sharedModels/sharedModel */ "./src/app/sharedModels/sharedModel.ts");


class UsersModel extends _sharedModels_sharedModel__WEBPACK_IMPORTED_MODULE_1__["sharedModel"] {
    constructor(user) {
        user = user || {};
        super(user);
        this.firstname = user.firstname || '';
        this.lastname = user.lastname || '';
        this.email = user.email || '';
        this.phone = user.phone || '';
        this.role = user.role || '';
        this.institutesids = user.institutesids || [];
    }
}


/***/ })

}]);
//# sourceMappingURL=add-user-add-user-module-es2015.js.map