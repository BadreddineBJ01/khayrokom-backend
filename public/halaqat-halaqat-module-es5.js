function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["halaqat-halaqat-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/halaqat/halaqat.page.html":
  /*!*********************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/halaqat/halaqat.page.html ***!
    \*********************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppHalaqatHalaqatPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\r\n  <ion-toolbar>\r\n    <ion-title>halaqat</ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n\r\n</ion-content>\r\n";
    /***/
  },

  /***/
  "./src/app/halaqat/halaqat-routing.module.ts":
  /*!***************************************************!*\
    !*** ./src/app/halaqat/halaqat-routing.module.ts ***!
    \***************************************************/

  /*! exports provided: HalaqatPageRoutingModule */

  /***/
  function srcAppHalaqatHalaqatRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "HalaqatPageRoutingModule", function () {
      return HalaqatPageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");

    var routes = [{
      path: '',
      redirectTo: 'halaqat-list',
      pathMatch: 'full'
    }, {
      path: 'halaqat-list/:id',
      loadChildren: function loadChildren() {
        return __webpack_require__.e(
        /*! import() | halaqat-list-halaqat-list-module */
        "halaqat-list-halaqat-list-module").then(__webpack_require__.bind(null,
        /*! ./halaqat-list/halaqat-list.module */
        "./src/app/halaqat/halaqat-list/halaqat-list.module.ts")).then(function (m) {
          return m.HalaqatListPageModule;
        });
      }
    }, {
      path: 'add-halaqa/:id',
      loadChildren: function loadChildren() {
        return Promise.all(
        /*! import() | add-halaqa-add-halaqa-module */
        [__webpack_require__.e("common"), __webpack_require__.e("add-halaqa-add-halaqa-module")]).then(__webpack_require__.bind(null,
        /*! ./add-halaqa/add-halaqa.module */
        "./src/app/halaqat/add-halaqa/add-halaqa.module.ts")).then(function (m) {
          return m.AddHalaqaPageModule;
        });
      }
    }, {
      path: 'halaqa-details/:id',
      loadChildren: function loadChildren() {
        return __webpack_require__.e(
        /*! import() | halaqa-details-halaqa-details-module */
        "halaqa-details-halaqa-details-module").then(__webpack_require__.bind(null,
        /*! ./halaqa-details/halaqa-details.module */
        "./src/app/halaqat/halaqa-details/halaqa-details.module.ts")).then(function (m) {
          return m.HalaqaDetailsPageModule;
        });
      }
    }, {
      path: 'edit-halaqa/:id',
      loadChildren: function loadChildren() {
        return Promise.all(
        /*! import() | add-halaqa-add-halaqa-module */
        [__webpack_require__.e("common"), __webpack_require__.e("add-halaqa-add-halaqa-module")]).then(__webpack_require__.bind(null,
        /*! ./add-halaqa/add-halaqa.module */
        "./src/app/halaqat/add-halaqa/add-halaqa.module.ts")).then(function (m) {
          return m.AddHalaqaPageModule;
        });
      }
    }, {
      path: '**',
      redirectTo: 'halaqat-list'
    }];

    var HalaqatPageRoutingModule = function HalaqatPageRoutingModule() {
      _classCallCheck(this, HalaqatPageRoutingModule);
    };

    HalaqatPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], HalaqatPageRoutingModule);
    /***/
  },

  /***/
  "./src/app/halaqat/halaqat.module.ts":
  /*!*******************************************!*\
    !*** ./src/app/halaqat/halaqat.module.ts ***!
    \*******************************************/

  /*! exports provided: HalaqatPageModule */

  /***/
  function srcAppHalaqatHalaqatModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "HalaqatPageModule", function () {
      return HalaqatPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _halaqat_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./halaqat-routing.module */
    "./src/app/halaqat/halaqat-routing.module.ts");
    /* harmony import */


    var _halaqat_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./halaqat.page */
    "./src/app/halaqat/halaqat.page.ts");

    var HalaqatPageModule = function HalaqatPageModule() {
      _classCallCheck(this, HalaqatPageModule);
    };

    HalaqatPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _halaqat_routing_module__WEBPACK_IMPORTED_MODULE_5__["HalaqatPageRoutingModule"]],
      declarations: [_halaqat_page__WEBPACK_IMPORTED_MODULE_6__["HalaqatPage"]]
    })], HalaqatPageModule);
    /***/
  },

  /***/
  "./src/app/halaqat/halaqat.page.scss":
  /*!*******************************************!*\
    !*** ./src/app/halaqat/halaqat.page.scss ***!
    \*******************************************/

  /*! exports provided: default */

  /***/
  function srcAppHalaqatHalaqatPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2hhbGFxYXQvaGFsYXFhdC5wYWdlLnNjc3MifQ== */";
    /***/
  },

  /***/
  "./src/app/halaqat/halaqat.page.ts":
  /*!*****************************************!*\
    !*** ./src/app/halaqat/halaqat.page.ts ***!
    \*****************************************/

  /*! exports provided: HalaqatPage */

  /***/
  function srcAppHalaqatHalaqatPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "HalaqatPage", function () {
      return HalaqatPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");

    var HalaqatPage = /*#__PURE__*/function () {
      function HalaqatPage() {
        _classCallCheck(this, HalaqatPage);
      }

      _createClass(HalaqatPage, [{
        key: "ngOnInit",
        value: function ngOnInit() {}
      }]);

      return HalaqatPage;
    }();

    HalaqatPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-halaqat',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./halaqat.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/halaqat/halaqat.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./halaqat.page.scss */
      "./src/app/halaqat/halaqat.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])], HalaqatPage);
    /***/
  }
}]);
//# sourceMappingURL=halaqat-halaqat-module-es5.js.map