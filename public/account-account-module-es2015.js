(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["account-account-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/students/account/account.page.html":
/*!******************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/students/account/account.page.html ***!
  \******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\r\n  <ion-toolbar>\r\n    <ion-title>Student account</ion-title>\r\n    <ion-buttons slot=\"start\">\r\n      <ion-back-button></ion-back-button>\r\n    </ion-buttons>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <ion-card>\r\n    <form  [formGroup]=\"StudentAccountForm\">\r\n            <ion-item lines=\"inset\">\r\n        <ion-input formControlName=\"email\" placeholder='email' required>\r\n        </ion-input>\r\n      </ion-item>\r\n      <div>\r\n        <ng-container *ngFor=\"let error of error_messages.email\">\r\n          <ion-text class=\"ion-padding\" color=\"danger\" *ngIf=\"StudentAccountForm.get('email').hasError(error.type) && (StudentAccountForm.get('email').dirty || StudentAccountForm.get('email').touched)\">\r\n            {{ error.message }}\r\n          </ion-text>\r\n        </ng-container>\r\n      </div>\r\n      <ion-item lines=\"inset\">\r\n        <ion-input formControlName=\"password\" type='password' placeholder='password' required>\r\n        </ion-input>\r\n      </ion-item>\r\n      <div>\r\n        <ng-container *ngFor=\"let error of error_messages.password\">\r\n          <ion-text class=\"ion-padding\" color=\"danger\" *ngIf=\"StudentAccountForm.get('password').hasError(error.type) && (StudentAccountForm.get('password').dirty || StudentAccountForm.get('password').touched)\">\r\n            {{ error.message }}\r\n          </ion-text>\r\n        </ng-container>\r\n      </div>\r\n      <ion-item lines=\"inset\">\r\n        <ion-input formControlName=\"confirmpassword\" type='password' placeholder='confirm password' required>\r\n        </ion-input>\r\n      </ion-item>\r\n      <div>\r\n        <ng-container *ngFor=\"let error of error_messages.confirmpassword\">\r\n          <ion-text class=\"ion-padding\" color=\"danger\" *ngIf=\"StudentAccountForm.get('confirmpassword').hasError(error.type) && (StudentAccountForm.get('confirmpassword').dirty || StudentAccountForm.get('confirmpassword').touched)\">\r\n            {{ error.message }}\r\n          </ion-text>\r\n        </ng-container>\r\n      </div>\r\n      <ion-text  class=\"ion-padding\" color=\"danger\" *ngIf=\"!StudentAccountForm.get('confirmpassword').errors && StudentAccountForm.hasError('passwordNotMatch') && (StudentAccountForm.get('confirmpassword').dirty || StudentAccountForm.get('confirmpassword').touched)\">\r\n        Password and Confirm Password fields should match\r\n      </ion-text>\r\n    </form>\r\n\r\n    <!---->\r\n    <ion-button  \r\n    shape=\"round\"\r\n    expand=\"block\"\r\n     >submit</ion-button>\r\n  </ion-card>\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/students/account/account-routing.module.ts":
/*!************************************************************!*\
  !*** ./src/app/students/account/account-routing.module.ts ***!
  \************************************************************/
/*! exports provided: AccountPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AccountPageRoutingModule", function() { return AccountPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _account_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./account.page */ "./src/app/students/account/account.page.ts");




const routes = [
    {
        path: '',
        component: _account_page__WEBPACK_IMPORTED_MODULE_3__["AccountPage"]
    }
];
let AccountPageRoutingModule = class AccountPageRoutingModule {
};
AccountPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], AccountPageRoutingModule);



/***/ }),

/***/ "./src/app/students/account/account.module.ts":
/*!****************************************************!*\
  !*** ./src/app/students/account/account.module.ts ***!
  \****************************************************/
/*! exports provided: AccountPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AccountPageModule", function() { return AccountPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _account_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./account-routing.module */ "./src/app/students/account/account-routing.module.ts");
/* harmony import */ var _account_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./account.page */ "./src/app/students/account/account.page.ts");
/* harmony import */ var src_app_shared_module_shared_module_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/shared-module/shared-module.module */ "./src/app/shared-module/shared-module.module.ts");








let AccountPageModule = class AccountPageModule {
};
AccountPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            src_app_shared_module_shared_module_module__WEBPACK_IMPORTED_MODULE_7__["SharedModuleModule"],
            _account_routing_module__WEBPACK_IMPORTED_MODULE_5__["AccountPageRoutingModule"]
        ],
        declarations: [_account_page__WEBPACK_IMPORTED_MODULE_6__["AccountPage"]]
    })
], AccountPageModule);



/***/ }),

/***/ "./src/app/students/account/account.page.scss":
/*!****************************************************!*\
  !*** ./src/app/students/account/account.page.scss ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3N0dWRlbnRzL2FjY291bnQvYWNjb3VudC5wYWdlLnNjc3MifQ== */");

/***/ }),

/***/ "./src/app/students/account/account.page.ts":
/*!**************************************************!*\
  !*** ./src/app/students/account/account.page.ts ***!
  \**************************************************/
/*! exports provided: AccountPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AccountPage", function() { return AccountPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");



let AccountPage = class AccountPage {
    constructor(formBuilder) {
        this.formBuilder = formBuilder;
        this.error_messages = {
            email: [
                { type: 'required', message: 'Email is required.' },
                { type: 'minlength', message: 'Email length.' },
                { type: 'maxlength', message: 'Email length.' },
                { type: 'required', message: 'please enter a valid email address.' }
            ],
            password: [
                { type: 'required', message: 'password is required.' },
                { type: 'minlength', message: 'password length.' },
                { type: 'maxlength', message: 'password length.' }
            ],
            confirmpassword: [
                { type: 'required', message: 'password is required.' },
                { type: 'minlength', message: 'password length.' },
                { type: 'maxlength', message: 'password length.' },
            ],
        };
    }
    ngOnInit() {
        // get the user Id from thr route.
        // get the user object.
        this.StudentAccountForm = this.InitRegisterForm();
    }
    InitRegisterForm() {
        return this.formBuilder.group({
            email: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required,
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(6),
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(30),
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
                ])],
            password: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required,
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(6),
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(30)
                ])],
            confirmpassword: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required,
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(6),
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(30)
                ])]
        }, {
            validators: this.password.bind(this)
        });
    }
    password(formGroup) {
        const { value: password } = formGroup.get('password');
        const { value: confirmPassword } = formGroup.get('confirmpassword');
        return password === confirmPassword ? null : { passwordNotMatch: true };
    }
    registerUser(UserToAdd) {
        // console.log(UserToAdd);
        // this.userService.registerUser(UserToAdd).subscribe(
        //   res => {
        //     if (res.code === '0000') {
        //       this.presentToast(res.message, 'success');
        //       this.resetForm();
        //       this.router.navigate(['/auth']);
        //       console.log('res 1', res);
        //     } else {
        //       this.presentToast(res.message, 'error');
        //       console.log('res 2', res);
        //     }
        //   },
        //   err => {
        //     console.log('error ---> ', err);
        //     this.presentToast(err, 'error');
        //   }
        // );
    }
};
AccountPage.ctorParameters = () => [
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] }
];
AccountPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-account',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./account.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/students/account/account.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./account.page.scss */ "./src/app/students/account/account.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"]])
], AccountPage);



/***/ })

}]);
//# sourceMappingURL=account-account-module-es2015.js.map