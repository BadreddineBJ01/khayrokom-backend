function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["user-details-user-details-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/users/user-details/user-details.page.html":
  /*!*************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/users/user-details/user-details.page.html ***!
    \*************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppUsersUserDetailsUserDetailsPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\r\n  <ion-toolbar>\r\n    <ion-title>user-details</ion-title>\r\n    <ion-buttons slot=\"start\">\r\n        <ion-menu-button autoHide=\"false\"></ion-menu-button>\r\n        <ion-back-button></ion-back-button>\r\n      </ion-buttons>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n\r\n  <ion-card *ngIf='userDetails'>\r\n    <ion-item>\r\n      <ion-label>First name</ion-label>\r\n      <ion-label *ngIf='userDetails.firstname; else noData'>{{userDetails.firstname}}</ion-label>\r\n    </ion-item>\r\n  \r\n    <ion-item>\r\n      <ion-label>Last name</ion-label>\r\n      <ion-label *ngIf='userDetails.lastname; else noData'>{{userDetails.lastname}}</ion-label>\r\n    </ion-item>\r\n  \r\n    <ion-item>\r\n      <ion-label>Email</ion-label>\r\n      <ion-label *ngIf='userDetails.email; else noData'>{{userDetails.email}}</ion-label>\r\n    </ion-item>\r\n\r\n    <ion-item>\r\n      <ion-label>Phone number</ion-label>\r\n      <ion-label *ngIf='userDetails.phone; else noData'>{{userDetails.phone}}</ion-label>\r\n    </ion-item>\r\n\r\n    <ion-item>\r\n      <ion-label>Role</ion-label>\r\n      <ion-label *ngIf='userDetails.role; else noData'>{{userDetails.role}}</ion-label>\r\n    </ion-item>\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n  </ion-card>\r\n<ng-template #noData>\r\n <ion-label>\r\n   N/A\r\n  </ion-label>\r\n</ng-template>\r\n</ion-content>\r\n";
    /***/
  },

  /***/
  "./src/app/users/user-details/user-details-routing.module.ts":
  /*!*******************************************************************!*\
    !*** ./src/app/users/user-details/user-details-routing.module.ts ***!
    \*******************************************************************/

  /*! exports provided: UserDetailsPageRoutingModule */

  /***/
  function srcAppUsersUserDetailsUserDetailsRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "UserDetailsPageRoutingModule", function () {
      return UserDetailsPageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _user_details_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./user-details.page */
    "./src/app/users/user-details/user-details.page.ts");

    var routes = [{
      path: '',
      component: _user_details_page__WEBPACK_IMPORTED_MODULE_3__["UserDetailsPage"]
    }];

    var UserDetailsPageRoutingModule = function UserDetailsPageRoutingModule() {
      _classCallCheck(this, UserDetailsPageRoutingModule);
    };

    UserDetailsPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], UserDetailsPageRoutingModule);
    /***/
  },

  /***/
  "./src/app/users/user-details/user-details.module.ts":
  /*!***********************************************************!*\
    !*** ./src/app/users/user-details/user-details.module.ts ***!
    \***********************************************************/

  /*! exports provided: UserDetailsPageModule */

  /***/
  function srcAppUsersUserDetailsUserDetailsModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "UserDetailsPageModule", function () {
      return UserDetailsPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _user_details_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./user-details-routing.module */
    "./src/app/users/user-details/user-details-routing.module.ts");
    /* harmony import */


    var _user_details_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./user-details.page */
    "./src/app/users/user-details/user-details.page.ts");

    var UserDetailsPageModule = function UserDetailsPageModule() {
      _classCallCheck(this, UserDetailsPageModule);
    };

    UserDetailsPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _user_details_routing_module__WEBPACK_IMPORTED_MODULE_5__["UserDetailsPageRoutingModule"]],
      declarations: [_user_details_page__WEBPACK_IMPORTED_MODULE_6__["UserDetailsPage"]]
    })], UserDetailsPageModule);
    /***/
  },

  /***/
  "./src/app/users/user-details/user-details.page.scss":
  /*!***********************************************************!*\
    !*** ./src/app/users/user-details/user-details.page.scss ***!
    \***********************************************************/

  /*! exports provided: default */

  /***/
  function srcAppUsersUserDetailsUserDetailsPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3VzZXJzL3VzZXItZGV0YWlscy91c2VyLWRldGFpbHMucGFnZS5zY3NzIn0= */";
    /***/
  },

  /***/
  "./src/app/users/user-details/user-details.page.ts":
  /*!*********************************************************!*\
    !*** ./src/app/users/user-details/user-details.page.ts ***!
    \*********************************************************/

  /*! exports provided: UserDetailsPage */

  /***/
  function srcAppUsersUserDetailsUserDetailsPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "UserDetailsPage", function () {
      return UserDetailsPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var src_app_services_users_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! src/app/services/users.service */
    "./src/app/services/users.service.ts");

    var UserDetailsPage = /*#__PURE__*/function () {
      function UserDetailsPage(activatedRoute, userService) {
        _classCallCheck(this, UserDetailsPage);

        this.activatedRoute = activatedRoute;
        this.userService = userService;
      }

      _createClass(UserDetailsPage, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          this.userId = this.activatedRoute.snapshot.paramMap.get('id');
          this.getUserById(this.userId);
        }
      }, {
        key: "getUserById",
        value: function getUserById(userId) {
          var _this = this;

          this.userService.getUserById(userId).subscribe(function (data) {
            if (data.code === '0000') {
              console.log(data.content);
              _this.userDetails = data.content[0];
            } else {
              console.log('error', data);
            }
          }, function (err) {
            console.log('error', err);
          });
        }
      }]);

      return UserDetailsPage;
    }();

    UserDetailsPage.ctorParameters = function () {
      return [{
        type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]
      }, {
        type: src_app_services_users_service__WEBPACK_IMPORTED_MODULE_3__["UserService"]
      }];
    };

    UserDetailsPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-user-details',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./user-details.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/users/user-details/user-details.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./user-details.page.scss */
      "./src/app/users/user-details/user-details.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"], src_app_services_users_service__WEBPACK_IMPORTED_MODULE_3__["UserService"]])], UserDetailsPage);
    /***/
  }
}]);
//# sourceMappingURL=user-details-user-details-module-es5.js.map