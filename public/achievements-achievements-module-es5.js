function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["achievements-achievements-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/achievements/achievements.page.html":
  /*!*******************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/achievements/achievements.page.html ***!
    \*******************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppAchievementsAchievementsPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\r\n  <ion-toolbar>\r\n    <ion-title>achievements</ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n\r\n</ion-content>\r\n";
    /***/
  },

  /***/
  "./src/app/achievements/achievements-routing.module.ts":
  /*!*************************************************************!*\
    !*** ./src/app/achievements/achievements-routing.module.ts ***!
    \*************************************************************/

  /*! exports provided: AchievementsPageRoutingModule */

  /***/
  function srcAppAchievementsAchievementsRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AchievementsPageRoutingModule", function () {
      return AchievementsPageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");

    var routes = [{
      path: '',
      redirectTo: 'achivement-list',
      pathMatch: 'full'
    }, {
      path: 'add-achivement/:id',
      loadChildren: function loadChildren() {
        return Promise.all(
        /*! import() | add-achivement-add-achivement-module */
        [__webpack_require__.e("common"), __webpack_require__.e("add-achivement-add-achivement-module")]).then(__webpack_require__.bind(null,
        /*! ./add-achivement/add-achivement.module */
        "./src/app/achievements/add-achivement/add-achivement.module.ts")).then(function (m) {
          return m.AddAchivementPageModule;
        });
      }
    }, {
      path: 'achivement-list/:id',
      loadChildren: function loadChildren() {
        return __webpack_require__.e(
        /*! import() | achivement-list-achivement-list-module */
        "achivement-list-achivement-list-module").then(__webpack_require__.bind(null,
        /*! ./achivement-list/achivement-list.module */
        "./src/app/achievements/achivement-list/achivement-list.module.ts")).then(function (m) {
          return m.AchivementListPageModule;
        });
      }
    }, {
      path: 'achivements-details/:id',
      loadChildren: function loadChildren() {
        return Promise.all(
        /*! import() | achivements-details-achivements-details-module */
        [__webpack_require__.e("common"), __webpack_require__.e("achivements-details-achivements-details-module")]).then(__webpack_require__.bind(null,
        /*! ./achivements-details/achivements-details.module */
        "./src/app/achievements/achivements-details/achivements-details.module.ts")).then(function (m) {
          return m.AchivementsDetailsPageModule;
        });
      }
    }, {
      path: 'edit-achivement/:id',
      loadChildren: function loadChildren() {
        return Promise.all(
        /*! import() | add-achivement-add-achivement-module */
        [__webpack_require__.e("common"), __webpack_require__.e("add-achivement-add-achivement-module")]).then(__webpack_require__.bind(null,
        /*! ./add-achivement/add-achivement.module */
        "./src/app/achievements/add-achivement/add-achivement.module.ts")).then(function (m) {
          return m.AddAchivementPageModule;
        });
      }
    }, {
      path: '**',
      redirectTo: 'achivement-list'
    }];

    var AchievementsPageRoutingModule = function AchievementsPageRoutingModule() {
      _classCallCheck(this, AchievementsPageRoutingModule);
    };

    AchievementsPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], AchievementsPageRoutingModule);
    /***/
  },

  /***/
  "./src/app/achievements/achievements.module.ts":
  /*!*****************************************************!*\
    !*** ./src/app/achievements/achievements.module.ts ***!
    \*****************************************************/

  /*! exports provided: AchievementsPageModule */

  /***/
  function srcAppAchievementsAchievementsModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AchievementsPageModule", function () {
      return AchievementsPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _achievements_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./achievements-routing.module */
    "./src/app/achievements/achievements-routing.module.ts");
    /* harmony import */


    var _achievements_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./achievements.page */
    "./src/app/achievements/achievements.page.ts");
    /* harmony import */


    var _shared_module_shared_module_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ../shared-module/shared-module.module */
    "./src/app/shared-module/shared-module.module.ts");

    var AchievementsPageModule = function AchievementsPageModule() {
      _classCallCheck(this, AchievementsPageModule);
    };

    AchievementsPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _shared_module_shared_module_module__WEBPACK_IMPORTED_MODULE_7__["SharedModuleModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _achievements_routing_module__WEBPACK_IMPORTED_MODULE_5__["AchievementsPageRoutingModule"]],
      declarations: [_achievements_page__WEBPACK_IMPORTED_MODULE_6__["AchievementsPage"]]
    })], AchievementsPageModule);
    /***/
  },

  /***/
  "./src/app/achievements/achievements.page.scss":
  /*!*****************************************************!*\
    !*** ./src/app/achievements/achievements.page.scss ***!
    \*****************************************************/

  /*! exports provided: default */

  /***/
  function srcAppAchievementsAchievementsPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FjaGlldmVtZW50cy9hY2hpZXZlbWVudHMucGFnZS5zY3NzIn0= */";
    /***/
  },

  /***/
  "./src/app/achievements/achievements.page.ts":
  /*!***************************************************!*\
    !*** ./src/app/achievements/achievements.page.ts ***!
    \***************************************************/

  /*! exports provided: AchievementsPage */

  /***/
  function srcAppAchievementsAchievementsPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AchievementsPage", function () {
      return AchievementsPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");

    var AchievementsPage = /*#__PURE__*/function () {
      function AchievementsPage() {
        _classCallCheck(this, AchievementsPage);
      }

      _createClass(AchievementsPage, [{
        key: "ngOnInit",
        value: function ngOnInit() {}
      }]);

      return AchievementsPage;
    }();

    AchievementsPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-achievements',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./achievements.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/achievements/achievements.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./achievements.page.scss */
      "./src/app/achievements/achievements.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])], AchievementsPage);
    /***/
  }
}]);
//# sourceMappingURL=achievements-achievements-module-es5.js.map