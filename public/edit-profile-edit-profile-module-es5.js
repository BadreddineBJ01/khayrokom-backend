function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _createSuper(Derived) { return function () { var Super = _getPrototypeOf(Derived), result; if (_isNativeReflectConstruct()) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (typeof call === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["edit-profile-edit-profile-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/profile/edit-profile/edit-profile.page.html":
  /*!***************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/profile/edit-profile/edit-profile.page.html ***!
    \***************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppProfileEditProfileEditProfilePageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\r\n    <ion-toolbar>\r\n      <ion-title>Edit User</ion-title>\r\n      <ion-buttons slot=\"start\">\r\n      <ion-menu-button autoHide=\"false\"></ion-menu-button>\r\n      <ion-back-button></ion-back-button>\r\n    </ion-buttons>\r\n    </ion-toolbar>\r\n  </ion-header>\r\n  \r\n  <ion-content>\r\n    <ion-card *ngIf=\"UsersForm\">\r\n     <form  [formGroup]=\"UsersForm\"> \r\n     <ion-item lines=\"inset\">\r\n        <ion-input  formControlName=\"firstname\" placeholder='First name' required>\r\n        </ion-input>\r\n      </ion-item>\r\n      <div>\r\n        <ng-container *ngFor=\"let error of error_messages.firstname\">\r\n          <ion-text class=\"ion-padding\" color=\"danger\" *ngIf=\"UsersForm.get('firstname').hasError(error.type) && (UsersForm.get('firstname').dirty || UsersForm.get('firstname').touched)\">\r\n            {{ error.message }}\r\n          </ion-text>\r\n        </ng-container>\r\n      </div>\r\n      <ion-item lines=\"inset\">\r\n        <ion-input formControlName=\"lastname\" placeholder='Lastname' required>\r\n        </ion-input>\r\n      </ion-item>\r\n      <div>\r\n        <ng-container *ngFor=\"let error of error_messages.lastname\">\r\n          <ion-text class=\"ion-padding\" color=\"danger\" *ngIf=\"UsersForm.get('lastname').hasError(error.type) && (UsersForm.get('lastname').dirty || UsersForm.get('lastname').touched)\">\r\n            {{ error.message }}\r\n          </ion-text>\r\n        </ng-container>\r\n      </div>\r\n      <ion-item lines=\"inset\">\r\n        <ion-input formControlName=\"email\" placeholder='Email'required>\r\n        </ion-input>\r\n      </ion-item>\r\n      <div>\r\n        <ng-container *ngFor=\"let error of error_messages.email\">\r\n          <ion-text class=\"ion-padding\" color=\"danger\" *ngIf=\"UsersForm.get('email').hasError(error.type) && (UsersForm.get('email').dirty || UsersForm.get('email').touched)\">\r\n            {{ error.message }}\r\n          </ion-text>\r\n        </ng-container>\r\n      </div>\r\n      <ion-item lines=\"inset\">\r\n          <ion-input formControlName=\"phone\" placeholder='Phone'>\r\n          </ion-input>\r\n        </ion-item>\r\n    </form> \r\n    <ion-button  \r\n    shape=\"round\"\r\n    expand=\"block\" \r\n    (click)='UpdateUser(UsersForm)'>submit</ion-button>\r\n  </ion-card>\r\n  </ion-content>\r\n  \r\n  ";
    /***/
  },

  /***/
  "./src/app/auth/models/user.model.ts":
  /*!*******************************************!*\
    !*** ./src/app/auth/models/user.model.ts ***!
    \*******************************************/

  /*! exports provided: UserModel */

  /***/
  function srcAppAuthModelsUserModelTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "UserModel", function () {
      return UserModel;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _sharedModels_sharedModel__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! ../../sharedModels/sharedModel */
    "./src/app/sharedModels/sharedModel.ts");

    var UserModel = /*#__PURE__*/function (_sharedModels_sharedM) {
      _inherits(UserModel, _sharedModels_sharedM);

      var _super = _createSuper(UserModel);

      function UserModel(user) {
        var _this;

        _classCallCheck(this, UserModel);

        user = user || {};
        _this = _super.call(this, user);
        _this.firstname = _this.firstname || '';
        _this.lastname = _this.lastname || '';
        _this.email = _this.email || '';
        _this.phone = _this.phone || '';
        return _this;
      }

      return UserModel;
    }(_sharedModels_sharedModel__WEBPACK_IMPORTED_MODULE_1__["sharedModel"]);
    /***/

  },

  /***/
  "./src/app/profile/edit-profile/edit-profile-routing.module.ts":
  /*!*********************************************************************!*\
    !*** ./src/app/profile/edit-profile/edit-profile-routing.module.ts ***!
    \*********************************************************************/

  /*! exports provided: EditProfilePageRoutingModule */

  /***/
  function srcAppProfileEditProfileEditProfileRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "EditProfilePageRoutingModule", function () {
      return EditProfilePageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _edit_profile_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./edit-profile.page */
    "./src/app/profile/edit-profile/edit-profile.page.ts");

    var routes = [{
      path: '',
      component: _edit_profile_page__WEBPACK_IMPORTED_MODULE_3__["EditProfilePage"]
    }];

    var EditProfilePageRoutingModule = function EditProfilePageRoutingModule() {
      _classCallCheck(this, EditProfilePageRoutingModule);
    };

    EditProfilePageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], EditProfilePageRoutingModule);
    /***/
  },

  /***/
  "./src/app/profile/edit-profile/edit-profile.module.ts":
  /*!*************************************************************!*\
    !*** ./src/app/profile/edit-profile/edit-profile.module.ts ***!
    \*************************************************************/

  /*! exports provided: EditProfilePageModule */

  /***/
  function srcAppProfileEditProfileEditProfileModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "EditProfilePageModule", function () {
      return EditProfilePageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _edit_profile_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./edit-profile-routing.module */
    "./src/app/profile/edit-profile/edit-profile-routing.module.ts");
    /* harmony import */


    var _edit_profile_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./edit-profile.page */
    "./src/app/profile/edit-profile/edit-profile.page.ts");
    /* harmony import */


    var src_app_shared_module_shared_module_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! src/app/shared-module/shared-module.module */
    "./src/app/shared-module/shared-module.module.ts");

    var EditProfilePageModule = function EditProfilePageModule() {
      _classCallCheck(this, EditProfilePageModule);
    };

    EditProfilePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], src_app_shared_module_shared_module_module__WEBPACK_IMPORTED_MODULE_7__["SharedModuleModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _edit_profile_routing_module__WEBPACK_IMPORTED_MODULE_5__["EditProfilePageRoutingModule"]],
      declarations: [_edit_profile_page__WEBPACK_IMPORTED_MODULE_6__["EditProfilePage"]]
    })], EditProfilePageModule);
    /***/
  },

  /***/
  "./src/app/profile/edit-profile/edit-profile.page.scss":
  /*!*************************************************************!*\
    !*** ./src/app/profile/edit-profile/edit-profile.page.scss ***!
    \*************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppProfileEditProfileEditProfilePageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3Byb2ZpbGUvZWRpdC1wcm9maWxlL2VkaXQtcHJvZmlsZS5wYWdlLnNjc3MifQ== */";
    /***/
  },

  /***/
  "./src/app/profile/edit-profile/edit-profile.page.ts":
  /*!***********************************************************!*\
    !*** ./src/app/profile/edit-profile/edit-profile.page.ts ***!
    \***********************************************************/

  /*! exports provided: EditProfilePage */

  /***/
  function srcAppProfileEditProfileEditProfilePageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "EditProfilePage", function () {
      return EditProfilePage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var src_app_services_users_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! src/app/services/users.service */
    "./src/app/services/users.service.ts");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var src_app_auth_models_user_model__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! src/app/auth/models/user.model */
    "./src/app/auth/models/user.model.ts");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");

    var EditProfilePage = /*#__PURE__*/function () {
      function EditProfilePage(router, formBuilder, toastController, userservice) {
        _classCallCheck(this, EditProfilePage);

        this.router = router;
        this.formBuilder = formBuilder;
        this.toastController = toastController;
        this.userservice = userservice;
        this.user = new src_app_auth_models_user_model__WEBPACK_IMPORTED_MODULE_4__["UserModel"](); // Change the new Value

        this.error_messages = {
          firstname: [{
            type: 'required',
            message: 'First name is required.'
          }],
          lastname: [{
            type: 'required',
            message: 'last name is required.'
          }],
          email: [{
            type: 'required',
            message: 'Email is required.'
          }, {
            type: 'minlength',
            message: 'Email length.'
          }, {
            type: 'maxlength',
            message: 'Email length.'
          }, {
            type: 'required',
            message: 'please enter a valid email address.'
          }]
        };
        this.setUserInfo();
      }

      _createClass(EditProfilePage, [{
        key: "ngOnInit",
        value: function ngOnInit() {}
      }, {
        key: "setUserInfo",
        value: function setUserInfo() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
            return regeneratorRuntime.wrap(function _callee$(_context) {
              while (1) {
                switch (_context.prev = _context.next) {
                  case 0:
                    this.reloadUserInfo();

                  case 1:
                  case "end":
                    return _context.stop();
                }
              }
            }, _callee, this);
          }));
        }
      }, {
        key: "initUserForm",
        value: function initUserForm(userInfo) {
          return this.formBuilder.group({
            firstname: [userInfo.firstname, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required],
            lastname: [userInfo.lastname, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required],
            email: [userInfo.email, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].minLength(6), _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].maxLength(30), _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')])],
            phone: [userInfo.phone]
          });
        }
      }, {
        key: "reloadUserInfo",
        value: function reloadUserInfo() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
            var _this2 = this;

            return regeneratorRuntime.wrap(function _callee2$(_context2) {
              while (1) {
                switch (_context2.prev = _context2.next) {
                  case 0:
                    _context2.next = 2;
                    return this.userservice.getConnectedUserId();

                  case 2:
                    this.userId = _context2.sent;
                    this.userservice.getUserById(this.userId).subscribe(function (res) {
                      _this2.userInfo = res.content[0];
                      _this2.UsersForm = _this2.initUserForm(_this2.userInfo);
                    });

                  case 4:
                  case "end":
                    return _context2.stop();
                }
              }
            }, _callee2, this);
          }));
        }
      }, {
        key: "UpdateUser",
        value: function UpdateUser(UsersForm) {
          var _this3 = this;

          this.userservice.updateUser(this.userId, UsersForm.value).subscribe(function (data) {
            if (data.code === '0000') {
              _this3.presentToast(data.message, 'success');

              _this3.GoBackToList();
            } else {
              _this3.presentToast(data.message, 'error');
            }
          }, function (err) {
            _this3.presentToast(err, 'error');

            console.log('error', err);
          });
        }
      }, {
        key: "presentToast",
        value: function presentToast(msg, messagetype) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
            var toast;
            return regeneratorRuntime.wrap(function _callee3$(_context3) {
              while (1) {
                switch (_context3.prev = _context3.next) {
                  case 0:
                    _context3.next = 2;
                    return this.toastController.create({
                      message: msg,
                      duration: 3000,
                      position: 'bottom',
                      color: messagetype === 'success' ? 'success' : 'danger'
                    });

                  case 2:
                    toast = _context3.sent;
                    toast.present();

                  case 4:
                  case "end":
                    return _context3.stop();
                }
              }
            }, _callee3, this);
          }));
        }
      }, {
        key: "GoBackToList",
        value: function GoBackToList() {
          this.router.navigate(['/profile']);
        }
      }]);

      return EditProfilePage;
    }();

    EditProfilePage.ctorParameters = function () {
      return [{
        type: _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"]
      }, {
        type: _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["ToastController"]
      }, {
        type: src_app_services_users_service__WEBPACK_IMPORTED_MODULE_2__["UserService"]
      }];
    };

    EditProfilePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-edit-profile',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./edit-profile.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/profile/edit-profile/edit-profile.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./edit-profile.page.scss */
      "./src/app/profile/edit-profile/edit-profile.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["ToastController"], src_app_services_users_service__WEBPACK_IMPORTED_MODULE_2__["UserService"]])], EditProfilePage);
    /***/
  }
}]);
//# sourceMappingURL=edit-profile-edit-profile-module-es5.js.map