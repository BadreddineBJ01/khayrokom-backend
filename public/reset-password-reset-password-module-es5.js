function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["reset-password-reset-password-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/auth/reset-password/reset-password.page.html":
  /*!****************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/auth/reset-password/reset-password.page.html ***!
    \****************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppAuthResetPasswordResetPasswordPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\r\n  <ion-toolbar>\r\n    <ion-title>Reset Password Link</ion-title>\r\n    <ion-buttons slot=\"start\">\r\n      <ion-back-button></ion-back-button>\r\n    </ion-buttons>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <ion-card >\r\n    <ion-title>Please enter your email </ion-title>\r\n    <form [formGroup]=\"EmailForm\" padding>\r\n      <ion-item lines=\"inset\">\r\n        <ion-input formControlName=\"email\" placeholder=\"Email\"></ion-input>\r\n      </ion-item>\r\n      <div>\r\n        <ng-container *ngFor=\"let error of error_messages.email\">\r\n          <ion-text class=\"ion-padding\"\r\n           color=\"danger\" \r\n           *ngIf=\"EmailForm.get('email').hasError(error.type) \r\n           && (EmailForm.get('email').dirty ||\r\n            EmailForm.get('email').touched)\">\r\n            {{ error.message }}\r\n          </ion-text>\r\n        </ng-container>\r\n      </div>\r\n    </form>\r\n    <ion-button expand=\"block\" [disabled]='!EmailForm.valid' routerDirection=\"root\" (click)='sendResetEmail()'>\r\n      Send \r\n    </ion-button>\r\n  </ion-card>\r\n</ion-content>\r\n\r\n<div align=\"center\" padding>\r\n  <ion-text color=\"primary\" routerLink=\"/auth\">Go back to login</ion-text>\r\n</div>";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/auth/reset-password/response-reset-component/response-reset-component.component.html":
  /*!********************************************************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/auth/reset-password/response-reset-component/response-reset-component.component.html ***!
    \********************************************************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppAuthResetPasswordResponseResetComponentResponseResetComponentComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar>\n    <ion-title>Reset Password</ion-title>\n    <ion-buttons slot=\"start\">\n      <ion-back-button></ion-back-button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n\n<ion-card *ngIf=\"CurrentState == 'Wait'\" padding>\n  Please Wait ...\n</ion-card>\n\n<ion-card *ngIf=\"CurrentState == 'NotVerified'\" padding>\n  Invalid Link, Please Resend your Email \n</ion-card>\n\n<ion-content  *ngIf=\"CurrentState == 'Verified'\">\n  <ion-card padding>\n  <form [formGroup]=\"ResponseResetForm\">\n    <ion-item lines=\"inset\">\n      <ion-input formControlName='newPassword'\n       placeholder='New password' \n       [type]=\"showPassword ? 'text' : 'password'\">\n      </ion-input>\n      <ion-icon slot=\"end\" \n      [name]=\"PasswordToggleIcon\" \n      (click)=\"togglePassword()\"></ion-icon>\n  </ion-item>\n       <div>\n    <ng-container *ngFor=\"let error of error_messages.newPassword\">\n      <ion-text class=\"ion-padding\" \n      color=\"danger\" \n      *ngIf=\"ResponseResetForm.get('newPassword').hasError(error.type) \n      && (ResponseResetForm.get('newPassword').dirty \n      || ResponseResetForm.get('newPassword').touched)\">\n        {{ error.message }}\n      </ion-text>\n    </ng-container>\n  </div>\n  <ion-item lines=\"inset\">\n      <ion-input formControlName='confirmPassword' \n      placeholder='confirm Password' \n      [type]=\"showPassword ? 'text' : 'password'\">\n    </ion-input>\n      <ion-icon slot=\"end\" \n      [name]=\"PasswordToggleIcon\" \n      (click)=\"togglePassword()\"></ion-icon>\n  </ion-item>\n  <div>\n    <ng-container *ngFor=\"let error of error_messages.confirmPassword\">\n      <ion-text class=\"ion-padding\" color=\"danger\"\n       *ngIf=\"ResponseResetForm.get('confirmPassword').hasError(error.type)\n        && (ResponseResetForm.get('confirmPassword').dirty || \n        ResponseResetForm.get('confirmPassword').touched)\">\n        {{ error.message }}\n      </ion-text>\n    </ng-container>\n  </div>\n  <ion-text  class=\"ion-padding\" color=\"danger\" *ngIf=\"!ResponseResetForm.get('confirmPassword').errors \n  && ResponseResetForm.hasError('passwordNotMatch') \n  && (ResponseResetForm.get('confirmPassword').dirty ||\n  ResponseResetForm.get('confirmPassword').touched)\">\n    New password and Confirm password fields should match\n  </ion-text>\n</form>\n\n<ion-button expand=\"block\" \n [disabled]='!ResponseResetForm.valid'\nrouterDirection=\"root\" \n(click)='ResetPassword(ResponseResetForm)'>\n  Confirm\n</ion-button>\n</ion-card>\n</ion-content>\n\n<div align=\"center\" padding>\n  <ion-text color=\"primary\" routerLink=\"/auth/reset-password/emailnotif\">Resend Email</ion-text>\n</div>\n";
    /***/
  },

  /***/
  "./src/app/auth/reset-password/reset-password-routing.module.ts":
  /*!**********************************************************************!*\
    !*** ./src/app/auth/reset-password/reset-password-routing.module.ts ***!
    \**********************************************************************/

  /*! exports provided: ResetPasswordPageRoutingModule */

  /***/
  function srcAppAuthResetPasswordResetPasswordRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ResetPasswordPageRoutingModule", function () {
      return ResetPasswordPageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _reset_password_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./reset-password.page */
    "./src/app/auth/reset-password/reset-password.page.ts");
    /* harmony import */


    var _response_reset_component_response_reset_component_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ./response-reset-component/response-reset-component.component */
    "./src/app/auth/reset-password/response-reset-component/response-reset-component.component.ts");

    var routes = [{
      path: 'emailnotif',
      component: _reset_password_page__WEBPACK_IMPORTED_MODULE_3__["ResetPasswordPage"]
    }, {
      path: 'resetpassword/:token',
      component: _response_reset_component_response_reset_component_component__WEBPACK_IMPORTED_MODULE_4__["ResponseResetComponentComponent"]
    }];

    var ResetPasswordPageRoutingModule = function ResetPasswordPageRoutingModule() {
      _classCallCheck(this, ResetPasswordPageRoutingModule);
    };

    ResetPasswordPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], ResetPasswordPageRoutingModule);
    /***/
  },

  /***/
  "./src/app/auth/reset-password/reset-password.module.ts":
  /*!**************************************************************!*\
    !*** ./src/app/auth/reset-password/reset-password.module.ts ***!
    \**************************************************************/

  /*! exports provided: ResetPasswordPageModule */

  /***/
  function srcAppAuthResetPasswordResetPasswordModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ResetPasswordPageModule", function () {
      return ResetPasswordPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _reset_password_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ./reset-password-routing.module */
    "./src/app/auth/reset-password/reset-password-routing.module.ts");
    /* harmony import */


    var _reset_password_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./reset-password.page */
    "./src/app/auth/reset-password/reset-password.page.ts");
    /* harmony import */


    var src_app_shared_module_shared_module_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! src/app/shared-module/shared-module.module */
    "./src/app/shared-module/shared-module.module.ts");
    /* harmony import */


    var _response_reset_component_response_reset_component_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ./response-reset-component/response-reset-component.component */
    "./src/app/auth/reset-password/response-reset-component/response-reset-component.component.ts");

    var ResetPasswordPageModule = function ResetPasswordPageModule() {
      _classCallCheck(this, ResetPasswordPageModule);
    };

    ResetPasswordPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["IonicModule"], src_app_shared_module_shared_module_module__WEBPACK_IMPORTED_MODULE_6__["SharedModuleModule"], _reset_password_routing_module__WEBPACK_IMPORTED_MODULE_4__["ResetPasswordPageRoutingModule"]],
      declarations: [_reset_password_page__WEBPACK_IMPORTED_MODULE_5__["ResetPasswordPage"], _response_reset_component_response_reset_component_component__WEBPACK_IMPORTED_MODULE_7__["ResponseResetComponentComponent"]]
    })], ResetPasswordPageModule);
    /***/
  },

  /***/
  "./src/app/auth/reset-password/reset-password.page.scss":
  /*!**************************************************************!*\
    !*** ./src/app/auth/reset-password/reset-password.page.scss ***!
    \**************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppAuthResetPasswordResetPasswordPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2F1dGgvcmVzZXQtcGFzc3dvcmQvcmVzZXQtcGFzc3dvcmQucGFnZS5zY3NzIn0= */";
    /***/
  },

  /***/
  "./src/app/auth/reset-password/reset-password.page.ts":
  /*!************************************************************!*\
    !*** ./src/app/auth/reset-password/reset-password.page.ts ***!
    \************************************************************/

  /*! exports provided: ResetPasswordPage */

  /***/
  function srcAppAuthResetPasswordResetPasswordPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ResetPasswordPage", function () {
      return ResetPasswordPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var src_app_services_users_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! src/app/services/users.service */
    "./src/app/services/users.service.ts");

    var ResetPasswordPage = /*#__PURE__*/function () {
      function ResetPasswordPage(_fb, usersService, toastController, router) {
        _classCallCheck(this, ResetPasswordPage);

        this._fb = _fb;
        this.usersService = usersService;
        this.toastController = toastController;
        this.router = router;
        this.error_messages = {
          email: [{
            type: 'required',
            message: 'Email is required.'
          }, {
            type: 'minlength',
            message: 'Email length.'
          }, {
            type: 'maxlength',
            message: 'Email length.'
          }, {
            type: 'required',
            message: 'please enter a valid email address.'
          }]
        };
      }

      _createClass(ResetPasswordPage, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          this.initResetform();
        }
      }, {
        key: "initResetform",
        value: function initResetform() {
          this.EmailForm = this._fb.group({
            email: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(6), _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(100), _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')])]
          });
        }
      }, {
        key: "sendResetEmail",
        value: function sendResetEmail() {
          var _this = this;

          this.usersService.requestReset(this.EmailForm.value).subscribe(function (data) {
            _this.EmailForm.reset();

            var successMessage = 'Reset password link send to email sucessfully, Please Check Your Email';

            _this.presentToast(successMessage, 'success');

            setTimeout(function () {
              _this.router.navigate(['/auth/login']);
            }, 3000);
          }, function (err) {
            if (err.error.message) {
              var errorMessage = err.error.message;

              _this.presentToast(errorMessage, 'error');
            }
          });
        }
      }, {
        key: "presentToast",
        value: function presentToast(msg, messagetype) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
            var toast;
            return regeneratorRuntime.wrap(function _callee$(_context) {
              while (1) {
                switch (_context.prev = _context.next) {
                  case 0:
                    _context.next = 2;
                    return this.toastController.create({
                      message: msg,
                      duration: 3000,
                      position: 'bottom',
                      color: messagetype === 'success' ? 'success' : 'danger'
                    });

                  case 2:
                    toast = _context.sent;
                    toast.present();

                  case 4:
                  case "end":
                    return _context.stop();
                }
              }
            }, _callee, this);
          }));
        }
      }]);

      return ResetPasswordPage;
    }();

    ResetPasswordPage.ctorParameters = function () {
      return [{
        type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"]
      }, {
        type: src_app_services_users_service__WEBPACK_IMPORTED_MODULE_5__["UserService"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]
      }];
    };

    ResetPasswordPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-reset-password',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./reset-password.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/auth/reset-password/reset-password.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./reset-password.page.scss */
      "./src/app/auth/reset-password/reset-password.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"], src_app_services_users_service__WEBPACK_IMPORTED_MODULE_5__["UserService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])], ResetPasswordPage);
    /***/
  },

  /***/
  "./src/app/auth/reset-password/response-reset-component/response-reset-component.component.scss":
  /*!******************************************************************************************************!*\
    !*** ./src/app/auth/reset-password/response-reset-component/response-reset-component.component.scss ***!
    \******************************************************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppAuthResetPasswordResponseResetComponentResponseResetComponentComponentScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2F1dGgvcmVzZXQtcGFzc3dvcmQvcmVzcG9uc2UtcmVzZXQtY29tcG9uZW50L3Jlc3BvbnNlLXJlc2V0LWNvbXBvbmVudC5jb21wb25lbnQuc2NzcyJ9 */";
    /***/
  },

  /***/
  "./src/app/auth/reset-password/response-reset-component/response-reset-component.component.ts":
  /*!****************************************************************************************************!*\
    !*** ./src/app/auth/reset-password/response-reset-component/response-reset-component.component.ts ***!
    \****************************************************************************************************/

  /*! exports provided: ResponseResetComponentComponent */

  /***/
  function srcAppAuthResetPasswordResponseResetComponentResponseResetComponentComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ResponseResetComponentComponent", function () {
      return ResponseResetComponentComponent;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var src_app_services_users_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! src/app/services/users.service */
    "./src/app/services/users.service.ts");

    var ResponseResetComponentComponent = /*#__PURE__*/function () {
      function ResponseResetComponentComponent(activatedRoute, router, userservice, fb, toastController) {
        var _this2 = this;

        _classCallCheck(this, ResponseResetComponentComponent);

        this.activatedRoute = activatedRoute;
        this.router = router;
        this.userservice = userservice;
        this.fb = fb;
        this.toastController = toastController;
        this.showPassword = false;
        this.PasswordToggleIcon = 'eye';
        this.error_messages = {
          newPassword: [{
            type: 'required',
            message: 'password is required.'
          }, {
            type: 'minlength',
            message: 'password length.'
          }, {
            type: 'maxlength',
            message: 'password length.'
          }],
          confirmPassword: [{
            type: 'required',
            message: 'password is required.'
          }, {
            type: 'minlength',
            message: 'password length.'
          }, {
            type: 'maxlength',
            message: 'password length.'
          }]
        };
        this.CurrentState = 'Wait';
        this.activatedRoute.params.subscribe(function (params) {
          if (params !== {} && params.token !== undefined) {
            _this2.resetToken = params.token;

            _this2.VerifyToken();
          }
        });
      }

      _createClass(ResponseResetComponentComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          this.Init();
        }
      }, {
        key: "VerifyToken",
        value: function VerifyToken() {
          var _this3 = this;

          this.userservice.ValidPasswordToken({
            resettoken: this.resetToken
          }).subscribe(function (data) {
            _this3.CurrentState = 'Verified';
          }, function (err) {
            _this3.CurrentState = 'NotVerified';
          });
        }
      }, {
        key: "Init",
        value: function Init() {
          this.ResponseResetForm = this.fb.group({
            resettoken: [this.resetToken],
            newPassword: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(6), _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(30)])],
            confirmPassword: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(6), _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(30)])]
          }, {
            validators: this.password.bind(this)
          });
        }
      }, {
        key: "password",
        value: function password(formGroup) {
          var _formGroup$get = formGroup.get('newPassword'),
              password = _formGroup$get.value;

          var _formGroup$get2 = formGroup.get('confirmPassword'),
              confirmPassword = _formGroup$get2.value;

          return password === confirmPassword ? null : {
            passwordNotMatch: true
          };
        }
      }, {
        key: "Validate",
        value: function Validate(passwordFormGroup) {
          var new_password = passwordFormGroup.controls.newPassword.value;
          var confirm_password = passwordFormGroup.controls.confirmPassword.value;

          if (confirm_password.length <= 0) {
            return null;
          }

          if (confirm_password !== new_password) {
            return {
              doesNotMatch: true
            };
          }

          return null;
        }
      }, {
        key: "ResetPassword",
        value: function ResetPassword(form) {
          var _this4 = this;

          console.log(form.get('confirmPassword'));
          this.userservice.newPassword(this.ResponseResetForm.value).subscribe(function (data) {
            _this4.ResponseResetForm.reset();

            var successMessage = data.message;

            _this4.presentToast(successMessage, 'success');

            setTimeout(function () {
              _this4.router.navigate(['/auth/login']);
            }, 3000);
          }, function (err) {
            if (err.error.message) {
              var errorMessage = err.error.message;

              _this4.presentToast(errorMessage, 'error');
            }
          });
        }
      }, {
        key: "presentToast",
        value: function presentToast(msg, messagetype) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
            var toast;
            return regeneratorRuntime.wrap(function _callee2$(_context2) {
              while (1) {
                switch (_context2.prev = _context2.next) {
                  case 0:
                    _context2.next = 2;
                    return this.toastController.create({
                      message: msg,
                      duration: 5000,
                      position: 'bottom',
                      color: messagetype === 'success' ? 'success' : 'danger'
                    });

                  case 2:
                    toast = _context2.sent;
                    toast.present();

                  case 4:
                  case "end":
                    return _context2.stop();
                }
              }
            }, _callee2, this);
          }));
        }
      }, {
        key: "togglePassword",
        value: function togglePassword() {
          this.showPassword = !this.showPassword;

          if (this.PasswordToggleIcon == 'eye') {
            this.PasswordToggleIcon = 'eye-off';
          } else {
            this.PasswordToggleIcon = 'eye';
          }
        }
      }]);

      return ResponseResetComponentComponent;
    }();

    ResponseResetComponentComponent.ctorParameters = function () {
      return [{
        type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]
      }, {
        type: src_app_services_users_service__WEBPACK_IMPORTED_MODULE_5__["UserService"]
      }, {
        type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"]
      }];
    };

    ResponseResetComponentComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-response-reset-component',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./response-reset-component.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/auth/reset-password/response-reset-component/response-reset-component.component.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./response-reset-component.component.scss */
      "./src/app/auth/reset-password/response-reset-component/response-reset-component.component.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], src_app_services_users_service__WEBPACK_IMPORTED_MODULE_5__["UserService"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"]])], ResponseResetComponentComponent);
    /***/
  }
}]);
//# sourceMappingURL=reset-password-reset-password-module-es5.js.map