(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["institut-details-institut-details-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/institutes/institut-details/institut-details.page.html":
/*!**************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/institutes/institut-details/institut-details.page.html ***!
  \**************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\r\n  <ion-toolbar>\r\n    <ion-title>Institut details</ion-title>\r\n    <ion-buttons slot=\"start\">\r\n        <ion-menu-button autoHide=\"true\"></ion-menu-button>\r\n        <ion-back-button></ion-back-button>\r\n      </ion-buttons>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <ion-card *ngIf='institutDetails'>\r\n    <ion-item>\r\n      <ion-label>name</ion-label>\r\n      <ion-label *ngIf='institutDetails.InstituteName; else noData'>{{institutDetails.InstituteName}}</ion-label>\r\n    </ion-item>\r\n  \r\n    <ion-item>\r\n      <ion-label>country</ion-label>\r\n      <ion-label *ngIf='institutDetails.country; else noData'>{{institutDetails.country}}</ion-label>\r\n    </ion-item>\r\n  \r\n    <ion-item>\r\n      <ion-label>city</ion-label>\r\n      <ion-label *ngIf='institutDetails.city; else noData'>{{institutDetails.city}}</ion-label>\r\n    </ion-item>\r\n  </ion-card>\r\n<ng-template #noData>\r\n <ion-label>\r\n   N/A\r\n  </ion-label>\r\n</ng-template>\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/institutes/institut-details/institut-details-routing.module.ts":
/*!********************************************************************************!*\
  !*** ./src/app/institutes/institut-details/institut-details-routing.module.ts ***!
  \********************************************************************************/
/*! exports provided: InstitutDetailsPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InstitutDetailsPageRoutingModule", function() { return InstitutDetailsPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _institut_details_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./institut-details.page */ "./src/app/institutes/institut-details/institut-details.page.ts");




const routes = [
    {
        path: '',
        component: _institut_details_page__WEBPACK_IMPORTED_MODULE_3__["InstitutDetailsPage"]
    }
];
let InstitutDetailsPageRoutingModule = class InstitutDetailsPageRoutingModule {
};
InstitutDetailsPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], InstitutDetailsPageRoutingModule);



/***/ }),

/***/ "./src/app/institutes/institut-details/institut-details.module.ts":
/*!************************************************************************!*\
  !*** ./src/app/institutes/institut-details/institut-details.module.ts ***!
  \************************************************************************/
/*! exports provided: InstitutDetailsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InstitutDetailsPageModule", function() { return InstitutDetailsPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _institut_details_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./institut-details-routing.module */ "./src/app/institutes/institut-details/institut-details-routing.module.ts");
/* harmony import */ var _institut_details_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./institut-details.page */ "./src/app/institutes/institut-details/institut-details.page.ts");







let InstitutDetailsPageModule = class InstitutDetailsPageModule {
};
InstitutDetailsPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _institut_details_routing_module__WEBPACK_IMPORTED_MODULE_5__["InstitutDetailsPageRoutingModule"]
        ],
        declarations: [_institut_details_page__WEBPACK_IMPORTED_MODULE_6__["InstitutDetailsPage"]]
    })
], InstitutDetailsPageModule);



/***/ }),

/***/ "./src/app/institutes/institut-details/institut-details.page.scss":
/*!************************************************************************!*\
  !*** ./src/app/institutes/institut-details/institut-details.page.scss ***!
  \************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2luc3RpdHV0ZXMvaW5zdGl0dXQtZGV0YWlscy9pbnN0aXR1dC1kZXRhaWxzLnBhZ2Uuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/institutes/institut-details/institut-details.page.ts":
/*!**********************************************************************!*\
  !*** ./src/app/institutes/institut-details/institut-details.page.ts ***!
  \**********************************************************************/
/*! exports provided: InstitutDetailsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InstitutDetailsPage", function() { return InstitutDetailsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var src_app_services_institut_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/institut.service */ "./src/app/services/institut.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");




let InstitutDetailsPage = class InstitutDetailsPage {
    constructor(activatedRoute, institutService) {
        this.activatedRoute = activatedRoute;
        this.institutService = institutService;
    }
    ngOnInit() {
        this.InstitutId = this.activatedRoute.snapshot.paramMap.get('id');
        this.getInstitutById(this.InstitutId);
    }
    getInstitutById(InstitutId) {
        this.institutService.getInstitutById(InstitutId).subscribe(data => {
            if (data.code === '0000') {
                console.log(data.content);
                this.institutDetails = data.content[0];
            }
            else {
                console.log('error', data);
            }
        }, (err) => {
            console.log('error', err);
        });
    }
};
InstitutDetailsPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"] },
    { type: src_app_services_institut_service__WEBPACK_IMPORTED_MODULE_2__["InstitutService"] }
];
InstitutDetailsPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-institut-details',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./institut-details.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/institutes/institut-details/institut-details.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./institut-details.page.scss */ "./src/app/institutes/institut-details/institut-details.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"],
        src_app_services_institut_service__WEBPACK_IMPORTED_MODULE_2__["InstitutService"]])
], InstitutDetailsPage);



/***/ })

}]);
//# sourceMappingURL=institut-details-institut-details-module-es2015.js.map