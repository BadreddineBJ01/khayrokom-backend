(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["institues-list-institues-list-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/institutes/institues-list/institues-list.page.html":
/*!**********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/institutes/institues-list/institues-list.page.html ***!
  \**********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\r\n  <ion-toolbar>\r\n    <ion-title>Institut list</ion-title>\r\n    <ion-buttons slot=\"start\">\r\n      <ion-menu-button autoHide=\"false\"></ion-menu-button>\r\n      <ion-back-button></ion-back-button>\r\n    </ion-buttons>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content padding>\r\n  <ion-button \r\n  *ngIf=\"connectedUserRole == 'admin'\"\r\n    shape=\"round\"\r\n    expand=\"block\"\r\n    routerLink=\"/institutes/add-institut\"\r\n    routerDirection=\"root\">\r\n    <ion-icon slot=\"icon-only\" name=\"add\"></ion-icon>\r\n    Add institut\r\n  </ion-button>\r\n\r\n  <ion-card>\r\n  <ion-item-sliding *ngFor=\"let institute of institutesList\">\r\n    <ion-item (click)='goToClasses(institute._id)'>\r\n      <ion-label>\r\n        {{institute.InstituteName}}\r\n      </ion-label>\r\n    </ion-item>\r\n    <ion-item-options>\r\n      <ion-item-option color=\"primary\" (click)=\"Institutdetails(institute._id)\">\r\n        <ion-icon slot=\"top\" name=\"more\"></ion-icon>\r\n        details\r\n      </ion-item-option>\r\n      <ion-item-option color=\"secondary\"  \r\n      *ngIf=\"connectedUserRole == 'admin'\"\r\n       (click)=\"editInstitut(institute._id)\">\r\n        <ion-icon slot=\"top\" name=\"create\"></ion-icon>\r\n        edit\r\n      </ion-item-option>\r\n      <ion-item-option \r\n        *ngIf=\"connectedUserRole == 'admin'\"\r\n        color=\"danger\"\r\n        (click)=\"presentConfirmDelete(institute._id)\">\r\n        <ion-icon slot=\"top\" name=\"trash\"></ion-icon>\r\n        trash\r\n      </ion-item-option>\r\n    </ion-item-options>\r\n  </ion-item-sliding>\r\n  </ion-card>\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/institutes/institues-list/institues-list-routing.module.ts":
/*!****************************************************************************!*\
  !*** ./src/app/institutes/institues-list/institues-list-routing.module.ts ***!
  \****************************************************************************/
/*! exports provided: InstituesListPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InstituesListPageRoutingModule", function() { return InstituesListPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _institues_list_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./institues-list.page */ "./src/app/institutes/institues-list/institues-list.page.ts");




const routes = [
    {
        path: '',
        component: _institues_list_page__WEBPACK_IMPORTED_MODULE_3__["InstituesListPage"]
    }
];
let InstituesListPageRoutingModule = class InstituesListPageRoutingModule {
};
InstituesListPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)
        ],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], InstituesListPageRoutingModule);



/***/ }),

/***/ "./src/app/institutes/institues-list/institues-list.module.ts":
/*!********************************************************************!*\
  !*** ./src/app/institutes/institues-list/institues-list.module.ts ***!
  \********************************************************************/
/*! exports provided: InstituesListPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InstituesListPageModule", function() { return InstituesListPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _institues_list_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./institues-list-routing.module */ "./src/app/institutes/institues-list/institues-list-routing.module.ts");
/* harmony import */ var _institues_list_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./institues-list.page */ "./src/app/institutes/institues-list/institues-list.page.ts");







let InstituesListPageModule = class InstituesListPageModule {
};
InstituesListPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _institues_list_routing_module__WEBPACK_IMPORTED_MODULE_5__["InstituesListPageRoutingModule"]
        ],
        declarations: [_institues_list_page__WEBPACK_IMPORTED_MODULE_6__["InstituesListPage"]]
    })
], InstituesListPageModule);



/***/ }),

/***/ "./src/app/institutes/institues-list/institues-list.page.scss":
/*!********************************************************************!*\
  !*** ./src/app/institutes/institues-list/institues-list.page.scss ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2luc3RpdHV0ZXMvaW5zdGl0dWVzLWxpc3QvaW5zdGl0dWVzLWxpc3QucGFnZS5zY3NzIn0= */");

/***/ }),

/***/ "./src/app/institutes/institues-list/institues-list.page.ts":
/*!******************************************************************!*\
  !*** ./src/app/institutes/institues-list/institues-list.page.ts ***!
  \******************************************************************/
/*! exports provided: InstituesListPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InstituesListPage", function() { return InstituesListPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var src_app_services_institut_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/institut.service */ "./src/app/services/institut.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_app_services_users_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/users.service */ "./src/app/services/users.service.ts");
/* harmony import */ var src_app_services_communication_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/communication.service */ "./src/app/services/communication.service.ts");







let InstituesListPage = class InstituesListPage {
    constructor(communicationService, institutService, router, userService, alertCtrl, modalController, loadingController, toastController) {
        this.communicationService = communicationService;
        this.institutService = institutService;
        this.router = router;
        this.userService = userService;
        this.alertCtrl = alertCtrl;
        this.modalController = modalController;
        this.loadingController = loadingController;
        this.toastController = toastController;
    }
    ngOnInit() {
        this.router.events.subscribe(res => {
            if (res instanceof _angular_router__WEBPACK_IMPORTED_MODULE_3__["NavigationEnd"]) {
                this.getAllInstitutes();
                this.setUserRole();
            }
        });
    }
    setUserRole() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            this.connectedUserRole = yield this.userService.getUserRole();
        });
    }
    goToClasses(institutId) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            this.communicationService.InstitutId = institutId;
            let userRole = yield this.userService.getUserRole();
            switch (userRole) {
                case "admin": {
                    this.router.navigate(["classes/classe-list/" + institutId]);
                    break;
                }
                case "cheick": {
                    this.router.navigate(["/halaqat/halaqat-list/" + institutId]);
                    break;
                }
                default: {
                    console.log("user role is not setted");
                }
            }
        });
    }
    getAllInstitutes() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const loading = yield this.loadingController.create({
                message: "Loading..."
            });
            yield loading.present();
            this.institutService.getAllInstitutes().subscribe(data => {
                if (data.code === "0000") {
                    console.log("data", data);
                    this.institutesList = data.content;
                    loading.dismiss();
                }
                else {
                    console.log("error");
                    loading.dismiss();
                }
            }, err => {
                console.log("subscription error", err);
                loading.dismiss();
            });
        });
    }
    editInstitut(institutId) {
        this.router.navigate(["institutes/edit-institut/" + institutId]);
    }
    delete(institutId) {
        this.institutService.deleteInstitut(institutId).subscribe(data => {
            if (data.code === "0000") {
                this.presentToast(data.message, "success");
                console.log("delete data", data);
            }
            else {
                this.presentToast(data.message, "error");
                console.log("error");
            }
        }, err => {
            this.presentToast(err, "error");
            console.log("error");
        });
    }
    Institutdetails(institutId) {
        this.router.navigate(["institutes/institut-details/" + institutId]);
    }
    presentConfirmDelete(institutId) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const alert = this.alertCtrl.create({
                message: "Do you want to delet this institut",
                buttons: [
                    {
                        text: "Cancel",
                        role: "cancel",
                        handler: () => {
                            console.log("Cancel clicked");
                        }
                    },
                    {
                        text: "Confirm",
                        handler: () => {
                            this.delete(institutId);
                            this.ReloadList();
                        }
                    }
                ]
            });
            (yield alert).present();
        });
    }
    ReloadList() {
        this.getAllInstitutes();
    }
    presentToast(msg, messagetype) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const toast = yield this.toastController.create({
                message: msg,
                duration: 3000,
                position: "bottom",
                color: messagetype === "success" ? "success" : "danger"
            });
            toast.present();
        });
    }
};
InstituesListPage.ctorParameters = () => [
    { type: src_app_services_communication_service__WEBPACK_IMPORTED_MODULE_6__["CommunicationService"] },
    { type: src_app_services_institut_service__WEBPACK_IMPORTED_MODULE_2__["InstitutService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
    { type: src_app_services_users_service__WEBPACK_IMPORTED_MODULE_5__["UserService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"] }
];
InstituesListPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: "app-institues-list",
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./institues-list.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/institutes/institues-list/institues-list.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./institues-list.page.scss */ "./src/app/institutes/institues-list/institues-list.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_communication_service__WEBPACK_IMPORTED_MODULE_6__["CommunicationService"],
        src_app_services_institut_service__WEBPACK_IMPORTED_MODULE_2__["InstitutService"],
        _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
        src_app_services_users_service__WEBPACK_IMPORTED_MODULE_5__["UserService"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"]])
], InstituesListPage);



/***/ })

}]);
//# sourceMappingURL=institues-list-institues-list-module-es2015.js.map