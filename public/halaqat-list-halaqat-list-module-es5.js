function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["halaqat-list-halaqat-list-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/halaqat/halaqat-list/halaqat-list.page.html":
  /*!***************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/halaqat/halaqat-list/halaqat-list.page.html ***!
    \***************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppHalaqatHalaqatListHalaqatListPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\r\n  <ion-toolbar>\r\n    <ion-title>halaqat-list</ion-title>\r\n    <ion-buttons slot=\"start\">\r\n        <ion-menu-button autoHide=\"false\"></ion-menu-button>\r\n        <ion-back-button></ion-back-button>\r\n      </ion-buttons>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content padding>\r\n  <ion-button\r\n    *ngIf=\"connectedUserRole == 'cheick' || connectedUserRole == 'admin'\"\r\n    shape=\"round\"\r\n    expand=\"block\"\r\n    (click)=\"goToAddHalaqa()\">\r\n    <ion-icon slot=\"icon-only\" name=\"add\"></ion-icon>\r\n    Add halaqa\r\n  </ion-button>\r\n\r\n  <ion-card>\r\n  <ion-item-sliding *ngFor=\"let halaqa of halaqatList\">\r\n    <ion-item (click)='goToAddStudents(halaqa._id)'>\r\n      <ion-label>\r\n        {{halaqa.Halaqaname}}\r\n      </ion-label>\r\n    </ion-item>\r\n    <ion-item-options>\r\n      <ion-item-option color=\"primary\" \r\n      (click)=\"Halaqadetails(halaqa._id)\">\r\n        <ion-icon slot=\"top\" name=\"more\"></ion-icon>\r\n        details\r\n      </ion-item-option>\r\n      <ion-item-option color=\"secondary\" \r\n      *ngIf=\"connectedUserRole == 'cheick' || connectedUserRole == 'admin'\"\r\n       (click)=\"edithalaqa(halaqa._id)\">\r\n        <ion-icon slot=\"top\" name=\"create\"></ion-icon>\r\n        edit\r\n      </ion-item-option>\r\n      <ion-item-option \r\n       *ngIf=\"connectedUserRole == 'cheick' || connectedUserRole == 'admin'\"\r\n        color=\"danger\"\r\n        (click)=\"presentConfirmDelete(halaqa._id)\">\r\n        <ion-icon slot=\"top\" name=\"trash\"></ion-icon>\r\n        trash\r\n      </ion-item-option>\r\n    </ion-item-options>\r\n  </ion-item-sliding>\r\n  </ion-card>\r\n</ion-content>\r\n";
    /***/
  },

  /***/
  "./src/app/halaqat/halaqat-list/halaqat-list-routing.module.ts":
  /*!*********************************************************************!*\
    !*** ./src/app/halaqat/halaqat-list/halaqat-list-routing.module.ts ***!
    \*********************************************************************/

  /*! exports provided: HalaqatListPageRoutingModule */

  /***/
  function srcAppHalaqatHalaqatListHalaqatListRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "HalaqatListPageRoutingModule", function () {
      return HalaqatListPageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _halaqat_list_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./halaqat-list.page */
    "./src/app/halaqat/halaqat-list/halaqat-list.page.ts");

    var routes = [{
      path: '',
      component: _halaqat_list_page__WEBPACK_IMPORTED_MODULE_3__["HalaqatListPage"]
    }];

    var HalaqatListPageRoutingModule = function HalaqatListPageRoutingModule() {
      _classCallCheck(this, HalaqatListPageRoutingModule);
    };

    HalaqatListPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], HalaqatListPageRoutingModule);
    /***/
  },

  /***/
  "./src/app/halaqat/halaqat-list/halaqat-list.module.ts":
  /*!*************************************************************!*\
    !*** ./src/app/halaqat/halaqat-list/halaqat-list.module.ts ***!
    \*************************************************************/

  /*! exports provided: HalaqatListPageModule */

  /***/
  function srcAppHalaqatHalaqatListHalaqatListModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "HalaqatListPageModule", function () {
      return HalaqatListPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _halaqat_list_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./halaqat-list-routing.module */
    "./src/app/halaqat/halaqat-list/halaqat-list-routing.module.ts");
    /* harmony import */


    var _halaqat_list_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./halaqat-list.page */
    "./src/app/halaqat/halaqat-list/halaqat-list.page.ts");

    var HalaqatListPageModule = function HalaqatListPageModule() {
      _classCallCheck(this, HalaqatListPageModule);
    };

    HalaqatListPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _halaqat_list_routing_module__WEBPACK_IMPORTED_MODULE_5__["HalaqatListPageRoutingModule"]],
      declarations: [_halaqat_list_page__WEBPACK_IMPORTED_MODULE_6__["HalaqatListPage"]]
    })], HalaqatListPageModule);
    /***/
  },

  /***/
  "./src/app/halaqat/halaqat-list/halaqat-list.page.scss":
  /*!*************************************************************!*\
    !*** ./src/app/halaqat/halaqat-list/halaqat-list.page.scss ***!
    \*************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppHalaqatHalaqatListHalaqatListPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2hhbGFxYXQvaGFsYXFhdC1saXN0L2hhbGFxYXQtbGlzdC5wYWdlLnNjc3MifQ== */";
    /***/
  },

  /***/
  "./src/app/halaqat/halaqat-list/halaqat-list.page.ts":
  /*!***********************************************************!*\
    !*** ./src/app/halaqat/halaqat-list/halaqat-list.page.ts ***!
    \***********************************************************/

  /*! exports provided: HalaqatListPage */

  /***/
  function srcAppHalaqatHalaqatListHalaqatListPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "HalaqatListPage", function () {
      return HalaqatListPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var src_app_services_halaqa_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! src/app/services/halaqa.service */
    "./src/app/services/halaqa.service.ts");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var src_app_services_users_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! src/app/services/users.service */
    "./src/app/services/users.service.ts");
    /* harmony import */


    var src_app_services_communication_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! src/app/services/communication.service */
    "./src/app/services/communication.service.ts");

    var HalaqatListPage = /*#__PURE__*/function () {
      function HalaqatListPage(halaqaService, router, userService, activatedRoute, alertCtrl, modalController, loadingController, communicationService) {
        _classCallCheck(this, HalaqatListPage);

        this.halaqaService = halaqaService;
        this.router = router;
        this.userService = userService;
        this.activatedRoute = activatedRoute;
        this.alertCtrl = alertCtrl;
        this.modalController = modalController;
        this.loadingController = loadingController;
        this.communicationService = communicationService;
      }

      _createClass(HalaqatListPage, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          var _this = this;

          this.activatedRoute.params.subscribe(function (params) {
            if (params !== {} && params.id !== undefined) {
              _this.InstitutId = _this.communicationService.InstitutId = params.id;
            } else {
              console.log('error id not found');
            }
          });
          this.router.events.subscribe(function (res) {
            if (res instanceof _angular_router__WEBPACK_IMPORTED_MODULE_3__["NavigationEnd"]) {
              _this.getAllCheickHalaqatById();
            }
          });
          this.getConnectdUserId();
          this.setUserRole();
        }
      }, {
        key: "setUserRole",
        value: function setUserRole() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
            return regeneratorRuntime.wrap(function _callee$(_context) {
              while (1) {
                switch (_context.prev = _context.next) {
                  case 0:
                    _context.next = 2;
                    return this.userService.getUserRole();

                  case 2:
                    this.connectedUserRole = _context.sent;

                  case 3:
                  case "end":
                    return _context.stop();
                }
              }
            }, _callee, this);
          }));
        }
      }, {
        key: "getConnectdUserId",
        value: function getConnectdUserId() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
            return regeneratorRuntime.wrap(function _callee2$(_context2) {
              while (1) {
                switch (_context2.prev = _context2.next) {
                  case 0:
                    _context2.next = 2;
                    return this.userService.getConnectedUserId();

                  case 2:
                    this.connectedUserId = _context2.sent;
                    console.log('user id ', this.connectedUserId);

                  case 4:
                  case "end":
                    return _context2.stop();
                }
              }
            }, _callee2, this);
          }));
        }
      }, {
        key: "goToAddStudents",
        value: function goToAddStudents(halaqaId) {
          this.router.navigate(['students/students-list/' + halaqaId]);
        }
      }, {
        key: "goToAddHalaqa",
        value: function goToAddHalaqa() {
          this.router.navigate(['halaqat/add-halaqa/' + this.InstitutId]);
        }
      }, {
        key: "getAllCheickHalaqatById",
        value: function getAllCheickHalaqatById() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
            var _this2 = this;

            var loading;
            return regeneratorRuntime.wrap(function _callee3$(_context3) {
              while (1) {
                switch (_context3.prev = _context3.next) {
                  case 0:
                    _context3.next = 2;
                    return this.loadingController.create({
                      message: 'Loading...'
                    });

                  case 2:
                    loading = _context3.sent;
                    _context3.next = 5;
                    return loading.present();

                  case 5:
                    this.halaqaService.getAllCheickHalaqats(this.InstitutId, this.connectedUserId).subscribe(function (data) {
                      if (data.code === '0000') {
                        console.log('data', data);
                        _this2.halaqatList = data.content;
                        loading.dismiss();
                      } else {
                        console.log('error');
                        loading.dismiss();
                      }
                    }, function (err) {
                      console.log('subscription error', err);
                      loading.dismiss();
                    });

                  case 6:
                  case "end":
                    return _context3.stop();
                }
              }
            }, _callee3, this);
          }));
        }
      }, {
        key: "edithalaqa",
        value: function edithalaqa(halaqaId) {
          this.router.navigate(['halaqat/edit-halaqa/' + halaqaId]);
        }
      }, {
        key: "delete",
        value: function _delete(halaqaId) {
          this.halaqaService.deleteHalaqa(halaqaId).subscribe(function (data) {
            if (data.code === '0000') {
              console.log('delete data', data);
            } else {
              console.log('error');
            }
          }, function (err) {
            console.log('error');
          });
        }
      }, {
        key: "Halaqadetails",
        value: function Halaqadetails(halaqaId) {
          this.router.navigate(['halaqat/halaqa-details/' + halaqaId]);
        }
      }, {
        key: "presentConfirmDelete",
        value: function presentConfirmDelete(halaqaId) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee4() {
            var _this3 = this;

            var alert;
            return regeneratorRuntime.wrap(function _callee4$(_context4) {
              while (1) {
                switch (_context4.prev = _context4.next) {
                  case 0:
                    alert = this.alertCtrl.create({
                      message: 'Do you want to delet this halaqa',
                      buttons: [{
                        text: 'Cancel',
                        role: 'cancel',
                        handler: function handler() {
                          console.log('Cancel clicked');
                        }
                      }, {
                        text: 'Confirm',
                        handler: function handler() {
                          _this3["delete"](halaqaId);

                          _this3.ReloadList();
                        }
                      }]
                    });
                    _context4.next = 3;
                    return alert;

                  case 3:
                    _context4.sent.present();

                  case 4:
                  case "end":
                    return _context4.stop();
                }
              }
            }, _callee4, this);
          }));
        }
      }, {
        key: "ReloadList",
        value: function ReloadList() {
          this.getAllCheickHalaqatById();
        }
      }]);

      return HalaqatListPage;
    }();

    HalaqatListPage.ctorParameters = function () {
      return [{
        type: src_app_services_halaqa_service__WEBPACK_IMPORTED_MODULE_2__["HalaqaService"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]
      }, {
        type: src_app_services_users_service__WEBPACK_IMPORTED_MODULE_5__["UserService"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"]
      }, {
        type: src_app_services_communication_service__WEBPACK_IMPORTED_MODULE_6__["CommunicationService"]
      }];
    };

    HalaqatListPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-halaqat-list',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./halaqat-list.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/halaqat/halaqat-list/halaqat-list.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./halaqat-list.page.scss */
      "./src/app/halaqat/halaqat-list/halaqat-list.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_halaqa_service__WEBPACK_IMPORTED_MODULE_2__["HalaqaService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], src_app_services_users_service__WEBPACK_IMPORTED_MODULE_5__["UserService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"], src_app_services_communication_service__WEBPACK_IMPORTED_MODULE_6__["CommunicationService"]])], HalaqatListPage);
    /***/
  }
}]);
//# sourceMappingURL=halaqat-list-halaqat-list-module-es5.js.map