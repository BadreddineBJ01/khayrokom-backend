(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["halaqat-list-halaqat-list-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/halaqat/halaqat-list/halaqat-list.page.html":
/*!***************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/halaqat/halaqat-list/halaqat-list.page.html ***!
  \***************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\r\n  <ion-toolbar>\r\n    <ion-title>halaqat-list</ion-title>\r\n    <ion-buttons slot=\"start\">\r\n        <ion-menu-button autoHide=\"false\"></ion-menu-button>\r\n        <ion-back-button></ion-back-button>\r\n      </ion-buttons>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content padding>\r\n  <ion-button\r\n    *ngIf=\"connectedUserRole == 'cheick' || connectedUserRole == 'admin'\"\r\n    shape=\"round\"\r\n    expand=\"block\"\r\n    (click)=\"goToAddHalaqa()\">\r\n    <ion-icon slot=\"icon-only\" name=\"add\"></ion-icon>\r\n    Add halaqa\r\n  </ion-button>\r\n\r\n  <ion-card>\r\n  <ion-item-sliding *ngFor=\"let halaqa of halaqatList\">\r\n    <ion-item (click)='goToAddStudents(halaqa._id)'>\r\n      <ion-label>\r\n        {{halaqa.Halaqaname}}\r\n      </ion-label>\r\n    </ion-item>\r\n    <ion-item-options>\r\n      <ion-item-option color=\"primary\" \r\n      (click)=\"Halaqadetails(halaqa._id)\">\r\n        <ion-icon slot=\"top\" name=\"more\"></ion-icon>\r\n        details\r\n      </ion-item-option>\r\n      <ion-item-option color=\"secondary\" \r\n      *ngIf=\"connectedUserRole == 'cheick' || connectedUserRole == 'admin'\"\r\n       (click)=\"edithalaqa(halaqa._id)\">\r\n        <ion-icon slot=\"top\" name=\"create\"></ion-icon>\r\n        edit\r\n      </ion-item-option>\r\n      <ion-item-option \r\n       *ngIf=\"connectedUserRole == 'cheick' || connectedUserRole == 'admin'\"\r\n        color=\"danger\"\r\n        (click)=\"presentConfirmDelete(halaqa._id)\">\r\n        <ion-icon slot=\"top\" name=\"trash\"></ion-icon>\r\n        trash\r\n      </ion-item-option>\r\n    </ion-item-options>\r\n  </ion-item-sliding>\r\n  </ion-card>\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/halaqat/halaqat-list/halaqat-list-routing.module.ts":
/*!*********************************************************************!*\
  !*** ./src/app/halaqat/halaqat-list/halaqat-list-routing.module.ts ***!
  \*********************************************************************/
/*! exports provided: HalaqatListPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HalaqatListPageRoutingModule", function() { return HalaqatListPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _halaqat_list_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./halaqat-list.page */ "./src/app/halaqat/halaqat-list/halaqat-list.page.ts");




const routes = [
    {
        path: '',
        component: _halaqat_list_page__WEBPACK_IMPORTED_MODULE_3__["HalaqatListPage"]
    }
];
let HalaqatListPageRoutingModule = class HalaqatListPageRoutingModule {
};
HalaqatListPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], HalaqatListPageRoutingModule);



/***/ }),

/***/ "./src/app/halaqat/halaqat-list/halaqat-list.module.ts":
/*!*************************************************************!*\
  !*** ./src/app/halaqat/halaqat-list/halaqat-list.module.ts ***!
  \*************************************************************/
/*! exports provided: HalaqatListPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HalaqatListPageModule", function() { return HalaqatListPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _halaqat_list_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./halaqat-list-routing.module */ "./src/app/halaqat/halaqat-list/halaqat-list-routing.module.ts");
/* harmony import */ var _halaqat_list_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./halaqat-list.page */ "./src/app/halaqat/halaqat-list/halaqat-list.page.ts");







let HalaqatListPageModule = class HalaqatListPageModule {
};
HalaqatListPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _halaqat_list_routing_module__WEBPACK_IMPORTED_MODULE_5__["HalaqatListPageRoutingModule"]
        ],
        declarations: [_halaqat_list_page__WEBPACK_IMPORTED_MODULE_6__["HalaqatListPage"]]
    })
], HalaqatListPageModule);



/***/ }),

/***/ "./src/app/halaqat/halaqat-list/halaqat-list.page.scss":
/*!*************************************************************!*\
  !*** ./src/app/halaqat/halaqat-list/halaqat-list.page.scss ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2hhbGFxYXQvaGFsYXFhdC1saXN0L2hhbGFxYXQtbGlzdC5wYWdlLnNjc3MifQ== */");

/***/ }),

/***/ "./src/app/halaqat/halaqat-list/halaqat-list.page.ts":
/*!***********************************************************!*\
  !*** ./src/app/halaqat/halaqat-list/halaqat-list.page.ts ***!
  \***********************************************************/
/*! exports provided: HalaqatListPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HalaqatListPage", function() { return HalaqatListPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var src_app_services_halaqa_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/halaqa.service */ "./src/app/services/halaqa.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_app_services_users_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/users.service */ "./src/app/services/users.service.ts");
/* harmony import */ var src_app_services_communication_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/communication.service */ "./src/app/services/communication.service.ts");







let HalaqatListPage = class HalaqatListPage {
    constructor(halaqaService, router, userService, activatedRoute, alertCtrl, modalController, loadingController, communicationService) {
        this.halaqaService = halaqaService;
        this.router = router;
        this.userService = userService;
        this.activatedRoute = activatedRoute;
        this.alertCtrl = alertCtrl;
        this.modalController = modalController;
        this.loadingController = loadingController;
        this.communicationService = communicationService;
    }
    ngOnInit() {
        this.activatedRoute.params.subscribe((params) => {
            if (params !== {} && params.id !== undefined) {
                this.InstitutId = this.communicationService.InstitutId = params.id;
            }
            else {
                console.log('error id not found');
            }
        });
        this.router.events.subscribe(res => {
            if (res instanceof _angular_router__WEBPACK_IMPORTED_MODULE_3__["NavigationEnd"]) {
                this.getAllCheickHalaqatById();
            }
        });
        this.getConnectdUserId();
        this.setUserRole();
    }
    setUserRole() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            this.connectedUserRole = yield this.userService.getUserRole();
        });
    }
    getConnectdUserId() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            this.connectedUserId = yield this.userService.getConnectedUserId();
            console.log('user id ', this.connectedUserId);
        });
    }
    goToAddStudents(halaqaId) {
        this.router.navigate(['students/students-list/' + halaqaId]);
    }
    goToAddHalaqa() {
        this.router.navigate(['halaqat/add-halaqa/' + this.InstitutId]);
    }
    getAllCheickHalaqatById() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const loading = yield this.loadingController.create({
                message: 'Loading...'
            });
            yield loading.present();
            this.halaqaService.getAllCheickHalaqats(this.InstitutId, this.connectedUserId).subscribe(data => {
                if (data.code === '0000') {
                    console.log('data', data);
                    this.halaqatList = data.content;
                    loading.dismiss();
                }
                else {
                    console.log('error');
                    loading.dismiss();
                }
            }, err => {
                console.log('subscription error', err);
                loading.dismiss();
            });
        });
    }
    edithalaqa(halaqaId) {
        this.router.navigate(['halaqat/edit-halaqa/' + halaqaId]);
    }
    delete(halaqaId) {
        this.halaqaService.deleteHalaqa(halaqaId).subscribe(data => {
            if (data.code === '0000') {
                console.log('delete data', data);
            }
            else {
                console.log('error');
            }
        }, err => {
            console.log('error');
        });
    }
    Halaqadetails(halaqaId) {
        this.router.navigate(['halaqat/halaqa-details/' + halaqaId]);
    }
    presentConfirmDelete(halaqaId) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const alert = this.alertCtrl.create({
                message: 'Do you want to delet this halaqa',
                buttons: [
                    {
                        text: 'Cancel',
                        role: 'cancel',
                        handler: () => {
                            console.log('Cancel clicked');
                        }
                    },
                    {
                        text: 'Confirm',
                        handler: () => {
                            this.delete(halaqaId);
                            this.ReloadList();
                        }
                    }
                ]
            });
            (yield alert).present();
        });
    }
    ReloadList() {
        this.getAllCheickHalaqatById();
    }
};
HalaqatListPage.ctorParameters = () => [
    { type: src_app_services_halaqa_service__WEBPACK_IMPORTED_MODULE_2__["HalaqaService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
    { type: src_app_services_users_service__WEBPACK_IMPORTED_MODULE_5__["UserService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"] },
    { type: src_app_services_communication_service__WEBPACK_IMPORTED_MODULE_6__["CommunicationService"] }
];
HalaqatListPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-halaqat-list',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./halaqat-list.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/halaqat/halaqat-list/halaqat-list.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./halaqat-list.page.scss */ "./src/app/halaqat/halaqat-list/halaqat-list.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_halaqa_service__WEBPACK_IMPORTED_MODULE_2__["HalaqaService"],
        _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
        src_app_services_users_service__WEBPACK_IMPORTED_MODULE_5__["UserService"],
        _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"],
        src_app_services_communication_service__WEBPACK_IMPORTED_MODULE_6__["CommunicationService"]])
], HalaqatListPage);



/***/ })

}]);
//# sourceMappingURL=halaqat-list-halaqat-list-module-es2015.js.map