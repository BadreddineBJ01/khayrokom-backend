(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["classe-list-classe-list-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/classes/classe-list/classe-list.page.html":
/*!*************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/classes/classe-list/classe-list.page.html ***!
  \*************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\r\n  <ion-toolbar>\r\n    <ion-title>classe-list</ion-title>\r\n    <ion-buttons slot=\"start\">\r\n        <ion-menu-button autoHide=\"false\"></ion-menu-button>\r\n        <ion-back-button></ion-back-button>\r\n      </ion-buttons>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content padding>\r\n  <ion-button\r\n  *ngIf=\"connectedUserRole == 'admin'\"\r\n    shape=\"round\"\r\n    expand=\"block\"\r\n    (click)=\"goToAddClass()\"\r\n    routerDirection=\"root\">\r\n    <ion-icon slot=\"icon-only\" name=\"add\"></ion-icon>\r\n    Add classes\r\n  </ion-button>\r\n\r\n  <ion-card>\r\n    <ion-item-sliding *ngFor=\"let classe of classesList\">\r\n      <ion-item>\r\n        <ion-label>\r\n          {{classe.Classname}}\r\n        </ion-label>\r\n      </ion-item>\r\n      <ion-item-options>\r\n        <ion-item-option color=\"primary\" (click)=\"Classesdetails(classe._id)\">\r\n          <ion-icon slot=\"top\" name=\"more\"></ion-icon>\r\n          details\r\n        </ion-item-option>\r\n        <ion-item-option color=\"secondary\" \r\n        *ngIf=\"connectedUserRole == 'admin'\"\r\n        (click)=\"editClasses(classe._id)\">\r\n          <ion-icon slot=\"top\" name=\"create\"></ion-icon>\r\n          edit\r\n        </ion-item-option>\r\n        <ion-item-option\r\n         *ngIf=\"connectedUserRole == 'admin'\"\r\n          color=\"danger\"\r\n          (click)=\"presentConfirmDelete(classe._id)\">\r\n          <ion-icon slot=\"top\" name=\"trash\"></ion-icon>\r\n          trash\r\n        </ion-item-option>\r\n      </ion-item-options>\r\n    </ion-item-sliding>\r\n    </ion-card>\r\n\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/classes/classe-list/classe-list-routing.module.ts":
/*!*******************************************************************!*\
  !*** ./src/app/classes/classe-list/classe-list-routing.module.ts ***!
  \*******************************************************************/
/*! exports provided: ClasseListPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ClasseListPageRoutingModule", function() { return ClasseListPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _classe_list_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./classe-list.page */ "./src/app/classes/classe-list/classe-list.page.ts");




const routes = [
    {
        path: '',
        component: _classe_list_page__WEBPACK_IMPORTED_MODULE_3__["ClasseListPage"]
    }
];
let ClasseListPageRoutingModule = class ClasseListPageRoutingModule {
};
ClasseListPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], ClasseListPageRoutingModule);



/***/ }),

/***/ "./src/app/classes/classe-list/classe-list.module.ts":
/*!***********************************************************!*\
  !*** ./src/app/classes/classe-list/classe-list.module.ts ***!
  \***********************************************************/
/*! exports provided: ClasseListPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ClasseListPageModule", function() { return ClasseListPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _classe_list_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./classe-list-routing.module */ "./src/app/classes/classe-list/classe-list-routing.module.ts");
/* harmony import */ var _classe_list_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./classe-list.page */ "./src/app/classes/classe-list/classe-list.page.ts");







let ClasseListPageModule = class ClasseListPageModule {
};
ClasseListPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _classe_list_routing_module__WEBPACK_IMPORTED_MODULE_5__["ClasseListPageRoutingModule"]
        ],
        declarations: [_classe_list_page__WEBPACK_IMPORTED_MODULE_6__["ClasseListPage"]]
    })
], ClasseListPageModule);



/***/ }),

/***/ "./src/app/classes/classe-list/classe-list.page.scss":
/*!***********************************************************!*\
  !*** ./src/app/classes/classe-list/classe-list.page.scss ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NsYXNzZXMvY2xhc3NlLWxpc3QvY2xhc3NlLWxpc3QucGFnZS5zY3NzIn0= */");

/***/ }),

/***/ "./src/app/classes/classe-list/classe-list.page.ts":
/*!*********************************************************!*\
  !*** ./src/app/classes/classe-list/classe-list.page.ts ***!
  \*********************************************************/
/*! exports provided: ClasseListPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ClasseListPage", function() { return ClasseListPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var src_app_services_classes_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/classes.service */ "./src/app/services/classes.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_app_services_users_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/users.service */ "./src/app/services/users.service.ts");






let ClasseListPage = class ClasseListPage {
    constructor(classesService, router, userService, alertCtrl, modalController, loadingController, activatedRoute) {
        this.classesService = classesService;
        this.router = router;
        this.userService = userService;
        this.alertCtrl = alertCtrl;
        this.modalController = modalController;
        this.loadingController = loadingController;
        this.activatedRoute = activatedRoute;
    }
    ngOnInit() {
        this.activatedRoute.params.subscribe((params) => {
            if (params !== {} && params.id !== undefined) {
                this.InstitutId = params.id;
            }
            else {
            }
        });
        this.setUserRole();
        this.router.events.subscribe((res) => {
            if (res instanceof _angular_router__WEBPACK_IMPORTED_MODULE_3__["NavigationEnd"]) {
                this.getAllInstitutClassesById();
            }
        });
    }
    setUserRole() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            this.connectedUserRole = yield this.userService.getUserRole();
        });
    }
    goToAddClass() {
        this.router.navigate(['classes/add-class/', this.InstitutId]);
    }
    getAllInstitutClassesById() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const loading = yield this.loadingController.create({
                message: 'Loading...',
            });
            yield loading.present();
            this.classesService.getAllInstitutClassesById(this.InstitutId).subscribe((data) => {
                if (data.code === '0000') {
                    this.classesList = data.content;
                    loading.dismiss();
                }
                else {
                    loading.dismiss();
                }
            }, (err) => {
                loading.dismiss();
            });
        });
    }
    editClasses(classId) {
        this.router.navigate(['classes/edit-class/' + classId]);
    }
    delete(classId) {
        this.classesService.deleteClass(classId).subscribe((data) => {
            if (data.code === '0000') {
            }
            else {
                console.log('error');
            }
        }, (err) => {
            console.log('error');
        });
    }
    Classesdetails(classId) {
        this.router.navigate(['classes/class-details/' + classId]);
    }
    presentConfirmDelete(classId) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const alert = this.alertCtrl.create({
                message: 'Do you want to delete this classe',
                buttons: [
                    {
                        text: 'Cancel',
                        role: 'cancel',
                        handler: () => {
                            console.log('Cancel clicked');
                        },
                    },
                    {
                        text: 'Confirm',
                        handler: () => {
                            this.delete(classId);
                            this.ReloadList();
                        },
                    },
                ],
            });
            (yield alert).present();
        });
    }
    ReloadList() {
        this.getAllInstitutClassesById();
    }
};
ClasseListPage.ctorParameters = () => [
    { type: src_app_services_classes_service__WEBPACK_IMPORTED_MODULE_2__["ClassesService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
    { type: src_app_services_users_service__WEBPACK_IMPORTED_MODULE_5__["UserService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"] }
];
ClasseListPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-classe-list',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./classe-list.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/classes/classe-list/classe-list.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./classe-list.page.scss */ "./src/app/classes/classe-list/classe-list.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_classes_service__WEBPACK_IMPORTED_MODULE_2__["ClassesService"],
        _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
        src_app_services_users_service__WEBPACK_IMPORTED_MODULE_5__["UserService"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"],
        _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"]])
], ClasseListPage);



/***/ })

}]);
//# sourceMappingURL=classe-list-classe-list-module-es2015.js.map