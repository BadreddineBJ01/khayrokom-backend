function _createSuper(Derived) { return function () { var Super = _getPrototypeOf(Derived), result; if (_isNativeReflectConstruct()) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (typeof call === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["add-institut-add-institut-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/institutes/add-institut/add-institut.page.html":
  /*!******************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/institutes/add-institut/add-institut.page.html ***!
    \******************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppInstitutesAddInstitutAddInstitutPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\r\n  <ion-toolbar>\r\n    <ion-title *ngIf='!EditInstitutInfo'>Add Institut</ion-title>\r\n    <ion-title *ngIf='EditInstitutInfo'>Edit institut</ion-title>\r\n    <ion-buttons slot=\"start\">\r\n    <ion-menu-button autoHide=\"false\"></ion-menu-button>\r\n    <ion-back-button></ion-back-button>\r\n  </ion-buttons>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <ion-card>\r\n  <form  [formGroup]=\"InstitutForm\">\r\n    <ion-item lines=\"inset\">\r\n      <ion-input  formControlName=\"InstituteName\" placeholder='name *' required></ion-input>\r\n    </ion-item>\r\n    <div>\r\n      <ng-container *ngFor=\"let error of error_messages.InstituteName\">\r\n        <ion-text class=\"ion-padding\" \r\n                  color=\"danger\"\r\n                  *ngIf=\"InstitutForm.get('InstituteName').hasError(error.type) && (InstitutForm.get('InstituteName').dirty || InstitutForm.get('InstituteName').touched)\">\r\n          {{ error.message }}\r\n        </ion-text>\r\n      </ng-container>\r\n    </div>\r\n        <ion-select lines=\"inset\" formControlName=\"country\" placeholder=\"country\">\r\n            <ion-select-option\r\n              *ngFor=\"let country of countriesArray\"\r\n              value=\"{{country.id}}\">{{country.name}}</ion-select-option>\r\n            </ion-select>\r\n    <ion-item lines=\"inset\">\r\n      <ion-input formControlName=\"city\" placeholder='city'>\r\n      </ion-input>\r\n    </ion-item>\r\n  </form>\r\n  <ion-button  \r\n  shape=\"round\"\r\n  expand=\"block\" \r\n  *ngIf='!EditInstitutInfo' \r\n  (click)='addInstitut(InstitutForm)'\r\n   [disabled]='!InstitutForm.valid'>submit</ion-button>\r\n  <ion-button  \r\n   shape=\"round\"\r\n  expand=\"block\"  \r\n  *ngIf='EditInstitutInfo' \r\n  (click)='UpdateInstitut(InstitutForm)' \r\n  [disabled]='!InstitutForm.valid'>Update</ion-button>  \r\n</ion-card>\r\n</ion-content>\r\n";
    /***/
  },

  /***/
  "./src/app/institutes/add-institut/add-institut-routing.module.ts":
  /*!************************************************************************!*\
    !*** ./src/app/institutes/add-institut/add-institut-routing.module.ts ***!
    \************************************************************************/

  /*! exports provided: AddInstitutPageRoutingModule */

  /***/
  function srcAppInstitutesAddInstitutAddInstitutRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AddInstitutPageRoutingModule", function () {
      return AddInstitutPageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _add_institut_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./add-institut.page */
    "./src/app/institutes/add-institut/add-institut.page.ts");

    var routes = [{
      path: '',
      component: _add_institut_page__WEBPACK_IMPORTED_MODULE_3__["AddInstitutPage"]
    }];

    var AddInstitutPageRoutingModule = function AddInstitutPageRoutingModule() {
      _classCallCheck(this, AddInstitutPageRoutingModule);
    };

    AddInstitutPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], AddInstitutPageRoutingModule);
    /***/
  },

  /***/
  "./src/app/institutes/add-institut/add-institut.module.ts":
  /*!****************************************************************!*\
    !*** ./src/app/institutes/add-institut/add-institut.module.ts ***!
    \****************************************************************/

  /*! exports provided: AddInstitutPageModule */

  /***/
  function srcAppInstitutesAddInstitutAddInstitutModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AddInstitutPageModule", function () {
      return AddInstitutPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _add_institut_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ./add-institut-routing.module */
    "./src/app/institutes/add-institut/add-institut-routing.module.ts");
    /* harmony import */


    var _add_institut_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./add-institut.page */
    "./src/app/institutes/add-institut/add-institut.page.ts");
    /* harmony import */


    var src_app_shared_module_shared_module_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! src/app/shared-module/shared-module.module */
    "./src/app/shared-module/shared-module.module.ts");

    var AddInstitutPageModule = function AddInstitutPageModule() {
      _classCallCheck(this, AddInstitutPageModule);
    };

    AddInstitutPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], src_app_shared_module_shared_module_module__WEBPACK_IMPORTED_MODULE_6__["SharedModuleModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["IonicModule"], _add_institut_routing_module__WEBPACK_IMPORTED_MODULE_4__["AddInstitutPageRoutingModule"]],
      declarations: [_add_institut_page__WEBPACK_IMPORTED_MODULE_5__["AddInstitutPage"]]
    })], AddInstitutPageModule);
    /***/
  },

  /***/
  "./src/app/institutes/add-institut/add-institut.page.scss":
  /*!****************************************************************!*\
    !*** ./src/app/institutes/add-institut/add-institut.page.scss ***!
    \****************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppInstitutesAddInstitutAddInstitutPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2luc3RpdHV0ZXMvYWRkLWluc3RpdHV0L2FkZC1pbnN0aXR1dC5wYWdlLnNjc3MifQ== */";
    /***/
  },

  /***/
  "./src/app/institutes/add-institut/add-institut.page.ts":
  /*!**************************************************************!*\
    !*** ./src/app/institutes/add-institut/add-institut.page.ts ***!
    \**************************************************************/

  /*! exports provided: AddInstitutPage */

  /***/
  function srcAppInstitutesAddInstitutAddInstitutPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AddInstitutPage", function () {
      return AddInstitutPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var src_app_services_institut_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! src/app/services/institut.service */
    "./src/app/services/institut.service.ts");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _models_Institut_model__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ../models/Institut.model */
    "./src/app/institutes/models/Institut.model.ts");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _models_Countries__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ../models/Countries */
    "./src/app/institutes/models/Countries.ts");

    var AddInstitutPage = /*#__PURE__*/function () {
      function AddInstitutPage(toastController, formBuilder, router, institutService, activatedRoute) {
        _classCallCheck(this, AddInstitutPage);

        this.toastController = toastController;
        this.formBuilder = formBuilder;
        this.router = router;
        this.institutService = institutService;
        this.activatedRoute = activatedRoute;
        this.InstitutToEdit = new _models_Institut_model__WEBPACK_IMPORTED_MODULE_5__["InstitutModel"]();
        this.EditInstitutInfo = false;
        this.countriesArray = [];
        this.error_messages = {
          InstituteName: [{
            type: 'required',
            message: 'Institut name is required.'
          }]
        };
      }

      _createClass(AddInstitutPage, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          var _this = this;

          this.loadAllCountries();
          this.initForm();
          this.activatedRoute.params.subscribe(function (params) {
            console.log('params', params);

            if (params !== {} && params.id !== undefined) {
              _this.GetInstitutById(params.id);

              _this.insitutId = params.id;
              _this.EditInstitutInfo = true;
            } else {
              _this.EditInstitutInfo = false;

              _this.initForm();
            }
          });
        }
      }, {
        key: "loadAllCountries",
        value: function loadAllCountries() {
          this.countriesArray = _models_Countries__WEBPACK_IMPORTED_MODULE_7__["countries"];
        }
      }, {
        key: "GetInstitutById",
        value: function GetInstitutById(InstitutId) {
          var _this2 = this;

          console.log('params --->', InstitutId);
          this.institutService.getInstitutById(InstitutId).subscribe(function (data) {
            if (data.code === '0000') {
              console.log('data content', data.content[0]);
              _this2.InstitutToEdit = new _models_Institut_model__WEBPACK_IMPORTED_MODULE_5__["InstitutModel"](data.content[0]);
              console.log('this.InstitutToEdit', _this2.InstitutToEdit);

              _this2.initForm();
            } else {
              console.log('error', data);
            }
          }, function (err) {
            console.log('error', err);
          });
        }
      }, {
        key: "initForm",
        value: function initForm() {
          this.InstitutForm = this.formBuilder.group({
            InstituteName: [this.InstitutToEdit.InstituteName, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            country: [this.InstitutToEdit.country],
            city: [this.InstitutToEdit.city]
          });
        }
      }, {
        key: "addInstitut",
        value: function addInstitut(InstitutForm) {
          var _this3 = this;

          console.log('ngForm', InstitutForm.value);
          this.institutService.addInstitut(InstitutForm.value).subscribe(function (data) {
            if (data.code === '0000') {
              _this3.presentToast(data.message, 'success');

              console.log('institut added with success');

              _this3.GoBackToList();
            } else {
              _this3.presentToast(data.message, 'error');

              console.log('error');
            }
          }, function (err) {
            _this3.presentToast(err, 'error');

            console.log('error', err);
          });
        }
      }, {
        key: "UpdateInstitut",
        value: function UpdateInstitut(InstitutForm) {
          var _this4 = this;

          console.log('InstittForm.value', InstitutForm.value, this.insitutId);
          this.institutService.UpdateInstitut(this.insitutId, InstitutForm.value).subscribe(function (data) {
            if (data.code === '0000') {
              _this4.presentToast(data.message, 'success');

              console.log('institut updated with success');

              _this4.GoBackToList();
            } else {
              _this4.presentToast(data.message, 'error');

              console.log('error update');
            }
          }, function (err) {
            _this4.presentToast(err, 'error');

            console.log('error', err);
          });
        }
      }, {
        key: "GoBackToList",
        value: function GoBackToList() {
          this.router.navigate(['/institutes/institues-list']);
        }
      }, {
        key: "presentToast",
        value: function presentToast(msg, messagetype) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
            var toast;
            return regeneratorRuntime.wrap(function _callee$(_context) {
              while (1) {
                switch (_context.prev = _context.next) {
                  case 0:
                    _context.next = 2;
                    return this.toastController.create({
                      message: msg,
                      duration: 3000,
                      position: 'bottom',
                      color: messagetype === 'success' ? 'success' : 'danger'
                    });

                  case 2:
                    toast = _context.sent;
                    toast.present();

                  case 4:
                  case "end":
                    return _context.stop();
                }
              }
            }, _callee, this);
          }));
        }
      }]);

      return AddInstitutPage;
    }();

    AddInstitutPage.ctorParameters = function () {
      return [{
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["ToastController"]
      }, {
        type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]
      }, {
        type: src_app_services_institut_service__WEBPACK_IMPORTED_MODULE_3__["InstitutService"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"]
      }];
    };

    AddInstitutPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-add-institut',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./add-institut.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/institutes/add-institut/add-institut.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./add-institut.page.scss */
      "./src/app/institutes/add-institut/add-institut.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_6__["ToastController"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"], src_app_services_institut_service__WEBPACK_IMPORTED_MODULE_3__["InstitutService"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"]])], AddInstitutPage);
    /***/
  },

  /***/
  "./src/app/institutes/models/Institut.model.ts":
  /*!*****************************************************!*\
    !*** ./src/app/institutes/models/Institut.model.ts ***!
    \*****************************************************/

  /*! exports provided: InstitutModel */

  /***/
  function srcAppInstitutesModelsInstitutModelTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "InstitutModel", function () {
      return InstitutModel;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _sharedModels_sharedModel__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! ../../sharedModels/sharedModel */
    "./src/app/sharedModels/sharedModel.ts");

    var InstitutModel = /*#__PURE__*/function (_sharedModels_sharedM) {
      _inherits(InstitutModel, _sharedModels_sharedM);

      var _super = _createSuper(InstitutModel);

      function InstitutModel(InstitutEntity) {
        var _this5;

        _classCallCheck(this, InstitutModel);

        InstitutEntity = InstitutEntity || {};
        _this5 = _super.call(this, InstitutEntity);
        _this5.InstituteName = InstitutEntity.InstituteName || '';
        _this5.country = InstitutEntity.country || '';
        _this5.city = InstitutEntity.city || '';
        return _this5;
      }

      return InstitutModel;
    }(_sharedModels_sharedModel__WEBPACK_IMPORTED_MODULE_1__["sharedModel"]);
    /***/

  }
}]);
//# sourceMappingURL=add-institut-add-institut-module-es5.js.map