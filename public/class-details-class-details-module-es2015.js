(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["class-details-class-details-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/classes/class-details/class-details.page.html":
/*!*****************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/classes/class-details/class-details.page.html ***!
  \*****************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\r\n  <ion-toolbar>\r\n    <ion-title>class-details</ion-title>\r\n    <ion-buttons slot=\"start\">\r\n        <ion-menu-button autoHide=\"false\"></ion-menu-button>\r\n        <ion-back-button></ion-back-button>\r\n      </ion-buttons>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <ion-card *ngIf='classDetails'>\r\n    <ion-item>\r\n      <ion-label>name</ion-label>\r\n      <ion-label *ngIf='classDetails.Classname; else noData'>{{classDetails.Classname}}</ion-label>\r\n    </ion-item>\r\n  </ion-card>\r\n  \r\n<ng-template #noData>\r\n <ion-label>\r\n   N/A\r\n  </ion-label>\r\n</ng-template>\r\n\r\n</ion-content>");

/***/ }),

/***/ "./src/app/classes/class-details/class-details-routing.module.ts":
/*!***********************************************************************!*\
  !*** ./src/app/classes/class-details/class-details-routing.module.ts ***!
  \***********************************************************************/
/*! exports provided: ClassDetailsPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ClassDetailsPageRoutingModule", function() { return ClassDetailsPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _class_details_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./class-details.page */ "./src/app/classes/class-details/class-details.page.ts");




const routes = [
    {
        path: '',
        component: _class_details_page__WEBPACK_IMPORTED_MODULE_3__["ClassDetailsPage"]
    }
];
let ClassDetailsPageRoutingModule = class ClassDetailsPageRoutingModule {
};
ClassDetailsPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], ClassDetailsPageRoutingModule);



/***/ }),

/***/ "./src/app/classes/class-details/class-details.module.ts":
/*!***************************************************************!*\
  !*** ./src/app/classes/class-details/class-details.module.ts ***!
  \***************************************************************/
/*! exports provided: ClassDetailsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ClassDetailsPageModule", function() { return ClassDetailsPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _class_details_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./class-details-routing.module */ "./src/app/classes/class-details/class-details-routing.module.ts");
/* harmony import */ var _class_details_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./class-details.page */ "./src/app/classes/class-details/class-details.page.ts");







let ClassDetailsPageModule = class ClassDetailsPageModule {
};
ClassDetailsPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _class_details_routing_module__WEBPACK_IMPORTED_MODULE_5__["ClassDetailsPageRoutingModule"]
        ],
        declarations: [_class_details_page__WEBPACK_IMPORTED_MODULE_6__["ClassDetailsPage"]]
    })
], ClassDetailsPageModule);



/***/ }),

/***/ "./src/app/classes/class-details/class-details.page.scss":
/*!***************************************************************!*\
  !*** ./src/app/classes/class-details/class-details.page.scss ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NsYXNzZXMvY2xhc3MtZGV0YWlscy9jbGFzcy1kZXRhaWxzLnBhZ2Uuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/classes/class-details/class-details.page.ts":
/*!*************************************************************!*\
  !*** ./src/app/classes/class-details/class-details.page.ts ***!
  \*************************************************************/
/*! exports provided: ClassDetailsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ClassDetailsPage", function() { return ClassDetailsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var src_app_services_classes_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/classes.service */ "./src/app/services/classes.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");




let ClassDetailsPage = class ClassDetailsPage {
    constructor(activatedRoute, classesService) {
        this.activatedRoute = activatedRoute;
        this.classesService = classesService;
    }
    ngOnInit() {
        this.ClassId = this.activatedRoute.snapshot.paramMap.get('id');
        this.getClassById(this.ClassId);
    }
    getClassById(ClassId) {
        this.classesService.getClassById(ClassId).subscribe(data => {
            if (data.code === '0000') {
                console.log(data.content);
                this.classDetails = data.content[0];
            }
            else {
                console.log('error', data);
            }
        }, (err) => {
            console.log('error', err);
        });
    }
};
ClassDetailsPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"] },
    { type: src_app_services_classes_service__WEBPACK_IMPORTED_MODULE_2__["ClassesService"] }
];
ClassDetailsPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-class-details',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./class-details.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/classes/class-details/class-details.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./class-details.page.scss */ "./src/app/classes/class-details/class-details.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"],
        src_app_services_classes_service__WEBPACK_IMPORTED_MODULE_2__["ClassesService"]])
], ClassDetailsPage);



/***/ })

}]);
//# sourceMappingURL=class-details-class-details-module-es2015.js.map