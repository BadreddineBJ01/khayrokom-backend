function _createSuper(Derived) { return function () { var Super = _getPrototypeOf(Derived), result; if (_isNativeReflectConstruct()) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (typeof call === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["add-halaqa-add-halaqa-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/halaqat/add-halaqa/add-halaqa.page.html":
  /*!***********************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/halaqat/add-halaqa/add-halaqa.page.html ***!
    \***********************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppHalaqatAddHalaqaAddHalaqaPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\r\n  <ion-toolbar>\r\n    <ion-title *ngIf='!EditHalaqaInfo'>Add Halaqa</ion-title>\r\n    <ion-title *ngIf='EditHalaqaInfo'>Edit Halaqa</ion-title>\r\n    <ion-buttons slot=\"start\">\r\n    <ion-menu-button autoHide=\"false\"></ion-menu-button>\r\n    <ion-back-button></ion-back-button>\r\n  </ion-buttons>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <ion-card>\r\n  <form  [formGroup]=\"HalaqaForm\">\r\n    <ion-item lines=\"inset\">\r\n      <ion-input  formControlName=\"Halaqaname\" placeholder='name' required></ion-input>\r\n    </ion-item>\r\n    <div>\r\n      <ng-container *ngFor=\"let error of error_messages.Halaqaname\">\r\n        <ion-text class=\"ion-padding\" \r\n                  color=\"danger\"\r\n                  *ngIf=\"HalaqaForm.get('Halaqaname').hasError(error.type) && (HalaqaForm.get('Halaqaname').dirty || HalaqaForm.get('Halaqaname').touched)\">\r\n          {{ error.message }}\r\n        </ion-text>\r\n      </ng-container>\r\n    </div>\r\n  </form>\r\n  <ion-button  \r\n  shape=\"round\"\r\n  expand=\"block\" \r\n  *ngIf='!EditHalaqaInfo' \r\n  (click)='AddHalaqa(HalaqaForm)'\r\n   [disabled]='!HalaqaForm.valid'>submit</ion-button>\r\n  <ion-button  \r\n   shape=\"round\"\r\n  expand=\"block\"  \r\n  *ngIf='EditHalaqaInfo' \r\n  (click)='UpdateHalaqa(HalaqaForm)' \r\n  [disabled]='!HalaqaForm.valid'>Update</ion-button>  \r\n</ion-card>\r\n</ion-content>\r\n";
    /***/
  },

  /***/
  "./src/app/halaqat/add-halaqa/add-halaqa-routing.module.ts":
  /*!*****************************************************************!*\
    !*** ./src/app/halaqat/add-halaqa/add-halaqa-routing.module.ts ***!
    \*****************************************************************/

  /*! exports provided: AddHalaqaPageRoutingModule */

  /***/
  function srcAppHalaqatAddHalaqaAddHalaqaRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AddHalaqaPageRoutingModule", function () {
      return AddHalaqaPageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _add_halaqa_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./add-halaqa.page */
    "./src/app/halaqat/add-halaqa/add-halaqa.page.ts");

    var routes = [{
      path: '',
      component: _add_halaqa_page__WEBPACK_IMPORTED_MODULE_3__["AddHalaqaPage"]
    }];

    var AddHalaqaPageRoutingModule = function AddHalaqaPageRoutingModule() {
      _classCallCheck(this, AddHalaqaPageRoutingModule);
    };

    AddHalaqaPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], AddHalaqaPageRoutingModule);
    /***/
  },

  /***/
  "./src/app/halaqat/add-halaqa/add-halaqa.module.ts":
  /*!*********************************************************!*\
    !*** ./src/app/halaqat/add-halaqa/add-halaqa.module.ts ***!
    \*********************************************************/

  /*! exports provided: AddHalaqaPageModule */

  /***/
  function srcAppHalaqatAddHalaqaAddHalaqaModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AddHalaqaPageModule", function () {
      return AddHalaqaPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _add_halaqa_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./add-halaqa-routing.module */
    "./src/app/halaqat/add-halaqa/add-halaqa-routing.module.ts");
    /* harmony import */


    var _add_halaqa_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./add-halaqa.page */
    "./src/app/halaqat/add-halaqa/add-halaqa.page.ts");
    /* harmony import */


    var src_app_shared_module_shared_module_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! src/app/shared-module/shared-module.module */
    "./src/app/shared-module/shared-module.module.ts");

    var AddHalaqaPageModule = function AddHalaqaPageModule() {
      _classCallCheck(this, AddHalaqaPageModule);
    };

    AddHalaqaPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _add_halaqa_routing_module__WEBPACK_IMPORTED_MODULE_5__["AddHalaqaPageRoutingModule"], src_app_shared_module_shared_module_module__WEBPACK_IMPORTED_MODULE_7__["SharedModuleModule"]],
      declarations: [_add_halaqa_page__WEBPACK_IMPORTED_MODULE_6__["AddHalaqaPage"]]
    })], AddHalaqaPageModule);
    /***/
  },

  /***/
  "./src/app/halaqat/add-halaqa/add-halaqa.page.scss":
  /*!*********************************************************!*\
    !*** ./src/app/halaqat/add-halaqa/add-halaqa.page.scss ***!
    \*********************************************************/

  /*! exports provided: default */

  /***/
  function srcAppHalaqatAddHalaqaAddHalaqaPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2hhbGFxYXQvYWRkLWhhbGFxYS9hZGQtaGFsYXFhLnBhZ2Uuc2NzcyJ9 */";
    /***/
  },

  /***/
  "./src/app/halaqat/add-halaqa/add-halaqa.page.ts":
  /*!*******************************************************!*\
    !*** ./src/app/halaqat/add-halaqa/add-halaqa.page.ts ***!
    \*******************************************************/

  /*! exports provided: AddHalaqaPage */

  /***/
  function srcAppHalaqatAddHalaqaAddHalaqaPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AddHalaqaPage", function () {
      return AddHalaqaPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var src_app_services_halaqa_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! src/app/services/halaqa.service */
    "./src/app/services/halaqa.service.ts");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _models_Halaqa_model__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ../models/Halaqa.model */
    "./src/app/halaqat/models/Halaqa.model.ts");
    /* harmony import */


    var rxjs_operators__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! rxjs/operators */
    "./node_modules/rxjs/_esm2015/operators/index.js");
    /* harmony import */


    var src_app_services_users_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! src/app/services/users.service */
    "./src/app/services/users.service.ts");
    /* harmony import */


    var src_app_services_communication_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! src/app/services/communication.service */
    "./src/app/services/communication.service.ts");

    var AddHalaqaPage = /*#__PURE__*/function () {
      function AddHalaqaPage(userService, formBuilder, router, halaqaService, activatedRoute, communicationService) {
        _classCallCheck(this, AddHalaqaPage);

        this.userService = userService;
        this.formBuilder = formBuilder;
        this.router = router;
        this.halaqaService = halaqaService;
        this.activatedRoute = activatedRoute;
        this.communicationService = communicationService;
        this.HalaqaToEdit = new _models_Halaqa_model__WEBPACK_IMPORTED_MODULE_5__["HalaqaModel"]();
        this.EditHalaqaInfo = false; // Form error types

        this.error_messages = {
          Halaqaname: [{
            type: 'required',
            message: 'Halaqa name is required.'
          }]
        };
      }

      _createClass(AddHalaqaPage, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          var _this = this;

          this.InstitutId = this.communicationService.InstitutId;
          this.getConnectdUserId();
          this.initForm();
          this.router.events.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["switchMap"])(function (navigation) {
            return _this.activatedRoute.params.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["map"])(function (params) {
              return {
                navigation: navigation,
                params: params
              };
            }));
          })).subscribe(function (result) {
            if (result.navigation instanceof _angular_router__WEBPACK_IMPORTED_MODULE_4__["NavigationEnd"]) {
              var urlfragment = result.navigation.url.split('/')[2];

              if (urlfragment.toLowerCase().includes('add') && result.params.id !== undefined && result.params !== {}) {
                //   this.InstitutId = result.params.id;
                _this.EditHalaqaInfo = false;

                _this.initForm();
              } else if (urlfragment.toLowerCase().includes('edit') && result.params.id !== undefined && result.params !== {}) {
                _this.GetHalaqaById(result.params.id);

                _this.halaqaId = result.params.id;
                _this.EditHalaqaInfo = true;
              } else {
                console.log('error id undefined');
              }
            }
          });
        }
      }, {
        key: "getConnectdUserId",
        value: function getConnectdUserId() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
            return regeneratorRuntime.wrap(function _callee$(_context) {
              while (1) {
                switch (_context.prev = _context.next) {
                  case 0:
                    _context.next = 2;
                    return this.userService.getConnectedUserId();

                  case 2:
                    this.connectedUserId = _context.sent;
                    console.log('user id ', this.connectedUserId);

                  case 4:
                  case "end":
                    return _context.stop();
                }
              }
            }, _callee, this);
          }));
        }
      }, {
        key: "GetHalaqaById",
        value: function GetHalaqaById(HalaqaId) {
          var _this2 = this;

          console.log('params --->', HalaqaId);
          this.halaqaService.getHalaqaById(HalaqaId).subscribe(function (data) {
            if (data.code === '0000') {
              console.log('data content', data.content[0]);
              _this2.HalaqaToEdit = new _models_Halaqa_model__WEBPACK_IMPORTED_MODULE_5__["HalaqaModel"](data.content[0]);
              console.log('this.HalaqaToEdit', _this2.HalaqaToEdit);

              _this2.initForm();
            } else {
              console.log('error', data);
            }
          }, function (err) {
            console.log('error', err);
          });
        }
      }, {
        key: "initForm",
        value: function initForm() {
          this.HalaqaForm = this.formBuilder.group({
            Halaqaname: [this.HalaqaToEdit.Halaqaname, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]
          });
        }
      }, {
        key: "AddHalaqa",
        value: function AddHalaqa(HalaqaForm) {
          var _this3 = this;

          var HalaqaToAdd = {
            Halaqaname: '',
            InstitutId: '',
            CheikhId: ''
          };
          HalaqaToAdd.Halaqaname = HalaqaForm.value.Halaqaname;
          HalaqaToAdd.CheikhId = this.connectedUserId;
          HalaqaToAdd.InstitutId = this.InstitutId;
          console.log('ngForm', HalaqaToAdd);
          this.halaqaService.addHalaqa(HalaqaToAdd).subscribe(function (data) {
            if (data.code === '0000') {
              console.log('halaqa added with success');

              _this3.GoBackToList();
            } else {
              console.log('error');
            }
          }, function (err) {
            console.log('error', err);
          });
        }
      }, {
        key: "UpdateHalaqa",
        value: function UpdateHalaqa(HalaqaForm) {
          var _this4 = this;

          var HalaqaToAdd = {
            Halaqaname: '',
            InstitutId: '',
            CheikhId: ''
          };
          HalaqaToAdd.Halaqaname = HalaqaForm.value.Halaqaname;
          HalaqaToAdd.CheikhId = this.connectedUserId;
          HalaqaToAdd.InstitutId = this.InstitutId;
          console.log('HalaqaForm.value', HalaqaToAdd, this.halaqaId);
          this.halaqaService.updateHalaqa(this.halaqaId, HalaqaToAdd).subscribe(function (data) {
            if (data.code === '0000') {
              console.log('halaqa updated with success');

              _this4.GoBackToList();
            } else {
              console.log('error update');
            }
          }, function (err) {
            console.log('error', err);
          });
        }
      }, {
        key: "GoBackToList",
        value: function GoBackToList() {
          this.router.navigate(['/halaqat/halaqat-list/' + this.InstitutId]);
        }
      }]);

      return AddHalaqaPage;
    }();

    AddHalaqaPage.ctorParameters = function () {
      return [{
        type: src_app_services_users_service__WEBPACK_IMPORTED_MODULE_7__["UserService"]
      }, {
        type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]
      }, {
        type: src_app_services_halaqa_service__WEBPACK_IMPORTED_MODULE_3__["HalaqaService"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"]
      }, {
        type: src_app_services_communication_service__WEBPACK_IMPORTED_MODULE_8__["CommunicationService"]
      }];
    };

    AddHalaqaPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-add-halaqa',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./add-halaqa.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/halaqat/add-halaqa/add-halaqa.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./add-halaqa.page.scss */
      "./src/app/halaqat/add-halaqa/add-halaqa.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_users_service__WEBPACK_IMPORTED_MODULE_7__["UserService"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"], src_app_services_halaqa_service__WEBPACK_IMPORTED_MODULE_3__["HalaqaService"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"], src_app_services_communication_service__WEBPACK_IMPORTED_MODULE_8__["CommunicationService"]])], AddHalaqaPage);
    /***/
  },

  /***/
  "./src/app/halaqat/models/Halaqa.model.ts":
  /*!************************************************!*\
    !*** ./src/app/halaqat/models/Halaqa.model.ts ***!
    \************************************************/

  /*! exports provided: HalaqaModel */

  /***/
  function srcAppHalaqatModelsHalaqaModelTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "HalaqaModel", function () {
      return HalaqaModel;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _sharedModels_sharedModel__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! ../../sharedModels/sharedModel */
    "./src/app/sharedModels/sharedModel.ts");

    var HalaqaModel = /*#__PURE__*/function (_sharedModels_sharedM) {
      _inherits(HalaqaModel, _sharedModels_sharedM);

      var _super = _createSuper(HalaqaModel);

      function HalaqaModel(HalaqaEntity) {
        var _this5;

        _classCallCheck(this, HalaqaModel);

        HalaqaEntity = HalaqaEntity || {};
        _this5 = _super.call(this, HalaqaEntity);
        _this5.Halaqaname = HalaqaEntity.Halaqaname || '';
        return _this5;
      }

      return HalaqaModel;
    }(_sharedModels_sharedModel__WEBPACK_IMPORTED_MODULE_1__["sharedModel"]);
    /***/

  }
}]);
//# sourceMappingURL=add-halaqa-add-halaqa-module-es5.js.map