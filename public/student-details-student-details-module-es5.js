function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["student-details-student-details-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/students/student-details/student-details.page.html":
  /*!**********************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/students/student-details/student-details.page.html ***!
    \**********************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppStudentsStudentDetailsStudentDetailsPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\r\n    <ion-toolbar>\r\n      <ion-title>Student details</ion-title>\r\n      <ion-buttons slot=\"start\">\r\n          <ion-menu-button autoHide=\"true\"></ion-menu-button>\r\n          <ion-back-button></ion-back-button>\r\n        </ion-buttons>\r\n    </ion-toolbar>\r\n  </ion-header>\r\n  \r\n  <ion-content>\r\n  \r\n    <ion-card *ngIf='studentDetails'>\r\n      <ion-item>\r\n        <ion-label>firstname</ion-label>\r\n        <ion-label *ngIf='studentDetails.firstname; else noData'>{{studentDetails.firstname}}</ion-label>\r\n      </ion-item>\r\n    \r\n      <ion-item>\r\n        <ion-label>lastname</ion-label>\r\n        <ion-label *ngIf='studentDetails.lastname; else noData'>{{studentDetails.lastname}}</ion-label>\r\n      </ion-item>\r\n    \r\n      <ion-item>\r\n        <ion-label>gender</ion-label>\r\n        <ion-label *ngIf='studentDetails.gender; else noData'>{{studentDetails.gender}}</ion-label>\r\n      </ion-item>\r\n    <ion-item>\r\n        <ion-label>status</ion-label>\r\n        <ion-label *ngIf='studentDetails.status; else noData'>{{studentDetails.status}}</ion-label>\r\n      </ion-item>\r\n  \r\n      <ion-item *ngIf='studentDetails.roomNumber && studentDetails.status == \"M\"'>\r\n        <ion-label>roomNumber</ion-label>\r\n        <ion-label>{{studentDetails.roomNumber}} </ion-label>\r\n      </ion-item>\r\n    \r\n      <ion-item *ngIf='studentDetails.level && studentDetails.status == \"M\"'>\r\n        <ion-label>level</ion-label>\r\n        <ion-label>{{studentDetails.level}}</ion-label>\r\n      </ion-item>\r\n  \r\n    <ion-item *ngIf='studentDetails.entryDate && studentDetails.status == \"M\"'>\r\n        <ion-label>entryDate</ion-label>\r\n        <ion-label>{{studentDetails.entryDate}}</ion-label>\r\n      </ion-item>\r\n    \r\n      <ion-item *ngIf='studentDetails.exitDate && studentDetails.status == \"M\"'>\r\n        <ion-label>exitDate</ion-label>\r\n        <ion-label>{{studentDetails.exitDate}}</ion-label>\r\n      </ion-item>\r\n    \r\n      <ion-item>\r\n        <ion-label>created_at</ion-label>\r\n        <ion-label *ngIf='studentDetails.created_at; else noData'>{{studentDetails.created_at | date }}</ion-label>\r\n      </ion-item>\r\n    </ion-card>\r\n  <ng-template #noData>\r\n   <ion-label>\r\n     N/A\r\n    </ion-label>\r\n  </ng-template>\r\n  </ion-content>\r\n  ";
    /***/
  },

  /***/
  "./src/app/students/student-details/student-details-routing.module.ts":
  /*!****************************************************************************!*\
    !*** ./src/app/students/student-details/student-details-routing.module.ts ***!
    \****************************************************************************/

  /*! exports provided: StudentDetailsPageRoutingModule */

  /***/
  function srcAppStudentsStudentDetailsStudentDetailsRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "StudentDetailsPageRoutingModule", function () {
      return StudentDetailsPageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _student_details_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./student-details.page */
    "./src/app/students/student-details/student-details.page.ts");

    var routes = [{
      path: '',
      component: _student_details_page__WEBPACK_IMPORTED_MODULE_3__["StudentDetailsPage"]
    }];

    var StudentDetailsPageRoutingModule = function StudentDetailsPageRoutingModule() {
      _classCallCheck(this, StudentDetailsPageRoutingModule);
    };

    StudentDetailsPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], StudentDetailsPageRoutingModule);
    /***/
  },

  /***/
  "./src/app/students/student-details/student-details.module.ts":
  /*!********************************************************************!*\
    !*** ./src/app/students/student-details/student-details.module.ts ***!
    \********************************************************************/

  /*! exports provided: StudentDetailsPageModule */

  /***/
  function srcAppStudentsStudentDetailsStudentDetailsModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "StudentDetailsPageModule", function () {
      return StudentDetailsPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _student_details_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./student-details-routing.module */
    "./src/app/students/student-details/student-details-routing.module.ts");
    /* harmony import */


    var _student_details_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./student-details.page */
    "./src/app/students/student-details/student-details.page.ts");

    var StudentDetailsPageModule = function StudentDetailsPageModule() {
      _classCallCheck(this, StudentDetailsPageModule);
    };

    StudentDetailsPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _student_details_routing_module__WEBPACK_IMPORTED_MODULE_5__["StudentDetailsPageRoutingModule"]],
      declarations: [_student_details_page__WEBPACK_IMPORTED_MODULE_6__["StudentDetailsPage"]]
    })], StudentDetailsPageModule);
    /***/
  },

  /***/
  "./src/app/students/student-details/student-details.page.scss":
  /*!********************************************************************!*\
    !*** ./src/app/students/student-details/student-details.page.scss ***!
    \********************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppStudentsStudentDetailsStudentDetailsPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3N0dWRlbnRzL3N0dWRlbnQtZGV0YWlscy9zdHVkZW50LWRldGFpbHMucGFnZS5zY3NzIn0= */";
    /***/
  },

  /***/
  "./src/app/students/student-details/student-details.page.ts":
  /*!******************************************************************!*\
    !*** ./src/app/students/student-details/student-details.page.ts ***!
    \******************************************************************/

  /*! exports provided: StudentDetailsPage */

  /***/
  function srcAppStudentsStudentDetailsStudentDetailsPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "StudentDetailsPage", function () {
      return StudentDetailsPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var src_app_services_Students_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! src/app/services/Students.service */
    "./src/app/services/Students.service.ts");
    /* harmony import */


    var src_app_services_classes_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! src/app/services/classes.service */
    "./src/app/services/classes.service.ts");

    var StudentDetailsPage = /*#__PURE__*/function () {
      function StudentDetailsPage(activatedRoute, studentsService, classesService) {
        _classCallCheck(this, StudentDetailsPage);

        this.activatedRoute = activatedRoute;
        this.studentsService = studentsService;
        this.classesService = classesService;
      }

      _createClass(StudentDetailsPage, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          this.StudentId = this.activatedRoute.snapshot.paramMap.get('id');
          this.getStudentById(this.StudentId); //this.studentClass = 
        }
      }, {
        key: "getStudentById",
        value: function getStudentById(StudentId) {
          var _this = this;

          this.studentsService.getStudentById(StudentId).subscribe(function (data) {
            if (data.code === '0000') {
              console.log(data.content);
              _this.studentDetails = data.content[0];

              _this.getClassById(_this.studentDetails.classId);
            } else {
              console.log('error', data);
            }
          }, function (err) {
            console.log('error', err);
          });
        }
      }, {
        key: "getClassById",
        value: function getClassById(ClassId) {
          var _this2 = this;

          this.classesService.getClassById(ClassId).subscribe(function (data) {
            console.log("Msg", data);

            if (data.code === '0000') {
              console.log(data.content);
              _this2.studentClass = data.content[0];
            } else {
              console.log('error', data);
            }
          }, function (err) {
            console.log('error', err);
          });
        }
      }]);

      return StudentDetailsPage;
    }();

    StudentDetailsPage.ctorParameters = function () {
      return [{
        type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]
      }, {
        type: src_app_services_Students_service__WEBPACK_IMPORTED_MODULE_3__["StudentsService"]
      }, {
        type: src_app_services_classes_service__WEBPACK_IMPORTED_MODULE_4__["ClassesService"]
      }];
    };

    StudentDetailsPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-student-details',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./student-details.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/students/student-details/student-details.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./student-details.page.scss */
      "./src/app/students/student-details/student-details.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"], src_app_services_Students_service__WEBPACK_IMPORTED_MODULE_3__["StudentsService"], src_app_services_classes_service__WEBPACK_IMPORTED_MODULE_4__["ClassesService"]])], StudentDetailsPage);
    /***/
  }
}]);
//# sourceMappingURL=student-details-student-details-module-es5.js.map