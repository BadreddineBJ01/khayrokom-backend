function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["users-list-users-list-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/users/users-list/users-list.page.html":
  /*!*********************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/users/users-list/users-list.page.html ***!
    \*********************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppUsersUsersListUsersListPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\r\n  <ion-toolbar>\r\n    <ion-title>users-list</ion-title>\r\n    <ion-buttons slot=\"start\">\r\n        <ion-menu-button autoHide=\"false\"></ion-menu-button>\r\n        <ion-back-button></ion-back-button>\r\n      </ion-buttons>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content padding>\r\n  <ion-button\r\n    shape=\"round\"\r\n    expand=\"block\"\r\n    routerLink=\"/users/add-user\"\r\n    routerDirection=\"root\">\r\n    <ion-icon slot=\"icon-only\" name=\"add\"></ion-icon>\r\n    Add user\r\n  </ion-button>\r\n\r\n  <ion-card>\r\n  <ion-item-sliding *ngFor=\"let user of usersList\">\r\n    <ion-item>\r\n      <ion-label>\r\n        {{user.firstname}} {{user.lastname}}\r\n      </ion-label>\r\n    </ion-item>\r\n    <ion-item-options>\r\n      <ion-item-option color=\"primary\" (click)=\"userdetails(user._id)\">\r\n        <ion-icon slot=\"top\" name=\"more\"></ion-icon>\r\n        details\r\n      </ion-item-option>\r\n      <ion-item-option color=\"secondary\" (click)=\"editUser(user._id)\">\r\n        <ion-icon slot=\"top\" name=\"create\"></ion-icon>\r\n        edit\r\n      </ion-item-option>\r\n      <ion-item-option *ngIf=\"user.role != 'admin'\"\r\n        color=\"danger\"\r\n        (click)=\"presentConfirmDelete(user._id)\"\r\n      >\r\n        <ion-icon slot=\"top\" name=\"trash\"></ion-icon>\r\n        trash\r\n      </ion-item-option>\r\n    </ion-item-options>\r\n  </ion-item-sliding>\r\n  </ion-card>\r\n</ion-content>\r\n";
    /***/
  },

  /***/
  "./src/app/users/users-list/users-list-routing.module.ts":
  /*!***************************************************************!*\
    !*** ./src/app/users/users-list/users-list-routing.module.ts ***!
    \***************************************************************/

  /*! exports provided: UsersListPageRoutingModule */

  /***/
  function srcAppUsersUsersListUsersListRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "UsersListPageRoutingModule", function () {
      return UsersListPageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _users_list_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./users-list.page */
    "./src/app/users/users-list/users-list.page.ts");

    var routes = [{
      path: '',
      component: _users_list_page__WEBPACK_IMPORTED_MODULE_3__["UsersListPage"]
    }];

    var UsersListPageRoutingModule = function UsersListPageRoutingModule() {
      _classCallCheck(this, UsersListPageRoutingModule);
    };

    UsersListPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], UsersListPageRoutingModule);
    /***/
  },

  /***/
  "./src/app/users/users-list/users-list.module.ts":
  /*!*******************************************************!*\
    !*** ./src/app/users/users-list/users-list.module.ts ***!
    \*******************************************************/

  /*! exports provided: UsersListPageModule */

  /***/
  function srcAppUsersUsersListUsersListModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "UsersListPageModule", function () {
      return UsersListPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _users_list_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./users-list-routing.module */
    "./src/app/users/users-list/users-list-routing.module.ts");
    /* harmony import */


    var _users_list_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./users-list.page */
    "./src/app/users/users-list/users-list.page.ts");

    var UsersListPageModule = function UsersListPageModule() {
      _classCallCheck(this, UsersListPageModule);
    };

    UsersListPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _users_list_routing_module__WEBPACK_IMPORTED_MODULE_5__["UsersListPageRoutingModule"]],
      declarations: [_users_list_page__WEBPACK_IMPORTED_MODULE_6__["UsersListPage"]]
    })], UsersListPageModule);
    /***/
  },

  /***/
  "./src/app/users/users-list/users-list.page.scss":
  /*!*******************************************************!*\
    !*** ./src/app/users/users-list/users-list.page.scss ***!
    \*******************************************************/

  /*! exports provided: default */

  /***/
  function srcAppUsersUsersListUsersListPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3VzZXJzL3VzZXJzLWxpc3QvdXNlcnMtbGlzdC5wYWdlLnNjc3MifQ== */";
    /***/
  },

  /***/
  "./src/app/users/users-list/users-list.page.ts":
  /*!*****************************************************!*\
    !*** ./src/app/users/users-list/users-list.page.ts ***!
    \*****************************************************/

  /*! exports provided: UsersListPage */

  /***/
  function srcAppUsersUsersListUsersListPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "UsersListPage", function () {
      return UsersListPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var src_app_services_users_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! src/app/services/users.service */
    "./src/app/services/users.service.ts");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");

    var UsersListPage = /*#__PURE__*/function () {
      function UsersListPage(userService, router, alertCtrl, modalController, toastController, loadingController) {
        _classCallCheck(this, UsersListPage);

        this.userService = userService;
        this.router = router;
        this.alertCtrl = alertCtrl;
        this.modalController = modalController;
        this.toastController = toastController;
        this.loadingController = loadingController; // adapt to the user model types

        this.usersList = [];
      }

      _createClass(UsersListPage, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          var _this = this;

          this.router.events.subscribe(function (res) {
            if (res instanceof _angular_router__WEBPACK_IMPORTED_MODULE_3__["NavigationEnd"]) {
              _this.getAllUsers();
            }
          });
        }
      }, {
        key: "getAllUsers",
        value: function getAllUsers() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
            var _this2 = this;

            var loading;
            return regeneratorRuntime.wrap(function _callee$(_context) {
              while (1) {
                switch (_context.prev = _context.next) {
                  case 0:
                    _context.next = 2;
                    return this.loadingController.create({
                      message: 'Loading...'
                    });

                  case 2:
                    loading = _context.sent;
                    _context.next = 5;
                    return loading.present();

                  case 5:
                    this.userService.getAllUsers().subscribe(function (data) {
                      if (data.code === '0000') {
                        _this2.usersList = data.content;
                        loading.dismiss();
                      } else {
                        loading.dismiss();
                      }
                    }, function (err) {
                      loading.dismiss();
                    });

                  case 6:
                  case "end":
                    return _context.stop();
                }
              }
            }, _callee, this);
          }));
        }
      }, {
        key: "editUser",
        value: function editUser(userId) {
          this.router.navigate(['users/edit-user/' + userId]);
        }
      }, {
        key: "delete",
        value: function _delete(userId) {
          var _this3 = this;

          this.userService.deleteUser(userId).subscribe(function (data) {
            if (data.code === '0000') {
              _this3.presentToast(data.message, 'success');
            } else {
              _this3.presentToast(data.message, 'error');

              console.log('error');
            }
          }, function (err) {
            _this3.presentToast(err, 'error');

            console.log('error');
          });
        }
      }, {
        key: "userdetails",
        value: function userdetails(userId) {
          this.router.navigate(['users/user-details/' + userId]);
        }
      }, {
        key: "presentConfirmDelete",
        value: function presentConfirmDelete(userId) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
            var _this4 = this;

            var alert;
            return regeneratorRuntime.wrap(function _callee2$(_context2) {
              while (1) {
                switch (_context2.prev = _context2.next) {
                  case 0:
                    alert = this.alertCtrl.create({
                      message: 'Do you want to delet this user',
                      buttons: [{
                        text: 'Cancel',
                        role: 'cancel',
                        handler: function handler() {
                          console.log('Cancel clicked');
                        }
                      }, {
                        text: 'Confirm',
                        handler: function handler() {
                          _this4["delete"](userId);

                          _this4.ReloadList();
                        }
                      }]
                    });
                    _context2.next = 3;
                    return alert;

                  case 3:
                    _context2.sent.present();

                  case 4:
                  case "end":
                    return _context2.stop();
                }
              }
            }, _callee2, this);
          }));
        }
      }, {
        key: "ReloadList",
        value: function ReloadList() {
          this.getAllUsers();
        }
      }, {
        key: "presentToast",
        value: function presentToast(msg, messagetype) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
            var toast;
            return regeneratorRuntime.wrap(function _callee3$(_context3) {
              while (1) {
                switch (_context3.prev = _context3.next) {
                  case 0:
                    _context3.next = 2;
                    return this.toastController.create({
                      message: msg,
                      duration: 3000,
                      position: 'bottom',
                      color: messagetype === 'success' ? 'success' : 'danger'
                    });

                  case 2:
                    toast = _context3.sent;
                    toast.present();

                  case 4:
                  case "end":
                    return _context3.stop();
                }
              }
            }, _callee3, this);
          }));
        }
      }]);

      return UsersListPage;
    }();

    UsersListPage.ctorParameters = function () {
      return [{
        type: src_app_services_users_service__WEBPACK_IMPORTED_MODULE_2__["UserService"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"]
      }];
    };

    UsersListPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-users-list',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./users-list.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/users/users-list/users-list.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./users-list.page.scss */
      "./src/app/users/users-list/users-list.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_users_service__WEBPACK_IMPORTED_MODULE_2__["UserService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"]])], UsersListPage);
    /***/
  }
}]);
//# sourceMappingURL=users-list-users-list-module-es5.js.map