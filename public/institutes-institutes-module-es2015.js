(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["institutes-institutes-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/institutes/institutes.page.html":
/*!***************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/institutes/institutes.page.html ***!
  \***************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\r\n  <ion-toolbar>\r\n    <ion-title>institutes</ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/institutes/institutes-routing.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/institutes/institutes-routing.module.ts ***!
  \*********************************************************/
/*! exports provided: InstitutesPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InstitutesPageRoutingModule", function() { return InstitutesPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");



const routes = [
    {
        path: '',
        redirectTo: 'institues-list',
        pathMatch: 'full'
    },
    {
        path: 'institues-list',
        loadChildren: () => __webpack_require__.e(/*! import() | institues-list-institues-list-module */ "institues-list-institues-list-module").then(__webpack_require__.bind(null, /*! ./institues-list/institues-list.module */ "./src/app/institutes/institues-list/institues-list.module.ts")).then(m => m.InstituesListPageModule)
    },
    {
        path: 'add-institut',
        loadChildren: () => Promise.all(/*! import() | add-institut-add-institut-module */[__webpack_require__.e("default~add-institut-add-institut-module~home-home-module"), __webpack_require__.e("common"), __webpack_require__.e("add-institut-add-institut-module")]).then(__webpack_require__.bind(null, /*! ./add-institut/add-institut.module */ "./src/app/institutes/add-institut/add-institut.module.ts")).then(m => m.AddInstitutPageModule)
    },
    {
        path: 'institut-details/:id',
        loadChildren: () => __webpack_require__.e(/*! import() | institut-details-institut-details-module */ "institut-details-institut-details-module").then(__webpack_require__.bind(null, /*! ./institut-details/institut-details.module */ "./src/app/institutes/institut-details/institut-details.module.ts")).then(m => m.InstitutDetailsPageModule)
    },
    {
        path: 'edit-institut/:id',
        loadChildren: () => Promise.all(/*! import() | add-institut-add-institut-module */[__webpack_require__.e("default~add-institut-add-institut-module~home-home-module"), __webpack_require__.e("common"), __webpack_require__.e("add-institut-add-institut-module")]).then(__webpack_require__.bind(null, /*! ./add-institut/add-institut.module */ "./src/app/institutes/add-institut/add-institut.module.ts")).then(m => m.AddInstitutPageModule)
    },
    {
        path: '**',
        redirectTo: 'institues-list'
    }
];
let InstitutesPageRoutingModule = class InstitutesPageRoutingModule {
};
InstitutesPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], InstitutesPageRoutingModule);



/***/ }),

/***/ "./src/app/institutes/institutes.module.ts":
/*!*************************************************!*\
  !*** ./src/app/institutes/institutes.module.ts ***!
  \*************************************************/
/*! exports provided: InstitutesPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InstitutesPageModule", function() { return InstitutesPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _institutes_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./institutes-routing.module */ "./src/app/institutes/institutes-routing.module.ts");
/* harmony import */ var _institutes_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./institutes.page */ "./src/app/institutes/institutes.page.ts");







let InstitutesPageModule = class InstitutesPageModule {
};
InstitutesPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _institutes_routing_module__WEBPACK_IMPORTED_MODULE_5__["InstitutesPageRoutingModule"]
        ],
        declarations: [_institutes_page__WEBPACK_IMPORTED_MODULE_6__["InstitutesPage"]]
    })
], InstitutesPageModule);



/***/ }),

/***/ "./src/app/institutes/institutes.page.scss":
/*!*************************************************!*\
  !*** ./src/app/institutes/institutes.page.scss ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2luc3RpdHV0ZXMvaW5zdGl0dXRlcy5wYWdlLnNjc3MifQ== */");

/***/ }),

/***/ "./src/app/institutes/institutes.page.ts":
/*!***********************************************!*\
  !*** ./src/app/institutes/institutes.page.ts ***!
  \***********************************************/
/*! exports provided: InstitutesPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InstitutesPage", function() { return InstitutesPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let InstitutesPage = class InstitutesPage {
    constructor() { }
    ngOnInit() {
    }
};
InstitutesPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-institutes',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./institutes.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/institutes/institutes.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./institutes.page.scss */ "./src/app/institutes/institutes.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], InstitutesPage);



/***/ })

}]);
//# sourceMappingURL=institutes-institutes-module-es2015.js.map