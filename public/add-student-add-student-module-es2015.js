(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["add-student-add-student-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/students/add-student/add-student.page.html":
/*!**************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/students/add-student/add-student.page.html ***!
  \**************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\r\n    <ion-toolbar>\r\n      <ion-title *ngIf=\"!EditStudentInfo\">Add Student</ion-title>\r\n      <ion-title *ngIf=\"EditStudentInfo\">Edit Student</ion-title>\r\n      <ion-buttons slot=\"start\">\r\n        <ion-menu-button autoHide=\"false\"></ion-menu-button>\r\n        <ion-back-button></ion-back-button>\r\n      </ion-buttons>\r\n    </ion-toolbar>\r\n  </ion-header>\r\n  \r\n  <ion-content>\r\n    <ion-card>\r\n      <form [formGroup]=\"StudentForm\">\r\n        <ion-item lines=\"inset\">\r\n          <ion-input\r\n            formControlName=\"firstname\"\r\n            placeholder=\"firstname\"\r\n            required\r\n          ></ion-input>\r\n        </ion-item>\r\n        <ion-item lines=\"inset\">\r\n          <ion-input\r\n            formControlName=\"lastname\"\r\n            placeholder=\"lastname\"\r\n          ></ion-input>\r\n        </ion-item>\r\n        <ion-select formControlName=\"gender\" \r\n                    placeholder='gender'>\r\n          <ion-select-option value=\"m\">Male</ion-select-option>\r\n          <ion-select-option value=\"f\">Female</ion-select-option>\r\n        </ion-select>\r\n        <ion-select\r\n         formControlName=\"status\"\r\n         placeholder=\"status\">\r\n          <ion-select-option value=\"A\">Amel</ion-select-option>\r\n          <ion-select-option value=\"M\">Marid</ion-select-option>\r\n        </ion-select>\r\n        <ion-item lines=\"inset\"  *ngIf=\"StudentForm.value.status == 'M'\">\r\n          <ion-input\r\n            formControlName=\"roomNumber\"\r\n            placeholder=\"roomNumber\"\r\n            type='number'\r\n          ></ion-input>\r\n        </ion-item>\r\n        <ion-item lines=\"inset\" *ngIf=\"StudentForm.value.status == 'M'\">\r\n          <ion-input formControlName=\"level\" placeholder=\"level\"></ion-input>\r\n        </ion-item>\r\n        <ion-item lines=\"inset\" *ngIf=\"StudentForm.value.status == 'M'\">\r\n          <ion-datetime \r\n           displayFormat=\"MM/DD/YYYY\" \r\n           formControlName=\"entryDate\"\r\n           placeholder=\"entryDate\" \r\n           value=\"entryDate\">\r\n          </ion-datetime>\r\n        </ion-item>\r\n  \r\n        <ion-item lines=\"inset\" *ngIf=\"StudentForm.value.status == 'M'\">\r\n          <ion-datetime\r\n            displayFormat=\"MM/DD/YYYY\" \r\n            formControlName=\"exitDate\"\r\n            placeholder=\"exitDate\" \r\n            value=\"exitDate\">\r\n          </ion-datetime>\r\n        </ion-item>\r\n         <ion-select\r\n            lines=\"inset\"\r\n            formControlName=\"classId\"\r\n            placeholder=\"Class\">\r\n            <ion-select-option\r\n              *ngFor=\"let class of classList\"\r\n              value=\"{{class._id}}\">{{class.Classname}}</ion-select-option>\r\n          </ion-select>\r\n      </form>\r\n      <ion-button\r\n        shape=\"round\"\r\n        expand=\"block\"\r\n        *ngIf=\"!EditStudentInfo\"\r\n        (click)=\"addStudent(StudentForm)\"\r\n        [disabled]=\"!StudentForm.valid\"\r\n        >submit</ion-button\r\n      >\r\n      <ion-button\r\n        shape=\"round\"\r\n        expand=\"block\"\r\n        *ngIf=\"EditStudentInfo\"\r\n        (click)=\"UpdateStudent(StudentForm)\"\r\n        [disabled]=\"!StudentForm.valid\"\r\n        >Update</ion-button\r\n      >\r\n    </ion-card>\r\n  </ion-content>\r\n  ");

/***/ }),

/***/ "./src/app/students/add-student/add-student-routing.module.ts":
/*!********************************************************************!*\
  !*** ./src/app/students/add-student/add-student-routing.module.ts ***!
  \********************************************************************/
/*! exports provided: AddStudentPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddStudentPageRoutingModule", function() { return AddStudentPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _add_student_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./add-student.page */ "./src/app/students/add-student/add-student.page.ts");




const routes = [
    {
        path: '',
        component: _add_student_page__WEBPACK_IMPORTED_MODULE_3__["AddStudentPage"]
    }
];
let AddStudentPageRoutingModule = class AddStudentPageRoutingModule {
};
AddStudentPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], AddStudentPageRoutingModule);



/***/ }),

/***/ "./src/app/students/add-student/add-student.module.ts":
/*!************************************************************!*\
  !*** ./src/app/students/add-student/add-student.module.ts ***!
  \************************************************************/
/*! exports provided: AddStudentPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddStudentPageModule", function() { return AddStudentPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _add_student_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./add-student-routing.module */ "./src/app/students/add-student/add-student-routing.module.ts");
/* harmony import */ var src_app_shared_module_shared_module_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/shared-module/shared-module.module */ "./src/app/shared-module/shared-module.module.ts");
/* harmony import */ var _add_student_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./add-student.page */ "./src/app/students/add-student/add-student.page.ts");








let AddStudentPageModule = class AddStudentPageModule {
};
AddStudentPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            src_app_shared_module_shared_module_module__WEBPACK_IMPORTED_MODULE_6__["SharedModuleModule"],
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _add_student_routing_module__WEBPACK_IMPORTED_MODULE_5__["AddStudentPageRoutingModule"]
        ],
        declarations: [_add_student_page__WEBPACK_IMPORTED_MODULE_7__["AddStudentPage"]]
    })
], AddStudentPageModule);



/***/ }),

/***/ "./src/app/students/add-student/add-student.page.scss":
/*!************************************************************!*\
  !*** ./src/app/students/add-student/add-student.page.scss ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3N0dWRlbnRzL2FkZC1zdHVkZW50L2FkZC1zdHVkZW50LnBhZ2Uuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/students/add-student/add-student.page.ts":
/*!**********************************************************!*\
  !*** ./src/app/students/add-student/add-student.page.ts ***!
  \**********************************************************/
/*! exports provided: AddStudentPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddStudentPage", function() { return AddStudentPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var src_app_services_Students_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/Students.service */ "./src/app/services/Students.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _models_student_model__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../models/student.model */ "./src/app/students/models/student.model.ts");
/* harmony import */ var src_app_services_classes_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/classes.service */ "./src/app/services/classes.service.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
/* harmony import */ var src_app_services_communication_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/services/communication.service */ "./src/app/services/communication.service.ts");









let AddStudentPage = class AddStudentPage {
    constructor(formBuilder, router, studentsService, activatedRoute, classService, communicationService) {
        this.formBuilder = formBuilder;
        this.router = router;
        this.studentsService = studentsService;
        this.activatedRoute = activatedRoute;
        this.classService = classService;
        this.communicationService = communicationService;
        this.StudentToEdit = new _models_student_model__WEBPACK_IMPORTED_MODULE_5__["StudentModel"]();
        this.EditStudentInfo = false;
    }
    ngOnInit() {
        this.HalaqaId = this.communicationService.HalaqaId;
        this.InstitutId = this.communicationService.InstitutId;
        this.initForm();
        this.router.events
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["switchMap"])((navigation) => this.activatedRoute.params.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["map"])((params) => ({ navigation, params })))))
            .subscribe((result) => {
            if (result.navigation instanceof _angular_router__WEBPACK_IMPORTED_MODULE_4__["NavigationEnd"]) {
                const urlfragment = result.navigation.url.split('/')[2];
                if (urlfragment.toLowerCase().includes('add') &&
                    result.params.id !== undefined &&
                    result.params !== {}) {
                    this.HalaqaId = this.communicationService.HalaqaId =
                        result.params.id;
                    this.EditStudentInfo = false;
                    this.initForm();
                }
                else if (urlfragment.toLowerCase().includes('edit') &&
                    result.params.id !== undefined &&
                    result.params !== {}) {
                    this.GetStudentById(result.params.id);
                    this.studentId = result.params.id;
                    this.EditStudentInfo = true;
                }
                else {
                    console.log('error id undefined');
                }
            }
        });
        this.getAllInstitutClassesById();
    }
    getAllInstitutClassesById() {
        this.classService.getAllInstitutClassesById(this.InstitutId).subscribe((data) => {
            if (data.code === '0000') {
                this.classList = data.content;
            }
            else {
                console.log('error');
            }
        }, (err) => {
            console.log('subscription error', err);
        });
    }
    GetStudentById(StudentId) {
        this.studentsService.getStudentById(StudentId).subscribe((data) => {
            if (data.code === '0000') {
                console.log('data content', data.content[0]);
                this.StudentToEdit = new _models_student_model__WEBPACK_IMPORTED_MODULE_5__["StudentModel"](data.content[0]);
                console.log('this.StudentToEdit', this.StudentToEdit);
                this.initForm();
            }
            else {
                console.log('error', data);
            }
        }, (err) => {
            console.log('error', err);
        });
    }
    initForm() {
        this.StudentForm = this.formBuilder.group({
            firstname: [this.StudentToEdit.firstname, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            lastname: [this.StudentToEdit.lastname, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            gender: [this.StudentToEdit.gender],
            status: [this.StudentToEdit.status],
            roomNumber: [this.StudentToEdit.roomNumber],
            level: [this.StudentToEdit.level],
            entryDate: [this.StudentToEdit.entryDate],
            exitDate: [this.StudentToEdit.exitDate],
            classId: [this.StudentToEdit.classId],
        });
    }
    addStudent(StudentForm) {
        const StudentToAdd = {
            firstname: '',
            lastname: '',
            gender: '',
            status: '',
            roomNumber: '',
            level: '',
            entryDate: '',
            exitDate: '',
            created_at: '',
            classId: '',
            halaqaId: '',
        };
        Object.assign(StudentToAdd, StudentForm.value);
        StudentToAdd.halaqaId = this.HalaqaId;
        console.log('StudentToAdd', StudentToAdd);
        this.studentsService.addStudent(StudentToAdd).subscribe((data) => {
            if (data.code === '0000') {
                console.log('student added with success');
                this.GoBackToList();
            }
            else {
                console.log('error');
            }
        }, (err) => {
            console.log('error', err);
        });
    }
    UpdateStudent(StudentForm) {
        const StudentToAdd = {
            firstname: '',
            lastname: '',
            gender: '',
            status: '',
            roomNumber: '',
            level: '',
            entryDate: '',
            exitDate: '',
            created_at: '',
            classId: '',
            halaqaId: '',
        };
        Object.assign(StudentToAdd, StudentForm.value);
        StudentToAdd.halaqaId = this.HalaqaId;
        this.studentsService.updateStudent(this.studentId, StudentToAdd).subscribe((data) => {
            if (data.code === '0000') {
                console.log('student updated with success');
                this.GoBackToList();
            }
            else {
                console.log('error update');
            }
        }, (err) => {
            console.log('error', err);
        });
    }
    GoBackToList() {
        this.router.navigate(['/students/students-list/' + this.HalaqaId]);
    }
};
AddStudentPage.ctorParameters = () => [
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
    { type: src_app_services_Students_service__WEBPACK_IMPORTED_MODULE_3__["StudentsService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"] },
    { type: src_app_services_classes_service__WEBPACK_IMPORTED_MODULE_6__["ClassesService"] },
    { type: src_app_services_communication_service__WEBPACK_IMPORTED_MODULE_8__["CommunicationService"] }
];
AddStudentPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-add-student',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./add-student.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/students/add-student/add-student.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./add-student.page.scss */ "./src/app/students/add-student/add-student.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
        _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"],
        src_app_services_Students_service__WEBPACK_IMPORTED_MODULE_3__["StudentsService"],
        _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"],
        src_app_services_classes_service__WEBPACK_IMPORTED_MODULE_6__["ClassesService"],
        src_app_services_communication_service__WEBPACK_IMPORTED_MODULE_8__["CommunicationService"]])
], AddStudentPage);



/***/ }),

/***/ "./src/app/students/models/student.model.ts":
/*!**************************************************!*\
  !*** ./src/app/students/models/student.model.ts ***!
  \**************************************************/
/*! exports provided: StudentModel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StudentModel", function() { return StudentModel; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _sharedModels_sharedModel__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../sharedModels/sharedModel */ "./src/app/sharedModels/sharedModel.ts");


class StudentModel extends _sharedModels_sharedModel__WEBPACK_IMPORTED_MODULE_1__["sharedModel"] {
    constructor(StudentEntity) {
        StudentEntity = StudentEntity || {};
        super(StudentEntity);
        this.firstname = StudentEntity.firstname || '';
        this.lastname = StudentEntity.lastname || '';
        this.gender = StudentEntity.gender || '';
        this.status = StudentEntity.status || '';
        this.roomNumber = StudentEntity.roomNumber || '';
        this.level = StudentEntity.level || '';
        this.entryDate = StudentEntity.entryDate || '';
        this.exitDate = StudentEntity.exitDate || '';
        this.created_at = StudentEntity.created_at || '';
        this.classId = StudentEntity.classId || '';
        this.halaqaId = StudentEntity.halaqaId || '';
    }
}


/***/ })

}]);
//# sourceMappingURL=add-student-add-student-module-es2015.js.map