function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["achivement-list-achivement-list-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/achievements/achivement-list/achivement-list.page.html":
  /*!**************************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/achievements/achivement-list/achivement-list.page.html ***!
    \**************************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppAchievementsAchivementListAchivementListPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\r\n  <ion-toolbar>\r\n    <ion-title>achivement-list</ion-title>\r\n    <ion-buttons slot=\"start\">\r\n        <ion-menu-button autoHide=\"false\"></ion-menu-button>\r\n        <ion-back-button></ion-back-button>\r\n      </ion-buttons>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-item>\r\n  <ion-icon name=\"person\"></ion-icon>\r\n  <ion-label style=\"padding-left: 30px;\" *ngIf='studentInfo?.firstname; else noData'>{{studentInfo?.firstname}} {{studentInfo?.lastname}}</ion-label>\r\n</ion-item>\r\n\r\n<ion-content padding>\r\n  <ion-button\r\n    shape=\"round\"\r\n    *ngIf=\"connectedUserRole == 'admin' || connectedUserRole == 'cheick'\"\r\n    expand=\"block\"\r\n    (click)='goToAddAchivement()'\r\n    routerDirection=\"root\">\r\n    <ion-icon slot=\"icon-only\" name=\"add\"></ion-icon>\r\n    Add achivement\r\n  </ion-button>\r\n  <form [formGroup]=\"FilterForm\">\r\n      <ion-select formControlName=\"program\" placeholder=\"Filter by program\" (ionChange)=\"SelectProgram($event)\" required>\r\n        <ion-select-option value=\"Memorize\">Memorize</ion-select-option>\r\n        <ion-select-option value=\"Revision\">Revision</ion-select-option>\r\n        <ion-select-option value=\"Exam\">Exam</ion-select-option>\r\n        <ion-select-option value=\"\">All</ion-select-option>\r\n      </ion-select>\r\n  </form>  \r\n  <ion-card>\r\n  <ion-item-sliding *ngFor=\"let achivement of achivementsList\">\r\n    <ion-item>\r\n      <ion-text>\r\n        {{achivement.program}} -- {{achivement.created_at | date}}\r\n      </ion-text>\r\n    </ion-item>\r\n    <ion-item-options>\r\n      <ion-item-option color=\"primary\"\r\n       (click)=\"Achivementdetails(achivement._id)\">\r\n        <ion-icon slot=\"top\" name=\"more\"></ion-icon>\r\n        details\r\n      </ion-item-option>\r\n      <ion-item-option color=\"secondary\" \r\n      *ngIf=\"connectedUserRole == 'admin' || connectedUserRole == 'cheick'\"\r\n       (click)=\"editAchivement(achivement._id)\">\r\n        <ion-icon slot=\"top\" name=\"create\"></ion-icon>\r\n        edit\r\n      </ion-item-option>\r\n      <ion-item-option\r\n      *ngIf=\"connectedUserRole == 'admin' || connectedUserRole == 'cheick'\"\r\n        color=\"danger\"\r\n        (click)=\"presentConfirmDelete(achivement._id)\">\r\n        <ion-icon slot=\"top\" name=\"trash\"></ion-icon>\r\n        trash\r\n      </ion-item-option>\r\n    </ion-item-options>\r\n  </ion-item-sliding>\r\n  </ion-card>\r\n</ion-content>\r\n";
    /***/
  },

  /***/
  "./src/app/achievements/achivement-list/achivement-list-routing.module.ts":
  /*!********************************************************************************!*\
    !*** ./src/app/achievements/achivement-list/achivement-list-routing.module.ts ***!
    \********************************************************************************/

  /*! exports provided: AchivementListPageRoutingModule */

  /***/
  function srcAppAchievementsAchivementListAchivementListRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AchivementListPageRoutingModule", function () {
      return AchivementListPageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _achivement_list_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./achivement-list.page */
    "./src/app/achievements/achivement-list/achivement-list.page.ts");

    var routes = [{
      path: '',
      component: _achivement_list_page__WEBPACK_IMPORTED_MODULE_3__["AchivementListPage"]
    }];

    var AchivementListPageRoutingModule = function AchivementListPageRoutingModule() {
      _classCallCheck(this, AchivementListPageRoutingModule);
    };

    AchivementListPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], AchivementListPageRoutingModule);
    /***/
  },

  /***/
  "./src/app/achievements/achivement-list/achivement-list.module.ts":
  /*!************************************************************************!*\
    !*** ./src/app/achievements/achivement-list/achivement-list.module.ts ***!
    \************************************************************************/

  /*! exports provided: AchivementListPageModule */

  /***/
  function srcAppAchievementsAchivementListAchivementListModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AchivementListPageModule", function () {
      return AchivementListPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _achivement_list_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./achivement-list-routing.module */
    "./src/app/achievements/achivement-list/achivement-list-routing.module.ts");
    /* harmony import */


    var _achivement_list_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./achivement-list.page */
    "./src/app/achievements/achivement-list/achivement-list.page.ts");
    /* harmony import */


    var src_app_shared_module_shared_module_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! src/app/shared-module/shared-module.module */
    "./src/app/shared-module/shared-module.module.ts");

    var AchivementListPageModule = function AchivementListPageModule() {
      _classCallCheck(this, AchivementListPageModule);
    };

    AchivementListPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], src_app_shared_module_shared_module_module__WEBPACK_IMPORTED_MODULE_7__["SharedModuleModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _achivement_list_routing_module__WEBPACK_IMPORTED_MODULE_5__["AchivementListPageRoutingModule"]],
      declarations: [_achivement_list_page__WEBPACK_IMPORTED_MODULE_6__["AchivementListPage"]]
    })], AchivementListPageModule);
    /***/
  },

  /***/
  "./src/app/achievements/achivement-list/achivement-list.page.scss":
  /*!************************************************************************!*\
    !*** ./src/app/achievements/achivement-list/achivement-list.page.scss ***!
    \************************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppAchievementsAchivementListAchivementListPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FjaGlldmVtZW50cy9hY2hpdmVtZW50LWxpc3QvYWNoaXZlbWVudC1saXN0LnBhZ2Uuc2NzcyJ9 */";
    /***/
  },

  /***/
  "./src/app/achievements/achivement-list/achivement-list.page.ts":
  /*!**********************************************************************!*\
    !*** ./src/app/achievements/achivement-list/achivement-list.page.ts ***!
    \**********************************************************************/

  /*! exports provided: AchivementListPage */

  /***/
  function srcAppAchievementsAchivementListAchivementListPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AchivementListPage", function () {
      return AchivementListPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var src_app_services_achivements_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! src/app/services/achivements.service */
    "./src/app/services/achivements.service.ts");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var src_app_services_communication_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! src/app/services/communication.service */
    "./src/app/services/communication.service.ts");
    /* harmony import */


    var src_app_services_users_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! src/app/services/users.service */
    "./src/app/services/users.service.ts");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var src_app_services_search_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! src/app/services/search.service */
    "./src/app/services/search.service.ts");
    /* harmony import */


    var src_app_services_Students_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! src/app/services/Students.service */
    "./src/app/services/Students.service.ts");

    var AchivementListPage = /*#__PURE__*/function () {
      function AchivementListPage(achivementService, router, activatedRoute, alertCtrl, modalController, loadingController, userService, fb, SearchService, studentsService, CommunicationService) {
        _classCallCheck(this, AchivementListPage);

        this.achivementService = achivementService;
        this.router = router;
        this.activatedRoute = activatedRoute;
        this.alertCtrl = alertCtrl;
        this.modalController = modalController;
        this.loadingController = loadingController;
        this.userService = userService;
        this.fb = fb;
        this.SearchService = SearchService;
        this.studentsService = studentsService;
        this.CommunicationService = CommunicationService;
      }

      _createClass(AchivementListPage, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          var _this = this;

          // get the student id from the url
          this.activatedRoute.params.subscribe(function (params) {
            if (params !== {} && params.id !== undefined) {
              _this.StudentId = _this.CommunicationService.StudentId = params.id;

              _this.getStudentById();
            }
          });
          this.router.events.subscribe(function (res) {
            if (res instanceof _angular_router__WEBPACK_IMPORTED_MODULE_3__["NavigationEnd"]) {
              _this.getAllStudentAchivements();
            }
          });
          this.setUserRole();
          this.initForm();
        }
      }, {
        key: "getStudentById",
        value: function getStudentById() {
          var _this2 = this;

          this.studentsService.getStudentById(this.StudentId).subscribe(function (res) {
            if (res.code == '0000') {
              _this2.studentInfo = res.content[0];
            } else {}
          }, function (err) {});
        }
      }, {
        key: "initForm",
        value: function initForm() {
          this.FilterForm = this.fb.group({
            program: []
          });
        }
      }, {
        key: "setUserRole",
        value: function setUserRole() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
            return regeneratorRuntime.wrap(function _callee$(_context) {
              while (1) {
                switch (_context.prev = _context.next) {
                  case 0:
                    _context.next = 2;
                    return this.userService.getUserRole();

                  case 2:
                    this.connectedUserRole = _context.sent;

                  case 3:
                  case "end":
                    return _context.stop();
                }
              }
            }, _callee, this);
          }));
        }
      }, {
        key: "SelectProgram",
        value: function SelectProgram(event) {
          var _this3 = this;

          var keyWord = event.detail.value;
          this.SearchService.StudentProgramFilter(this.StudentId, keyWord).subscribe(function (res) {
            if (res.code === '0000') {
              _this3.achivementsList = res.content;
            } else {
              console.log('no data found');
            }
          }, function (err) {});
        }
      }, {
        key: "getAllStudentAchivements",
        value: function getAllStudentAchivements() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
            var _this4 = this;

            var loading;
            return regeneratorRuntime.wrap(function _callee2$(_context2) {
              while (1) {
                switch (_context2.prev = _context2.next) {
                  case 0:
                    _context2.next = 2;
                    return this.loadingController.create({
                      message: 'Loading...'
                    });

                  case 2:
                    loading = _context2.sent;
                    _context2.next = 5;
                    return loading.present();

                  case 5:
                    this.SearchService.StudentProgramFilter(this.StudentId, '').subscribe(function (res) {
                      if (res.code === '0000') {
                        _this4.achivementsList = res.content;
                        loading.dismiss();
                      } else {
                        loading.dismiss();
                      }
                    }, function (err) {
                      loading.dismiss();
                    }); // this.achivementService.getAllStudentAchivements(this.StudentId).subscribe(
                    //   data => {
                    //     if (data.code === '0000') {
                    //       console.log('data', data);
                    //       this.achivementsList = data.content;
                    //       loading.dismiss();
                    //     } else {
                    //       console.log('error');
                    //       loading.dismiss();
                    //     }
                    //   },
                    //   err => {
                    //     console.log('subscription error', err);
                    //     loading.dismiss();
                    //   }
                    // );

                  case 6:
                  case "end":
                    return _context2.stop();
                }
              }
            }, _callee2, this);
          }));
        } // getColor(program)
        // {
        // switch(program)
        // {
        //   case 'Memorize':
        //     return 'primary';
        //     break;
        //   case 'Revision':
        //     return 'secondary';
        //   break;
        //   case 'Exam':
        //     return 'tertiary';
        //   break;
        // }
        // }

      }, {
        key: "goToAddAchivement",
        value: function goToAddAchivement() {
          this.router.navigate(['/achievements/add-achivement/' + this.StudentId]);
        }
      }, {
        key: "editAchivement",
        value: function editAchivement(achivementId) {
          this.router.navigate(['/achievements/edit-achivement/' + achivementId]);
        }
      }, {
        key: "delete",
        value: function _delete(achivementId) {
          this.achivementService.deleteAchivement(achivementId).subscribe(function (data) {
            if (data.code === '0000') {} else {
              console.log('error');
            }
          }, function (err) {
            console.log('error');
          });
        }
      }, {
        key: "Achivementdetails",
        value: function Achivementdetails(achivementId) {
          this.router.navigate(['achievements/achivements-details/' + achivementId]);
        }
      }, {
        key: "presentConfirmDelete",
        value: function presentConfirmDelete(achivementId) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
            var _this5 = this;

            var alert;
            return regeneratorRuntime.wrap(function _callee3$(_context3) {
              while (1) {
                switch (_context3.prev = _context3.next) {
                  case 0:
                    alert = this.alertCtrl.create({
                      message: 'Do you want to delet this achivement',
                      buttons: [{
                        text: 'Cancel',
                        role: 'cancel',
                        handler: function handler() {
                          console.log('Cancel clicked');
                        }
                      }, {
                        text: 'Confirm',
                        handler: function handler() {
                          _this5["delete"](achivementId);

                          _this5.ReloadList();
                        }
                      }]
                    });
                    _context3.next = 3;
                    return alert;

                  case 3:
                    _context3.sent.present();

                  case 4:
                  case "end":
                    return _context3.stop();
                }
              }
            }, _callee3, this);
          }));
        }
      }, {
        key: "ReloadList",
        value: function ReloadList() {
          this.getAllStudentAchivements();
        }
      }]);

      return AchivementListPage;
    }();

    AchivementListPage.ctorParameters = function () {
      return [{
        type: src_app_services_achivements_service__WEBPACK_IMPORTED_MODULE_2__["AchivementService"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"]
      }, {
        type: src_app_services_users_service__WEBPACK_IMPORTED_MODULE_6__["UserService"]
      }, {
        type: _angular_forms__WEBPACK_IMPORTED_MODULE_7__["FormBuilder"]
      }, {
        type: src_app_services_search_service__WEBPACK_IMPORTED_MODULE_8__["SearchService"]
      }, {
        type: src_app_services_Students_service__WEBPACK_IMPORTED_MODULE_9__["StudentsService"]
      }, {
        type: src_app_services_communication_service__WEBPACK_IMPORTED_MODULE_5__["CommunicationService"]
      }];
    };

    AchivementListPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-achivement-list',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./achivement-list.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/achievements/achivement-list/achivement-list.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./achivement-list.page.scss */
      "./src/app/achievements/achivement-list/achivement-list.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_achivements_service__WEBPACK_IMPORTED_MODULE_2__["AchivementService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"], src_app_services_users_service__WEBPACK_IMPORTED_MODULE_6__["UserService"], _angular_forms__WEBPACK_IMPORTED_MODULE_7__["FormBuilder"], src_app_services_search_service__WEBPACK_IMPORTED_MODULE_8__["SearchService"], src_app_services_Students_service__WEBPACK_IMPORTED_MODULE_9__["StudentsService"], src_app_services_communication_service__WEBPACK_IMPORTED_MODULE_5__["CommunicationService"]])], AchivementListPage);
    /***/
  }
}]);
//# sourceMappingURL=achivement-list-achivement-list-module-es5.js.map