function _createSuper(Derived) { return function () { var Super = _getPrototypeOf(Derived), result; if (_isNativeReflectConstruct()) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (typeof call === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["add-user-add-user-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/users/add-user/add-user.page.html":
  /*!*****************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/users/add-user/add-user.page.html ***!
    \*****************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppUsersAddUserAddUserPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\r\n  <ion-toolbar>\r\n    <ion-title *ngIf='!EditUserInfo'>Add User</ion-title>\r\n    <ion-title *ngIf='EditUserInfo'>Edit User</ion-title>\r\n    <ion-buttons slot=\"start\">\r\n    <ion-menu-button autoHide=\"false\"></ion-menu-button>\r\n    <ion-back-button></ion-back-button>\r\n  </ion-buttons>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <ion-card>\r\n  <form  [formGroup]=\"UsersForm\">\r\n    <ion-item lines=\"inset\">\r\n      <ion-input \r\n      formControlName=\"firstname\" \r\n      placeholder='first name' required></ion-input>\r\n    </ion-item>\r\n    <div>\r\n      <ng-container *ngFor=\"let error of error_messages.firstname\">\r\n        <ion-text class=\"ion-padding\" color=\"danger\" *ngIf=\"UsersForm.get('firstname').hasError(error.type) && (UsersForm.get('firstname').dirty || UsersForm.get('firstname').touched)\">\r\n          {{ error.message }}\r\n        </ion-text>\r\n      </ng-container>\r\n    </div>\r\n    <ion-item lines=\"inset\">\r\n      <ion-input formControlName=\"lastname\" placeholder='lastname' required>\r\n      </ion-input>\r\n    </ion-item>\r\n    <div>\r\n      <ng-container *ngFor=\"let error of error_messages.lastname\">\r\n        <ion-text class=\"ion-padding\" color=\"danger\" *ngIf=\"UsersForm.get('lastname').hasError(error.type) && (UsersForm.get('lastname').dirty || UsersForm.get('lastname').touched)\">\r\n          {{ error.message }}\r\n        </ion-text>\r\n      </ng-container>\r\n    </div>\r\n    <ion-item lines=\"inset\">\r\n      <ion-input formControlName=\"email\" placeholder='email'required>\r\n      </ion-input>\r\n    </ion-item>\r\n    <div>\r\n      <ng-container *ngFor=\"let error of error_messages.email\">\r\n        <ion-text class=\"ion-padding\" color=\"danger\" *ngIf=\"UsersForm.get('email').hasError(error.type) && (UsersForm.get('email').dirty || UsersForm.get('email').touched)\">\r\n          {{ error.message }}\r\n        </ion-text>\r\n      </ng-container>\r\n    </div>\r\n    <ion-item lines=\"inset\" \r\n      *ngIf='!EditUserInfo'>\r\n      <ion-input formControlName='password' placeholder='password' [type]=\"showPassword ? 'text' : 'password'\"></ion-input>\r\n      <ion-icon slot=\"end\" [name]=\"PasswordToggleIcon\" (click)=\"togglePassword()\"></ion-icon>\r\n    </ion-item>\r\n    <div>\r\n      <ng-container *ngFor=\"let error of error_messages.password\">\r\n        <ion-text class=\"ion-padding\" color=\"danger\" *ngIf=\"UsersForm.get('password').hasError(error.type) && (UsersForm.get('password').dirty || UsersForm.get('password').touched)\">\r\n          {{ error.message }}\r\n        </ion-text>\r\n      </ng-container>\r\n    </div>\r\n    <ion-item lines=\"inset\" \r\n      *ngIf='!EditUserInfo'>\r\n      <ion-input formControlName='confirmpassword' placeholder='confirmpassword' [type]=\"showPassword ? 'text' : 'password'\"></ion-input>\r\n      <ion-icon slot=\"end\" [name]=\"PasswordToggleIcon\" (click)=\"togglePassword()\"></ion-icon>\r\n    </ion-item>\r\n    <div>\r\n      <ng-container *ngFor=\"let error of error_messages.confirmpassword\">\r\n        <ion-text class=\"ion-padding\" color=\"danger\" *ngIf=\"UsersForm.get('confirmpassword').hasError(error.type) && (UsersForm.get('confirmpassword').dirty || UsersForm.get('confirmpassword').touched)\">\r\n          {{ error.message }}\r\n        </ion-text>\r\n      </ng-container>\r\n    </div>\r\n    <div  class=\"ion-padding\" \r\n          color=\"danger\" \r\n          *ngIf=\"!UsersForm.get('confirmpassword').errors && UsersForm.hasError('passwordNotMatch') && (UsersForm.get('confirmpassword').dirty || UsersForm.get('confirmpassword').touched)\">\r\n      Password and Confirm Password fields should match\r\n    </div>\r\n    <ion-item lines=\"inset\">\r\n      <ion-input formControlName=\"phone\" placeholder='phone'>\r\n      </ion-input>\r\n    </ion-item>\r\n    <ion-item lines=\"inset\">\r\n      <!-- <ion-input formControlName=\"role\" placeholder='role'>\r\n      </ion-input> -->\r\n      <ion-select  lines=\"inset\" formControlName=\"role\" placeholder=\"role\" required>\r\n        <ion-select-option value=\"admin\">admin</ion-select-option>\r\n        <ion-select-option value=\"cheick\">cheikh</ion-select-option>\r\n        <!-- <ion-select-option value=\"guest\">Exam</ion-select-option> -->\r\n      </ion-select>\r\n\r\n    </ion-item>\r\n      <ion-select lines=\"inset\"\r\n       multiple=\"true\" \r\n       cancelText=\"No\" \r\n       okText=\"Yes\"\r\n       formControlName=\"institutesids\" \r\n       placeholder='Institutes'>\r\n        <ion-select-option *ngFor=\"let Institut of listInstitutes\" value={{Institut._id}}>{{Institut.InstituteName}}</ion-select-option>\r\n      </ion-select>\r\n  </form>\r\n  <ion-button  \r\n  shape=\"round\"\r\n  expand=\"block\" \r\n  *ngIf='!EditUserInfo' \r\n  (click)='addUser(UsersForm)'\r\n   [disabled]='!UsersForm.valid'>submit</ion-button>\r\n  <ion-button  \r\n   shape=\"round\"\r\n   expand=\"block\"  \r\n  *ngIf='EditUserInfo' \r\n  (click)='UpdateUser(UsersForm)'>Update</ion-button>  \r\n</ion-card>\r\n</ion-content>\r\n\r\n";
    /***/
  },

  /***/
  "./src/app/users/add-user/add-user-routing.module.ts":
  /*!***********************************************************!*\
    !*** ./src/app/users/add-user/add-user-routing.module.ts ***!
    \***********************************************************/

  /*! exports provided: AddUserPageRoutingModule */

  /***/
  function srcAppUsersAddUserAddUserRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AddUserPageRoutingModule", function () {
      return AddUserPageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _add_user_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./add-user.page */
    "./src/app/users/add-user/add-user.page.ts");

    var routes = [{
      path: '',
      component: _add_user_page__WEBPACK_IMPORTED_MODULE_3__["AddUserPage"]
    }];

    var AddUserPageRoutingModule = function AddUserPageRoutingModule() {
      _classCallCheck(this, AddUserPageRoutingModule);
    };

    AddUserPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], AddUserPageRoutingModule);
    /***/
  },

  /***/
  "./src/app/users/add-user/add-user.module.ts":
  /*!***************************************************!*\
    !*** ./src/app/users/add-user/add-user.module.ts ***!
    \***************************************************/

  /*! exports provided: AddUserPageModule */

  /***/
  function srcAppUsersAddUserAddUserModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AddUserPageModule", function () {
      return AddUserPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _add_user_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./add-user-routing.module */
    "./src/app/users/add-user/add-user-routing.module.ts");
    /* harmony import */


    var _add_user_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./add-user.page */
    "./src/app/users/add-user/add-user.page.ts");
    /* harmony import */


    var src_app_shared_module_shared_module_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! src/app/shared-module/shared-module.module */
    "./src/app/shared-module/shared-module.module.ts");

    var AddUserPageModule = function AddUserPageModule() {
      _classCallCheck(this, AddUserPageModule);
    };

    AddUserPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], src_app_shared_module_shared_module_module__WEBPACK_IMPORTED_MODULE_7__["SharedModuleModule"], _add_user_routing_module__WEBPACK_IMPORTED_MODULE_5__["AddUserPageRoutingModule"]],
      declarations: [_add_user_page__WEBPACK_IMPORTED_MODULE_6__["AddUserPage"]]
    })], AddUserPageModule);
    /***/
  },

  /***/
  "./src/app/users/add-user/add-user.page.scss":
  /*!***************************************************!*\
    !*** ./src/app/users/add-user/add-user.page.scss ***!
    \***************************************************/

  /*! exports provided: default */

  /***/
  function srcAppUsersAddUserAddUserPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3VzZXJzL2FkZC11c2VyL2FkZC11c2VyLnBhZ2Uuc2NzcyJ9 */";
    /***/
  },

  /***/
  "./src/app/users/add-user/add-user.page.ts":
  /*!*************************************************!*\
    !*** ./src/app/users/add-user/add-user.page.ts ***!
    \*************************************************/

  /*! exports provided: AddUserPage */

  /***/
  function srcAppUsersAddUserAddUserPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AddUserPage", function () {
      return AddUserPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var src_app_services_users_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! src/app/services/users.service */
    "./src/app/services/users.service.ts");
    /* harmony import */


    var _models_users_models__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ../models/users.models */
    "./src/app/users/models/users.models.ts");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var src_app_services_institut_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! src/app/services/institut.service */
    "./src/app/services/institut.service.ts");

    var AddUserPage = /*#__PURE__*/function () {
      function AddUserPage(formBuilder, router, userService, institutService, toastController, activatedRoute) {
        _classCallCheck(this, AddUserPage);

        this.formBuilder = formBuilder;
        this.router = router;
        this.userService = userService;
        this.institutService = institutService;
        this.toastController = toastController;
        this.activatedRoute = activatedRoute;
        this.UserToEdit = new _models_users_models__WEBPACK_IMPORTED_MODULE_5__["UsersModel"]();
        this.EditUserInfo = false;
        this.listInstitutes = [];
        this.showPassword = false;
        this.showConfirmPassword = false;
        this.PasswordToggleIcon = 'eye';
        this.ConfirmPasswordToggleIcon = 'eye'; // Login Forms

        this.error_messages = {
          firstname: [{
            type: 'required',
            message: 'First Name is required.'
          }],
          lastname: [{
            type: 'required',
            message: 'Last Name is required.'
          }],
          email: [{
            type: 'required',
            message: 'Email is required.'
          }, {
            type: 'minlength',
            message: 'Email length.'
          }, {
            type: 'maxlength',
            message: 'Email length.'
          }, {
            type: 'required',
            message: 'please enter a valid email address.'
          }],
          password: [{
            type: 'required',
            message: 'password is required.'
          }, {
            type: 'minlength',
            message: 'password length.'
          }, {
            type: 'maxlength',
            message: 'password length.'
          }],
          confirmpassword: [{
            type: 'required',
            message: 'password is required.'
          }, {
            type: 'minlength',
            message: 'password length.'
          }, {
            type: 'maxlength',
            message: 'password length.'
          }]
        };
      }

      _createClass(AddUserPage, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          var _this = this;

          this.getAllInstitutes();
          this.initForm();
          this.activatedRoute.params.subscribe(function (params) {
            console.log('params', params);

            if (params !== {} && params.id !== undefined) {
              _this.GetUserById(params.id);

              _this.userId = params.id;
              _this.EditUserInfo = true;
            } else {
              _this.EditUserInfo = false;

              _this.initForm();
            }
          });
          this.getConnectedUserRole();
        }
      }, {
        key: "getConnectedUserRole",
        value: function getConnectedUserRole() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
            return regeneratorRuntime.wrap(function _callee$(_context) {
              while (1) {
                switch (_context.prev = _context.next) {
                  case 0:
                    _context.next = 2;
                    return this.userService.getUserRole();

                  case 2:
                    this.connectedUserRole = _context.sent;

                  case 3:
                  case "end":
                    return _context.stop();
                }
              }
            }, _callee, this);
          }));
        }
      }, {
        key: "getAllInstitutes",
        value: function getAllInstitutes() {
          var _this2 = this;

          this.institutService.getAllInstitutes().subscribe(function (data) {
            if (data.code === '0000') {
              console.log('data', data);
              _this2.listInstitutes = data.content;
            } else {
              console.log('error');
            }
          }, function (err) {
            console.log('subscription error', err);
          });
        }
      }, {
        key: "GetUserById",
        value: function GetUserById(userId) {
          var _this3 = this;

          console.log('params --->', userId);
          this.userService.getUserById(userId).subscribe(function (data) {
            if (data.code === '0000') {
              console.log('data content', data.content[0]);
              _this3.UserToEdit = new _models_users_models__WEBPACK_IMPORTED_MODULE_5__["UsersModel"](data.content[0]);
              console.log('this.InstitutToEdit', _this3.UserToEdit);

              _this3.initForm();
            } else {
              console.log('error', data);
            }
          }, function (err) {
            console.log('error', err);
          });
        }
      }, {
        key: "initForm",
        value: function initForm() {
          this.UsersForm = this.formBuilder.group({
            firstname: [this.UserToEdit.firstname, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required])],
            lastname: [this.UserToEdit.lastname, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required])],
            email: [this.UserToEdit.email, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(6), _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(30), _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')])],
            phone: [this.UserToEdit.phone],
            role: [this.UserToEdit.role],
            institutesids: [this.UserToEdit.institutesids],
            password: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(6), _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(30)])],
            confirmpassword: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(6), _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(30)])]
          }, {
            validators: this.password.bind(this)
          });
        }
      }, {
        key: "password",
        value: function password(formGroup) {
          var _formGroup$get = formGroup.get('password'),
              password = _formGroup$get.value;

          var _formGroup$get2 = formGroup.get('confirmpassword'),
              confirmPassword = _formGroup$get2.value;

          return password === confirmPassword ? null : {
            passwordNotMatch: true
          };
        }
      }, {
        key: "addUser",
        value: function addUser(UsersForm) {
          var _this4 = this;

          console.log('ngForm', UsersForm.value);
          var usertoSend = Object.assign({}, UsersForm.value);
          delete usertoSend.confirmpassword;
          console.log('usertosend', usertoSend);
          this.userService.addUser(UsersForm.value).subscribe(function (data) {
            if (data.code === '0000') {
              _this4.presentToast(data.message, 'success');

              console.log('user added with success');

              _this4.GoBackToList();
            } else {
              _this4.presentToast(data.message, 'error');

              console.log('error');
            }
          }, function (err) {
            _this4.presentToast(err, 'error');

            console.log('error', err);
          });
        }
      }, {
        key: "UpdateUser",
        value: function UpdateUser(UsersForm) {
          var _this5 = this;

          console.log('users.value', UsersForm.value, this.userId);
          var usertoUpdate = Object.assign({}, UsersForm.value);
          delete usertoUpdate.confirmpassword;
          delete usertoUpdate.password;
          console.log('usertoUpdate', usertoUpdate);
          this.userService.updateUser(this.userId, usertoUpdate).subscribe(function (data) {
            if (data.code === '0000') {
              _this5.presentToast(data.message, 'success');

              console.log('user updated with success');

              _this5.GoBackToList();
            } else {
              _this5.presentToast(data.message, 'error');

              console.log('error update');
            }
          }, function (err) {
            _this5.presentToast(err, 'error');

            console.log('error', err);
          });
        }
      }, {
        key: "presentToast",
        value: function presentToast(msg, messagetype) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
            var toast;
            return regeneratorRuntime.wrap(function _callee2$(_context2) {
              while (1) {
                switch (_context2.prev = _context2.next) {
                  case 0:
                    _context2.next = 2;
                    return this.toastController.create({
                      message: msg,
                      duration: 3000,
                      position: 'bottom',
                      color: messagetype === 'success' ? 'success' : 'danger'
                    });

                  case 2:
                    toast = _context2.sent;
                    toast.present();

                  case 4:
                  case "end":
                    return _context2.stop();
                }
              }
            }, _callee2, this);
          }));
        }
      }, {
        key: "GoBackToList",
        value: function GoBackToList() {
          this.router.navigate(['/users/users-list']);
        }
      }, {
        key: "togglePassword",
        value: function togglePassword() {
          this.showPassword = !this.showPassword;

          if (this.PasswordToggleIcon == 'eye') {
            this.PasswordToggleIcon = 'eye-off';
          } else {
            this.PasswordToggleIcon = 'eye';
          }
        }
      }]);

      return AddUserPage;
    }();

    AddUserPage.ctorParameters = function () {
      return [{
        type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]
      }, {
        type: src_app_services_users_service__WEBPACK_IMPORTED_MODULE_4__["UserService"]
      }, {
        type: src_app_services_institut_service__WEBPACK_IMPORTED_MODULE_7__["InstitutService"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["ToastController"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"]
      }];
    };

    AddUserPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-add-user',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./add-user.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/users/add-user/add-user.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./add-user.page.scss */
      "./src/app/users/add-user/add-user.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], src_app_services_users_service__WEBPACK_IMPORTED_MODULE_4__["UserService"], src_app_services_institut_service__WEBPACK_IMPORTED_MODULE_7__["InstitutService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["ToastController"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"]])], AddUserPage);
    /***/
  },

  /***/
  "./src/app/users/models/users.models.ts":
  /*!**********************************************!*\
    !*** ./src/app/users/models/users.models.ts ***!
    \**********************************************/

  /*! exports provided: UsersModel */

  /***/
  function srcAppUsersModelsUsersModelsTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "UsersModel", function () {
      return UsersModel;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _sharedModels_sharedModel__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! ../../sharedModels/sharedModel */
    "./src/app/sharedModels/sharedModel.ts");

    var UsersModel = /*#__PURE__*/function (_sharedModels_sharedM) {
      _inherits(UsersModel, _sharedModels_sharedM);

      var _super = _createSuper(UsersModel);

      function UsersModel(user) {
        var _this6;

        _classCallCheck(this, UsersModel);

        user = user || {};
        _this6 = _super.call(this, user);
        _this6.firstname = user.firstname || '';
        _this6.lastname = user.lastname || '';
        _this6.email = user.email || '';
        _this6.phone = user.phone || '';
        _this6.role = user.role || '';
        _this6.institutesids = user.institutesids || [];
        return _this6;
      }

      return UsersModel;
    }(_sharedModels_sharedModel__WEBPACK_IMPORTED_MODULE_1__["sharedModel"]);
    /***/

  }
}]);
//# sourceMappingURL=add-user-add-user-module-es5.js.map