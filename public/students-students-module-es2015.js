(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["students-students-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/students/students.page.html":
/*!***********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/students/students.page.html ***!
  \***********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\r\n  <ion-toolbar>\r\n    <ion-title>students</ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/students/students-routing.module.ts":
/*!*****************************************************!*\
  !*** ./src/app/students/students-routing.module.ts ***!
  \*****************************************************/
/*! exports provided: StudentsPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StudentsPageRoutingModule", function() { return StudentsPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");



const routes = [
    {
        path: '',
        redirectTo: 'students-list',
        pathMatch: 'full'
    },
    {
        path: 'students-list/:id',
        loadChildren: () => __webpack_require__.e(/*! import() | students-list-students-list-module */ "students-list-students-list-module").then(__webpack_require__.bind(null, /*! ./students-list/students-list.module */ "./src/app/students/students-list/students-list.module.ts")).then(m => m.StudentsListPageModule)
    },
    {
        path: 'add-student/:id',
        loadChildren: () => Promise.all(/*! import() | add-student-add-student-module */[__webpack_require__.e("common"), __webpack_require__.e("add-student-add-student-module")]).then(__webpack_require__.bind(null, /*! ./add-student/add-student.module */ "./src/app/students/add-student/add-student.module.ts")).then(m => m.AddStudentPageModule)
    },
    {
        path: 'student-details/:id',
        loadChildren: () => __webpack_require__.e(/*! import() | student-details-student-details-module */ "student-details-student-details-module").then(__webpack_require__.bind(null, /*! ./student-details/student-details.module */ "./src/app/students/student-details/student-details.module.ts")).then(m => m.StudentDetailsPageModule)
    },
    {
        path: 'edit-student/:id',
        loadChildren: () => Promise.all(/*! import() | add-student-add-student-module */[__webpack_require__.e("common"), __webpack_require__.e("add-student-add-student-module")]).then(__webpack_require__.bind(null, /*! ./add-student/add-student.module */ "./src/app/students/add-student/add-student.module.ts")).then(m => m.AddStudentPageModule)
    },
    {
        path: '**',
        redirectTo: 'students-list'
    },
    {
        path: 'account/:id',
        loadChildren: () => __webpack_require__.e(/*! import() | account-account-module */ "account-account-module").then(__webpack_require__.bind(null, /*! ./account/account.module */ "./src/app/students/account/account.module.ts")).then(m => m.AccountPageModule)
    }
];
let StudentsPageRoutingModule = class StudentsPageRoutingModule {
};
StudentsPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], StudentsPageRoutingModule);



/***/ }),

/***/ "./src/app/students/students.module.ts":
/*!*********************************************!*\
  !*** ./src/app/students/students.module.ts ***!
  \*********************************************/
/*! exports provided: StudentsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StudentsPageModule", function() { return StudentsPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _students_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./students-routing.module */ "./src/app/students/students-routing.module.ts");
/* harmony import */ var _students_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./students.page */ "./src/app/students/students.page.ts");







let StudentsPageModule = class StudentsPageModule {
};
StudentsPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _students_routing_module__WEBPACK_IMPORTED_MODULE_5__["StudentsPageRoutingModule"]
        ],
        declarations: [_students_page__WEBPACK_IMPORTED_MODULE_6__["StudentsPage"]]
    })
], StudentsPageModule);



/***/ }),

/***/ "./src/app/students/students.page.scss":
/*!*********************************************!*\
  !*** ./src/app/students/students.page.scss ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3N0dWRlbnRzL3N0dWRlbnRzLnBhZ2Uuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/students/students.page.ts":
/*!*******************************************!*\
  !*** ./src/app/students/students.page.ts ***!
  \*******************************************/
/*! exports provided: StudentsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StudentsPage", function() { return StudentsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let StudentsPage = class StudentsPage {
    constructor() { }
    ngOnInit() {
    }
};
StudentsPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-students',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./students.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/students/students.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./students.page.scss */ "./src/app/students/students.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], StudentsPage);



/***/ })

}]);
//# sourceMappingURL=students-students-module-es2015.js.map