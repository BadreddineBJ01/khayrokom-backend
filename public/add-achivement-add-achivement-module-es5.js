function _createSuper(Derived) { return function () { var Super = _getPrototypeOf(Derived), result; if (_isNativeReflectConstruct()) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (typeof call === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["add-achivement-add-achivement-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/achievements/add-achivement/add-achivement.page.html":
  /*!************************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/achievements/add-achivement/add-achivement.page.html ***!
    \************************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppAchievementsAddAchivementAddAchivementPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\r\n  <ion-toolbar>\r\n    <ion-title *ngIf=\"!EditAchivementInfo\">Add achivement</ion-title>\r\n    <ion-title *ngIf=\"EditAchivementInfo\">Edit achivement</ion-title>\r\n    <ion-buttons slot=\"start\">\r\n      <ion-menu-button autoHide=\"false\"></ion-menu-button>\r\n      <ion-back-button></ion-back-button>\r\n    </ion-buttons>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-item>\r\n  <ion-icon name=\"person\"></ion-icon>\r\n  <ion-label style=\"padding-left: 30px;\" *ngIf='studentInfo?.firstname'>{{studentInfo?.firstname}} {{studentInfo?.lastname}}</ion-label>\r\n</ion-item>\r\n\r\n<ion-content>\r\n  <ion-card>\r\n    <form [formGroup]=\"AchivementForm\">\r\n      <ion-select formControlName=\"program\" placeholder=\"program\" required>\r\n        <ion-select-option value=\"Memorize\">Memorize</ion-select-option>\r\n        <ion-select-option value=\"Revision\">Revision</ion-select-option>\r\n        <ion-select-option value=\"Exam\">Exam</ion-select-option>\r\n      </ion-select>\r\n      <ion-select lines=\"inset\" formControlName=\"surah\" placeholder=\"surah\">\r\n        <ion-select-option\r\n          *ngFor=\"let sura of suratsArray\"\r\n          value=\"{{sura.surah}}\"\r\n          >{{sura.name}}</ion-select-option>\r\n      </ion-select>\r\n      <ion-item lines=\"inset\">\r\n        <ion-input type=\"number\" formControlName=\"fromVerse\" placeholder=\"fromVerse\">\r\n        </ion-input>\r\n      </ion-item>\r\n      <ion-item lines=\"inset\">\r\n        <ion-input type=\"number\" formControlName=\"toVerse\" placeholder=\"toVerse\"> </ion-input>\r\n      </ion-item>\r\n      <ion-item lines=\"inset\">\r\n        <ion-input formControlName=\"evaluation\" placeholder=\"evaluation\">\r\n        </ion-input>\r\n      </ion-item>\r\n      <ion-item lines=\"inset\">\r\n        <ion-input formControlName=\"notes\" placeholder=\"notes\"> </ion-input>\r\n      </ion-item>\r\n      <ion-datetime\r\n        placeholder=\"MM/DD/YYYY\"\r\n        displayFormat=\"MM/DD/YYYY\"\r\n        formControlName=\"achievDate\"\r\n        value=\"achievDate\"\r\n      ></ion-datetime>\r\n    </form>\r\n    <ion-button\r\n      shape=\"round\"\r\n      expand=\"block\"\r\n      *ngIf=\"!EditAchivementInfo\"\r\n      (click)=\"addAchivement(AchivementForm)\"\r\n      [disabled]=\"!AchivementForm.valid\"\r\n      >submit</ion-button\r\n    >\r\n    <ion-button\r\n      shape=\"round\"\r\n      expand=\"block\"\r\n      *ngIf=\"EditAchivementInfo\"\r\n      (click)=\"UpdateAchivement(AchivementForm)\"\r\n      [disabled]=\"!AchivementForm.valid\"\r\n      >Update</ion-button\r\n    >\r\n  </ion-card>\r\n</ion-content>\r\n";
    /***/
  },

  /***/
  "./src/app/achievements/add-achivement/add-achivement-routing.module.ts":
  /*!******************************************************************************!*\
    !*** ./src/app/achievements/add-achivement/add-achivement-routing.module.ts ***!
    \******************************************************************************/

  /*! exports provided: AddAchivementPageRoutingModule */

  /***/
  function srcAppAchievementsAddAchivementAddAchivementRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AddAchivementPageRoutingModule", function () {
      return AddAchivementPageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _add_achivement_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./add-achivement.page */
    "./src/app/achievements/add-achivement/add-achivement.page.ts");

    var routes = [{
      path: '',
      component: _add_achivement_page__WEBPACK_IMPORTED_MODULE_3__["AddAchivementPage"]
    }];

    var AddAchivementPageRoutingModule = function AddAchivementPageRoutingModule() {
      _classCallCheck(this, AddAchivementPageRoutingModule);
    };

    AddAchivementPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], AddAchivementPageRoutingModule);
    /***/
  },

  /***/
  "./src/app/achievements/add-achivement/add-achivement.module.ts":
  /*!**********************************************************************!*\
    !*** ./src/app/achievements/add-achivement/add-achivement.module.ts ***!
    \**********************************************************************/

  /*! exports provided: AddAchivementPageModule */

  /***/
  function srcAppAchievementsAddAchivementAddAchivementModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AddAchivementPageModule", function () {
      return AddAchivementPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _add_achivement_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./add-achivement-routing.module */
    "./src/app/achievements/add-achivement/add-achivement-routing.module.ts");
    /* harmony import */


    var _add_achivement_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./add-achivement.page */
    "./src/app/achievements/add-achivement/add-achivement.page.ts");
    /* harmony import */


    var src_app_shared_module_shared_module_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! src/app/shared-module/shared-module.module */
    "./src/app/shared-module/shared-module.module.ts");

    var AddAchivementPageModule = function AddAchivementPageModule() {
      _classCallCheck(this, AddAchivementPageModule);
    };

    AddAchivementPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], src_app_shared_module_shared_module_module__WEBPACK_IMPORTED_MODULE_7__["SharedModuleModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _add_achivement_routing_module__WEBPACK_IMPORTED_MODULE_5__["AddAchivementPageRoutingModule"]],
      declarations: [_add_achivement_page__WEBPACK_IMPORTED_MODULE_6__["AddAchivementPage"]]
    })], AddAchivementPageModule);
    /***/
  },

  /***/
  "./src/app/achievements/add-achivement/add-achivement.page.scss":
  /*!**********************************************************************!*\
    !*** ./src/app/achievements/add-achivement/add-achivement.page.scss ***!
    \**********************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppAchievementsAddAchivementAddAchivementPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FjaGlldmVtZW50cy9hZGQtYWNoaXZlbWVudC9hZGQtYWNoaXZlbWVudC5wYWdlLnNjc3MifQ== */";
    /***/
  },

  /***/
  "./src/app/achievements/add-achivement/add-achivement.page.ts":
  /*!********************************************************************!*\
    !*** ./src/app/achievements/add-achivement/add-achivement.page.ts ***!
    \********************************************************************/

  /*! exports provided: AddAchivementPage */

  /***/
  function srcAppAchievementsAddAchivementAddAchivementPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AddAchivementPage", function () {
      return AddAchivementPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var src_app_services_achivements_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! src/app/services/achivements.service */
    "./src/app/services/achivements.service.ts");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _models_Achivement_model__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ../models/Achivement.model */
    "./src/app/achievements/models/Achivement.model.ts");
    /* harmony import */


    var rxjs_operators__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! rxjs/operators */
    "./node_modules/rxjs/_esm2015/operators/index.js");
    /* harmony import */


    var src_app_services_communication_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! src/app/services/communication.service */
    "./src/app/services/communication.service.ts");
    /* harmony import */


    var _models_QuranSurat__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! ../models/QuranSurat */
    "./src/app/achievements/models/QuranSurat.ts");
    /* harmony import */


    var src_app_services_Students_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! src/app/services/Students.service */
    "./src/app/services/Students.service.ts");

    var AddAchivementPage = /*#__PURE__*/function () {
      function AddAchivementPage(formBuilder, router, achivementService, activatedRoute, communicationService, studentsService) {
        _classCallCheck(this, AddAchivementPage);

        this.formBuilder = formBuilder;
        this.router = router;
        this.achivementService = achivementService;
        this.activatedRoute = activatedRoute;
        this.communicationService = communicationService;
        this.studentsService = studentsService;
        this.AchivementToEdit = new _models_Achivement_model__WEBPACK_IMPORTED_MODULE_5__["AchivementModel"]();
        this.EditAchivementInfo = false;
        this.suratsArray = [];
      }

      _createClass(AddAchivementPage, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          var _this = this;

          this.loadAllSurat();
          this.StudentId = this.communicationService.StudentId; //  this.getStudentById();

          this.initForm();
          this.router.events.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["switchMap"])(function (navigation) {
            return _this.activatedRoute.params.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["map"])(function (params) {
              return {
                navigation: navigation,
                params: params
              };
            }));
          })).subscribe(function (result) {
            if (result.navigation instanceof _angular_router__WEBPACK_IMPORTED_MODULE_4__["NavigationEnd"]) {
              var urlfragment = result.navigation.url.split('/')[2];

              if (urlfragment.toLowerCase().includes('add') && result.params.id !== undefined && result.params !== {}) {
                _this.StudentId = _this.communicationService.StudentId = result.params.id;

                _this.getStudentById();

                _this.EditAchivementInfo = false;

                _this.initForm();
              } else if (urlfragment.toLowerCase().includes('edit') && result.params.id !== undefined && result.params !== {}) {
                _this.GetAchivementById(result.params.id);

                _this.achivementId = result.params.id;
                _this.EditAchivementInfo = true;
              } else {
                console.log('error id undefined');
              }
            }
          });
        }
      }, {
        key: "getStudentById",
        value: function getStudentById() {
          var _this2 = this;

          this.studentsService.getStudentById(this.StudentId).subscribe(function (res) {
            if (res.code == '0000') {
              _this2.studentInfo = res.content[0];
            } else {
              console.log('error');
            }
          }, function (err) {
            console.log('Error');
          });
        }
      }, {
        key: "loadAllSurat",
        value: function loadAllSurat() {
          this.suratsArray = _models_QuranSurat__WEBPACK_IMPORTED_MODULE_8__["surats"];
        }
      }, {
        key: "GetAchivementById",
        value: function GetAchivementById(AchivementId) {
          var _this3 = this;

          console.log('params --->', AchivementId);
          this.achivementService.getAchivementById(AchivementId).subscribe(function (data) {
            if (data.code === '0000') {
              console.log('data content', data.content[0]);
              _this3.AchivementToEdit = new _models_Achivement_model__WEBPACK_IMPORTED_MODULE_5__["AchivementModel"](data.content[0]);
              console.log('this.AchivementToEdit', _this3.AchivementToEdit);

              _this3.initForm();
            } else {
              console.log('error', data);
            }
          }, function (err) {
            console.log('error', err);
          });
        }
      }, {
        key: "initForm",
        value: function initForm() {
          this.AchivementForm = this.formBuilder.group({
            program: [this.AchivementToEdit.program, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            surah: [this.AchivementToEdit.surah],
            fromVerse: [this.AchivementToEdit.fromVerse],
            toVerse: [this.AchivementToEdit.toVerse],
            evaluation: [this.AchivementToEdit.evaluation],
            notes: [this.AchivementToEdit.notes],
            achievDate: [this.AchivementToEdit.achievDate]
          });
        }
      }, {
        key: "addAchivement",
        value: function addAchivement(AchivementForm) {
          var _this4 = this;

          var achivementToAdd = {
            program: '',
            surah: '',
            fromVerse: '',
            toVerse: '',
            evaluation: '',
            notes: '',
            achievDate: '',
            idstudent: ''
          };
          Object.assign(achivementToAdd, AchivementForm.value);
          achivementToAdd.idstudent = this.StudentId;
          console.log('ngForm', achivementToAdd);
          this.achivementService.addAchivement(achivementToAdd).subscribe(function (data) {
            if (data.code === '0000') {
              console.log('achivement added with success');

              _this4.GoBackToList();
            } else {
              console.log('error');
            }
          }, function (err) {
            console.log('error', err);
          });
        }
      }, {
        key: "UpdateAchivement",
        value: function UpdateAchivement(AchivementForm) {
          var _this5 = this;

          var achivementToAdd = {
            program: '',
            surah: '',
            fromVerse: '',
            toVerse: '',
            evaluation: '',
            notes: '',
            achievDate: '',
            idstudent: ''
          };
          Object.assign(achivementToAdd, AchivementForm.value);
          achivementToAdd.idstudent = this.StudentId;
          this.achivementService.updateAchivement(this.achivementId, achivementToAdd).subscribe(function (data) {
            if (data.code === '0000') {
              console.log('achivement updated with success');

              _this5.GoBackToList();
            } else {
              console.log('error update');
            }
          }, function (err) {
            console.log('error', err);
          });
        }
      }, {
        key: "GoBackToList",
        value: function GoBackToList() {
          this.router.navigate(['/achievements/achivement-list/' + this.StudentId]);
        }
      }]);

      return AddAchivementPage;
    }();

    AddAchivementPage.ctorParameters = function () {
      return [{
        type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]
      }, {
        type: src_app_services_achivements_service__WEBPACK_IMPORTED_MODULE_3__["AchivementService"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"]
      }, {
        type: src_app_services_communication_service__WEBPACK_IMPORTED_MODULE_7__["CommunicationService"]
      }, {
        type: src_app_services_Students_service__WEBPACK_IMPORTED_MODULE_9__["StudentsService"]
      }];
    };

    AddAchivementPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-add-achivement',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./add-achivement.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/achievements/add-achivement/add-achivement.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./add-achivement.page.scss */
      "./src/app/achievements/add-achivement/add-achivement.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"], src_app_services_achivements_service__WEBPACK_IMPORTED_MODULE_3__["AchivementService"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"], src_app_services_communication_service__WEBPACK_IMPORTED_MODULE_7__["CommunicationService"], src_app_services_Students_service__WEBPACK_IMPORTED_MODULE_9__["StudentsService"]])], AddAchivementPage);
    /***/
  },

  /***/
  "./src/app/achievements/models/Achivement.model.ts":
  /*!*********************************************************!*\
    !*** ./src/app/achievements/models/Achivement.model.ts ***!
    \*********************************************************/

  /*! exports provided: AchivementModel */

  /***/
  function srcAppAchievementsModelsAchivementModelTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AchivementModel", function () {
      return AchivementModel;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _sharedModels_sharedModel__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! ../../sharedModels/sharedModel */
    "./src/app/sharedModels/sharedModel.ts");

    var AchivementModel = /*#__PURE__*/function (_sharedModels_sharedM) {
      _inherits(AchivementModel, _sharedModels_sharedM);

      var _super = _createSuper(AchivementModel);

      function AchivementModel(AchivementEntity) {
        var _this6;

        _classCallCheck(this, AchivementModel);

        AchivementEntity = AchivementEntity || {};
        _this6 = _super.call(this, AchivementEntity);
        _this6.program = AchivementEntity.program || '';
        _this6.surah = AchivementEntity.surah || '';
        _this6.fromVerse = AchivementEntity.fromVerse || '';
        _this6.toVerse = AchivementEntity.toVerse || '';
        _this6.evaluation = AchivementEntity.evaluation || '';
        _this6.notes = AchivementEntity.notes || '';
        _this6.achievDate = AchivementEntity.achievDate || '';
        _this6.idstudent = AchivementEntity.idstudent || '';
        _this6.created_at = AchivementEntity.created_at || '';
        return _this6;
      }

      return AchivementModel;
    }(_sharedModels_sharedModel__WEBPACK_IMPORTED_MODULE_1__["sharedModel"]);
    /***/

  }
}]);
//# sourceMappingURL=add-achivement-add-achivement-module-es5.js.map