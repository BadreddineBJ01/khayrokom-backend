function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["classes-classes-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/classes/classes.page.html":
  /*!*********************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/classes/classes.page.html ***!
    \*********************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppClassesClassesPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\r\n  <ion-toolbar>\r\n    <ion-title>classes</ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n\r\n</ion-content>\r\n";
    /***/
  },

  /***/
  "./src/app/classes/classes-routing.module.ts":
  /*!***************************************************!*\
    !*** ./src/app/classes/classes-routing.module.ts ***!
    \***************************************************/

  /*! exports provided: ClassesPageRoutingModule */

  /***/
  function srcAppClassesClassesRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ClassesPageRoutingModule", function () {
      return ClassesPageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");

    var routes = [{
      path: '',
      redirectTo: 'classe-list',
      pathMatch: 'full'
    }, {
      path: 'add-class/:id',
      loadChildren: function loadChildren() {
        return Promise.all(
        /*! import() | add-class-add-class-module */
        [__webpack_require__.e("common"), __webpack_require__.e("add-class-add-class-module")]).then(__webpack_require__.bind(null,
        /*! ./add-class/add-class.module */
        "./src/app/classes/add-class/add-class.module.ts")).then(function (m) {
          return m.AddClassPageModule;
        });
      }
    }, {
      path: 'classe-list/:id',
      loadChildren: function loadChildren() {
        return __webpack_require__.e(
        /*! import() | classe-list-classe-list-module */
        "classe-list-classe-list-module").then(__webpack_require__.bind(null,
        /*! ./classe-list/classe-list.module */
        "./src/app/classes/classe-list/classe-list.module.ts")).then(function (m) {
          return m.ClasseListPageModule;
        });
      }
    }, {
      path: 'class-details/:id',
      loadChildren: function loadChildren() {
        return __webpack_require__.e(
        /*! import() | class-details-class-details-module */
        "class-details-class-details-module").then(__webpack_require__.bind(null,
        /*! ./class-details/class-details.module */
        "./src/app/classes/class-details/class-details.module.ts")).then(function (m) {
          return m.ClassDetailsPageModule;
        });
      }
    }, {
      path: 'edit-class/:id',
      loadChildren: function loadChildren() {
        return Promise.all(
        /*! import() | add-class-add-class-module */
        [__webpack_require__.e("common"), __webpack_require__.e("add-class-add-class-module")]).then(__webpack_require__.bind(null,
        /*! ./add-class/add-class.module */
        "./src/app/classes/add-class/add-class.module.ts")).then(function (m) {
          return m.AddClassPageModule;
        });
      }
    }, {
      path: '**',
      redirectTo: 'classe-list'
    }];

    var ClassesPageRoutingModule = function ClassesPageRoutingModule() {
      _classCallCheck(this, ClassesPageRoutingModule);
    };

    ClassesPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], ClassesPageRoutingModule);
    /***/
  },

  /***/
  "./src/app/classes/classes.module.ts":
  /*!*******************************************!*\
    !*** ./src/app/classes/classes.module.ts ***!
    \*******************************************/

  /*! exports provided: ClassesPageModule */

  /***/
  function srcAppClassesClassesModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ClassesPageModule", function () {
      return ClassesPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _classes_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./classes-routing.module */
    "./src/app/classes/classes-routing.module.ts");
    /* harmony import */


    var _classes_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./classes.page */
    "./src/app/classes/classes.page.ts");

    var ClassesPageModule = function ClassesPageModule() {
      _classCallCheck(this, ClassesPageModule);
    };

    ClassesPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _classes_routing_module__WEBPACK_IMPORTED_MODULE_5__["ClassesPageRoutingModule"]],
      declarations: [_classes_page__WEBPACK_IMPORTED_MODULE_6__["ClassesPage"]]
    })], ClassesPageModule);
    /***/
  },

  /***/
  "./src/app/classes/classes.page.scss":
  /*!*******************************************!*\
    !*** ./src/app/classes/classes.page.scss ***!
    \*******************************************/

  /*! exports provided: default */

  /***/
  function srcAppClassesClassesPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NsYXNzZXMvY2xhc3Nlcy5wYWdlLnNjc3MifQ== */";
    /***/
  },

  /***/
  "./src/app/classes/classes.page.ts":
  /*!*****************************************!*\
    !*** ./src/app/classes/classes.page.ts ***!
    \*****************************************/

  /*! exports provided: ClassesPage */

  /***/
  function srcAppClassesClassesPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ClassesPage", function () {
      return ClassesPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");

    var ClassesPage = /*#__PURE__*/function () {
      function ClassesPage() {
        _classCallCheck(this, ClassesPage);
      }

      _createClass(ClassesPage, [{
        key: "ngOnInit",
        value: function ngOnInit() {}
      }]);

      return ClassesPage;
    }();

    ClassesPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-classes',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./classes.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/classes/classes.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./classes.page.scss */
      "./src/app/classes/classes.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])], ClassesPage);
    /***/
  }
}]);
//# sourceMappingURL=classes-classes-module-es5.js.map