(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["add-institut-add-institut-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/institutes/add-institut/add-institut.page.html":
/*!******************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/institutes/add-institut/add-institut.page.html ***!
  \******************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\r\n  <ion-toolbar>\r\n    <ion-title *ngIf='!EditInstitutInfo'>Add Institut</ion-title>\r\n    <ion-title *ngIf='EditInstitutInfo'>Edit institut</ion-title>\r\n    <ion-buttons slot=\"start\">\r\n    <ion-menu-button autoHide=\"false\"></ion-menu-button>\r\n    <ion-back-button></ion-back-button>\r\n  </ion-buttons>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <ion-card>\r\n  <form  [formGroup]=\"InstitutForm\">\r\n    <ion-item lines=\"inset\">\r\n      <ion-input  formControlName=\"InstituteName\" placeholder='name *' required></ion-input>\r\n    </ion-item>\r\n    <div>\r\n      <ng-container *ngFor=\"let error of error_messages.InstituteName\">\r\n        <ion-text class=\"ion-padding\" \r\n                  color=\"danger\"\r\n                  *ngIf=\"InstitutForm.get('InstituteName').hasError(error.type) && (InstitutForm.get('InstituteName').dirty || InstitutForm.get('InstituteName').touched)\">\r\n          {{ error.message }}\r\n        </ion-text>\r\n      </ng-container>\r\n    </div>\r\n        <ion-select lines=\"inset\" formControlName=\"country\" placeholder=\"country\">\r\n            <ion-select-option\r\n              *ngFor=\"let country of countriesArray\"\r\n              value=\"{{country.id}}\">{{country.name}}</ion-select-option>\r\n            </ion-select>\r\n    <ion-item lines=\"inset\">\r\n      <ion-input formControlName=\"city\" placeholder='city'>\r\n      </ion-input>\r\n    </ion-item>\r\n  </form>\r\n  <ion-button  \r\n  shape=\"round\"\r\n  expand=\"block\" \r\n  *ngIf='!EditInstitutInfo' \r\n  (click)='addInstitut(InstitutForm)'\r\n   [disabled]='!InstitutForm.valid'>submit</ion-button>\r\n  <ion-button  \r\n   shape=\"round\"\r\n  expand=\"block\"  \r\n  *ngIf='EditInstitutInfo' \r\n  (click)='UpdateInstitut(InstitutForm)' \r\n  [disabled]='!InstitutForm.valid'>Update</ion-button>  \r\n</ion-card>\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/institutes/add-institut/add-institut-routing.module.ts":
/*!************************************************************************!*\
  !*** ./src/app/institutes/add-institut/add-institut-routing.module.ts ***!
  \************************************************************************/
/*! exports provided: AddInstitutPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddInstitutPageRoutingModule", function() { return AddInstitutPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _add_institut_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./add-institut.page */ "./src/app/institutes/add-institut/add-institut.page.ts");




const routes = [
    {
        path: '',
        component: _add_institut_page__WEBPACK_IMPORTED_MODULE_3__["AddInstitutPage"]
    }
];
let AddInstitutPageRoutingModule = class AddInstitutPageRoutingModule {
};
AddInstitutPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], AddInstitutPageRoutingModule);



/***/ }),

/***/ "./src/app/institutes/add-institut/add-institut.module.ts":
/*!****************************************************************!*\
  !*** ./src/app/institutes/add-institut/add-institut.module.ts ***!
  \****************************************************************/
/*! exports provided: AddInstitutPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddInstitutPageModule", function() { return AddInstitutPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _add_institut_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./add-institut-routing.module */ "./src/app/institutes/add-institut/add-institut-routing.module.ts");
/* harmony import */ var _add_institut_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./add-institut.page */ "./src/app/institutes/add-institut/add-institut.page.ts");
/* harmony import */ var src_app_shared_module_shared_module_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/shared-module/shared-module.module */ "./src/app/shared-module/shared-module.module.ts");







let AddInstitutPageModule = class AddInstitutPageModule {
};
AddInstitutPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            src_app_shared_module_shared_module_module__WEBPACK_IMPORTED_MODULE_6__["SharedModuleModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["IonicModule"],
            _add_institut_routing_module__WEBPACK_IMPORTED_MODULE_4__["AddInstitutPageRoutingModule"]
        ],
        declarations: [_add_institut_page__WEBPACK_IMPORTED_MODULE_5__["AddInstitutPage"]]
    })
], AddInstitutPageModule);



/***/ }),

/***/ "./src/app/institutes/add-institut/add-institut.page.scss":
/*!****************************************************************!*\
  !*** ./src/app/institutes/add-institut/add-institut.page.scss ***!
  \****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2luc3RpdHV0ZXMvYWRkLWluc3RpdHV0L2FkZC1pbnN0aXR1dC5wYWdlLnNjc3MifQ== */");

/***/ }),

/***/ "./src/app/institutes/add-institut/add-institut.page.ts":
/*!**************************************************************!*\
  !*** ./src/app/institutes/add-institut/add-institut.page.ts ***!
  \**************************************************************/
/*! exports provided: AddInstitutPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddInstitutPage", function() { return AddInstitutPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var src_app_services_institut_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/institut.service */ "./src/app/services/institut.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _models_Institut_model__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../models/Institut.model */ "./src/app/institutes/models/Institut.model.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _models_Countries__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../models/Countries */ "./src/app/institutes/models/Countries.ts");








let AddInstitutPage = class AddInstitutPage {
    constructor(toastController, formBuilder, router, institutService, activatedRoute) {
        this.toastController = toastController;
        this.formBuilder = formBuilder;
        this.router = router;
        this.institutService = institutService;
        this.activatedRoute = activatedRoute;
        this.InstitutToEdit = new _models_Institut_model__WEBPACK_IMPORTED_MODULE_5__["InstitutModel"]();
        this.EditInstitutInfo = false;
        this.countriesArray = [];
        this.error_messages = {
            InstituteName: [
                { type: 'required', message: 'Institut name is required.' },
            ],
        };
    }
    ngOnInit() {
        this.loadAllCountries();
        this.initForm();
        this.activatedRoute.params.subscribe((params) => {
            console.log('params', params);
            if (params !== {} && params.id !== undefined) {
                this.GetInstitutById(params.id);
                this.insitutId = params.id;
                this.EditInstitutInfo = true;
            }
            else {
                this.EditInstitutInfo = false;
                this.initForm();
            }
        });
    }
    loadAllCountries() {
        this.countriesArray = _models_Countries__WEBPACK_IMPORTED_MODULE_7__["countries"];
    }
    GetInstitutById(InstitutId) {
        console.log('params --->', InstitutId);
        this.institutService.getInstitutById(InstitutId).subscribe((data) => {
            if (data.code === '0000') {
                console.log('data content', data.content[0]);
                this.InstitutToEdit = new _models_Institut_model__WEBPACK_IMPORTED_MODULE_5__["InstitutModel"](data.content[0]);
                console.log('this.InstitutToEdit', this.InstitutToEdit);
                this.initForm();
            }
            else {
                console.log('error', data);
            }
        }, (err) => {
            console.log('error', err);
        });
    }
    initForm() {
        this.InstitutForm = this.formBuilder.group({
            InstituteName: [this.InstitutToEdit.InstituteName, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            country: [this.InstitutToEdit.country],
            city: [this.InstitutToEdit.city],
        });
    }
    addInstitut(InstitutForm) {
        console.log('ngForm', InstitutForm.value);
        this.institutService.addInstitut(InstitutForm.value).subscribe((data) => {
            if (data.code === '0000') {
                this.presentToast(data.message, 'success');
                console.log('institut added with success');
                this.GoBackToList();
            }
            else {
                this.presentToast(data.message, 'error');
                console.log('error');
            }
        }, (err) => {
            this.presentToast(err, 'error');
            console.log('error', err);
        });
    }
    UpdateInstitut(InstitutForm) {
        console.log('InstittForm.value', InstitutForm.value, this.insitutId);
        this.institutService
            .UpdateInstitut(this.insitutId, InstitutForm.value)
            .subscribe((data) => {
            if (data.code === '0000') {
                this.presentToast(data.message, 'success');
                console.log('institut updated with success');
                this.GoBackToList();
            }
            else {
                this.presentToast(data.message, 'error');
                console.log('error update');
            }
        }, (err) => {
            this.presentToast(err, 'error');
            console.log('error', err);
        });
    }
    GoBackToList() {
        this.router.navigate(['/institutes/institues-list']);
    }
    presentToast(msg, messagetype) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const toast = yield this.toastController.create({
                message: msg,
                duration: 3000,
                position: 'bottom',
                color: messagetype === 'success' ? 'success' : 'danger',
            });
            toast.present();
        });
    }
};
AddInstitutPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["ToastController"] },
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
    { type: src_app_services_institut_service__WEBPACK_IMPORTED_MODULE_3__["InstitutService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"] }
];
AddInstitutPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-add-institut',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./add-institut.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/institutes/add-institut/add-institut.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./add-institut.page.scss */ "./src/app/institutes/add-institut/add-institut.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_6__["ToastController"],
        _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
        _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"],
        src_app_services_institut_service__WEBPACK_IMPORTED_MODULE_3__["InstitutService"],
        _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"]])
], AddInstitutPage);



/***/ }),

/***/ "./src/app/institutes/models/Institut.model.ts":
/*!*****************************************************!*\
  !*** ./src/app/institutes/models/Institut.model.ts ***!
  \*****************************************************/
/*! exports provided: InstitutModel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InstitutModel", function() { return InstitutModel; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _sharedModels_sharedModel__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../sharedModels/sharedModel */ "./src/app/sharedModels/sharedModel.ts");


class InstitutModel extends _sharedModels_sharedModel__WEBPACK_IMPORTED_MODULE_1__["sharedModel"] {
    constructor(InstitutEntity) {
        InstitutEntity = InstitutEntity || {};
        super(InstitutEntity);
        this.InstituteName = InstitutEntity.InstituteName || '';
        this.country = InstitutEntity.country || '';
        this.city = InstitutEntity.city || '';
    }
}


/***/ })

}]);
//# sourceMappingURL=add-institut-add-institut-module-es2015.js.map