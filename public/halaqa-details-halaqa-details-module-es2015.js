(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["halaqa-details-halaqa-details-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/halaqat/halaqa-details/halaqa-details.page.html":
/*!*******************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/halaqat/halaqa-details/halaqa-details.page.html ***!
  \*******************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\r\n  <ion-toolbar>\r\n    <ion-title>Halaqa details</ion-title>\r\n    <ion-buttons slot=\"start\">\r\n        <ion-menu-button autoHide=\"false\"></ion-menu-button>\r\n        <ion-back-button></ion-back-button>\r\n      </ion-buttons>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <ion-card *ngIf='halaqaDetails'>\r\n    <ion-item>\r\n      <ion-label>name</ion-label>\r\n      <ion-label *ngIf='halaqaDetails.Halaqaname; else noData'>{{halaqaDetails.Halaqaname}}</ion-label>\r\n    </ion-item>\r\n  </ion-card>\r\n<ng-template #noData>\r\n <ion-label>\r\n   N/A\r\n  </ion-label>\r\n</ng-template>\r\n\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/halaqat/halaqa-details/halaqa-details-routing.module.ts":
/*!*************************************************************************!*\
  !*** ./src/app/halaqat/halaqa-details/halaqa-details-routing.module.ts ***!
  \*************************************************************************/
/*! exports provided: HalaqaDetailsPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HalaqaDetailsPageRoutingModule", function() { return HalaqaDetailsPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _halaqa_details_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./halaqa-details.page */ "./src/app/halaqat/halaqa-details/halaqa-details.page.ts");




const routes = [
    {
        path: '',
        component: _halaqa_details_page__WEBPACK_IMPORTED_MODULE_3__["HalaqaDetailsPage"]
    }
];
let HalaqaDetailsPageRoutingModule = class HalaqaDetailsPageRoutingModule {
};
HalaqaDetailsPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], HalaqaDetailsPageRoutingModule);



/***/ }),

/***/ "./src/app/halaqat/halaqa-details/halaqa-details.module.ts":
/*!*****************************************************************!*\
  !*** ./src/app/halaqat/halaqa-details/halaqa-details.module.ts ***!
  \*****************************************************************/
/*! exports provided: HalaqaDetailsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HalaqaDetailsPageModule", function() { return HalaqaDetailsPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _halaqa_details_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./halaqa-details-routing.module */ "./src/app/halaqat/halaqa-details/halaqa-details-routing.module.ts");
/* harmony import */ var _halaqa_details_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./halaqa-details.page */ "./src/app/halaqat/halaqa-details/halaqa-details.page.ts");







let HalaqaDetailsPageModule = class HalaqaDetailsPageModule {
};
HalaqaDetailsPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _halaqa_details_routing_module__WEBPACK_IMPORTED_MODULE_5__["HalaqaDetailsPageRoutingModule"]
        ],
        declarations: [_halaqa_details_page__WEBPACK_IMPORTED_MODULE_6__["HalaqaDetailsPage"]]
    })
], HalaqaDetailsPageModule);



/***/ }),

/***/ "./src/app/halaqat/halaqa-details/halaqa-details.page.scss":
/*!*****************************************************************!*\
  !*** ./src/app/halaqat/halaqa-details/halaqa-details.page.scss ***!
  \*****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2hhbGFxYXQvaGFsYXFhLWRldGFpbHMvaGFsYXFhLWRldGFpbHMucGFnZS5zY3NzIn0= */");

/***/ }),

/***/ "./src/app/halaqat/halaqa-details/halaqa-details.page.ts":
/*!***************************************************************!*\
  !*** ./src/app/halaqat/halaqa-details/halaqa-details.page.ts ***!
  \***************************************************************/
/*! exports provided: HalaqaDetailsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HalaqaDetailsPage", function() { return HalaqaDetailsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var src_app_services_halaqa_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/halaqa.service */ "./src/app/services/halaqa.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");




let HalaqaDetailsPage = class HalaqaDetailsPage {
    constructor(activatedRoute, halaqaService) {
        this.activatedRoute = activatedRoute;
        this.halaqaService = halaqaService;
    }
    ngOnInit() {
        this.HalaqaId = this.activatedRoute.snapshot.paramMap.get('id');
        this.getHalaqaById(this.HalaqaId);
    }
    getHalaqaById(HalaqaId) {
        this.halaqaService.getHalaqaById(HalaqaId).subscribe(data => {
            if (data.code === '0000') {
                console.log(data.content);
                this.halaqaDetails = data.content[0];
            }
            else {
                console.log('error', data);
            }
        }, (err) => {
            console.log('error', err);
        });
    }
};
HalaqaDetailsPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"] },
    { type: src_app_services_halaqa_service__WEBPACK_IMPORTED_MODULE_2__["HalaqaService"] }
];
HalaqaDetailsPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-halaqa-details',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./halaqa-details.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/halaqat/halaqa-details/halaqa-details.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./halaqa-details.page.scss */ "./src/app/halaqat/halaqa-details/halaqa-details.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"],
        src_app_services_halaqa_service__WEBPACK_IMPORTED_MODULE_2__["HalaqaService"]])
], HalaqaDetailsPage);



/***/ })

}]);
//# sourceMappingURL=halaqa-details-halaqa-details-module-es2015.js.map