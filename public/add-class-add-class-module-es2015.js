(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["add-class-add-class-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/classes/add-class/add-class.page.html":
/*!*********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/classes/add-class/add-class.page.html ***!
  \*********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\r\n  <ion-toolbar>\r\n    <ion-title *ngIf='!EditClassInfo'>Add Class</ion-title>\r\n    <ion-title *ngIf='EditClassInfo'>Edit Class</ion-title>\r\n    <ion-buttons slot=\"start\">\r\n      <ion-menu-button autoHide=\"false\"></ion-menu-button>\r\n      <ion-back-button></ion-back-button>\r\n    </ion-buttons>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <ion-card>\r\n    <form  [formGroup]=\"ClassForm\">\r\n      <ion-item lines=\"inset\">\r\n        <ion-input  formControlName=\"Classname\" placeholder='name' required></ion-input>\r\n      </ion-item>\r\n      <div>\r\n        <ng-container *ngFor=\"let error of error_messages.Classname\">\r\n          <ion-text class=\"ion-padding\" \r\n                    color=\"danger\"\r\n                    *ngIf=\"ClassForm.get('Classname').hasError(error.type) && (ClassForm.get('Classname').dirty || ClassForm.get('Classname').touched)\">\r\n            {{ error.message }}\r\n          </ion-text>\r\n        </ng-container>\r\n      </div>\r\n    </form>\r\n    <ion-button  \r\n      shape=\"round\"\r\n      expand=\"block\" \r\n      *ngIf='!EditClassInfo' \r\n      (click)='addClass(ClassForm)'\r\n      [disabled]='!ClassForm.valid'>submit</ion-button>\r\n    <ion-button  \r\n      shape=\"round\"\r\n      expand=\"block\"  \r\n      *ngIf='EditClassInfo' \r\n      (click)='UpdateClass(ClassForm)' \r\n      [disabled]='!ClassForm.valid'>Update</ion-button> \r\n  </ion-card>\r\n</ion-content>");

/***/ }),

/***/ "./src/app/classes/add-class/add-class-routing.module.ts":
/*!***************************************************************!*\
  !*** ./src/app/classes/add-class/add-class-routing.module.ts ***!
  \***************************************************************/
/*! exports provided: AddClassPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddClassPageRoutingModule", function() { return AddClassPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _add_class_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./add-class.page */ "./src/app/classes/add-class/add-class.page.ts");




const routes = [
    {
        path: '',
        component: _add_class_page__WEBPACK_IMPORTED_MODULE_3__["AddClassPage"]
    }
];
let AddClassPageRoutingModule = class AddClassPageRoutingModule {
};
AddClassPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], AddClassPageRoutingModule);



/***/ }),

/***/ "./src/app/classes/add-class/add-class.module.ts":
/*!*******************************************************!*\
  !*** ./src/app/classes/add-class/add-class.module.ts ***!
  \*******************************************************/
/*! exports provided: AddClassPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddClassPageModule", function() { return AddClassPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _add_class_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./add-class-routing.module */ "./src/app/classes/add-class/add-class-routing.module.ts");
/* harmony import */ var _add_class_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./add-class.page */ "./src/app/classes/add-class/add-class.page.ts");
/* harmony import */ var src_app_shared_module_shared_module_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/shared-module/shared-module.module */ "./src/app/shared-module/shared-module.module.ts");








let AddClassPageModule = class AddClassPageModule {
};
AddClassPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _add_class_routing_module__WEBPACK_IMPORTED_MODULE_5__["AddClassPageRoutingModule"],
            src_app_shared_module_shared_module_module__WEBPACK_IMPORTED_MODULE_7__["SharedModuleModule"]
        ],
        declarations: [_add_class_page__WEBPACK_IMPORTED_MODULE_6__["AddClassPage"]]
    })
], AddClassPageModule);



/***/ }),

/***/ "./src/app/classes/add-class/add-class.page.scss":
/*!*******************************************************!*\
  !*** ./src/app/classes/add-class/add-class.page.scss ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NsYXNzZXMvYWRkLWNsYXNzL2FkZC1jbGFzcy5wYWdlLnNjc3MifQ== */");

/***/ }),

/***/ "./src/app/classes/add-class/add-class.page.ts":
/*!*****************************************************!*\
  !*** ./src/app/classes/add-class/add-class.page.ts ***!
  \*****************************************************/
/*! exports provided: AddClassPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddClassPage", function() { return AddClassPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var src_app_services_classes_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/classes.service */ "./src/app/services/classes.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _models_Classe_model__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../models/Classe.model */ "./src/app/classes/models/Classe.model.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");







let AddClassPage = class AddClassPage {
    constructor(formBuilder, router, classesService, activatedRoute) {
        this.formBuilder = formBuilder;
        this.router = router;
        this.classesService = classesService;
        this.activatedRoute = activatedRoute;
        this.ClassToEdit = new _models_Classe_model__WEBPACK_IMPORTED_MODULE_5__["ClasseModel"]();
        this.EditClassInfo = false;
        this.error_messages = {
            Classname: [{ type: 'required', message: 'Class name is required.' }],
        };
    }
    ngOnInit() {
        this.initForm();
        this.router.events
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["switchMap"])((navigation) => this.activatedRoute.params.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["map"])((params) => ({ navigation, params })))))
            .subscribe((result) => {
            if (result.navigation instanceof _angular_router__WEBPACK_IMPORTED_MODULE_4__["NavigationEnd"]) {
                const urlfragment = result.navigation.url.split('/')[2];
                if (urlfragment.toLowerCase().includes('add') &&
                    result.params.id !== undefined &&
                    result.params !== {}) {
                    this.InstitutId = result.params.id;
                    this.EditClassInfo = false;
                    this.initForm();
                }
                else if (urlfragment.toLowerCase().includes('edit') &&
                    result.params.id !== undefined &&
                    result.params !== {}) {
                    this.GetClassById(result.params.id);
                    this.classId = result.params.id;
                    this.EditClassInfo = true;
                }
                else {
                    console.log('error id undefined');
                }
            }
        });
    }
    GetClassById(ClassId) {
        console.log('params --->', ClassId);
        this.classesService.getClassById(ClassId).subscribe((data) => {
            if (data.code === '0000') {
                console.log('data content', data.content[0]);
                this.ClassToEdit = new _models_Classe_model__WEBPACK_IMPORTED_MODULE_5__["ClasseModel"](data.content[0]);
                console.log('this.ClassToEdit', this.ClassToEdit);
                this.InstitutId = this.ClassToEdit.institutId;
                this.initForm();
            }
            else {
                console.log('error', data);
            }
        }, (err) => {
            console.log('error', err);
        });
    }
    initForm() {
        this.ClassForm = this.formBuilder.group({
            Classname: [this.ClassToEdit.Classname, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
        });
    }
    addClass(ClassForm) {
        const classToAdd = { Classname: '', institutId: '' };
        classToAdd.Classname = ClassForm.value.Classname;
        classToAdd.institutId = this.InstitutId;
        console.log('add class', classToAdd);
        this.classesService.addClass(classToAdd).subscribe((data) => {
            if (data.code === '0000') {
                console.log('class added with success');
                this.GoBackToList();
            }
            else {
                console.log('error');
            }
        }, (err) => {
            console.log('error', err);
        });
    }
    UpdateClass(ClassForm) {
        console.log('ClassForm.value', ClassForm.value, this.classId);
        this.classesService.updateClass(this.classId, ClassForm.value).subscribe((data) => {
            if (data.code === '0000') {
                console.log('class updated with success');
                this.GoBackToList();
            }
            else {
                console.log('error update');
            }
        }, (err) => {
            console.log('error', err);
        });
    }
    GoBackToList() {
        console.log('---------> InstitutId', this.InstitutId);
        this.router.navigate(['/classes/classe-list/', this.InstitutId]);
    }
};
AddClassPage.ctorParameters = () => [
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
    { type: src_app_services_classes_service__WEBPACK_IMPORTED_MODULE_3__["ClassesService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"] }
];
AddClassPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-add-class',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./add-class.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/classes/add-class/add-class.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./add-class.page.scss */ "./src/app/classes/add-class/add-class.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
        _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"],
        src_app_services_classes_service__WEBPACK_IMPORTED_MODULE_3__["ClassesService"],
        _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"]])
], AddClassPage);



/***/ }),

/***/ "./src/app/classes/models/Classe.model.ts":
/*!************************************************!*\
  !*** ./src/app/classes/models/Classe.model.ts ***!
  \************************************************/
/*! exports provided: ClasseModel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ClasseModel", function() { return ClasseModel; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _sharedModels_sharedModel__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../sharedModels/sharedModel */ "./src/app/sharedModels/sharedModel.ts");


class ClasseModel extends _sharedModels_sharedModel__WEBPACK_IMPORTED_MODULE_1__["sharedModel"] {
    constructor(ClasseEntity) {
        ClasseEntity = ClasseEntity || {};
        super(ClasseEntity);
        this.Classname = ClasseEntity.Classname || '';
        this.institutId = ClasseEntity.institutId || '';
    }
}


/***/ })

}]);
//# sourceMappingURL=add-class-add-class-module-es2015.js.map