// ToDo: add control  in update and delete, id should exist

var institute = require('../models/Institutes');
var student = require('../models/students');
var classe = require('../models/classes');
var halaqa = require('../models/Halaqat');

var usercontroller = require('../controllers/UsersController');
// get all institues
exports.getAllInstitutes = function(req,res) {
    if(req.user.role != '' && req.user.role == 'admin')
    {
        institute.find({},function(err,data){
            if (err) {
                res.statusCode = 500;
                return res.json({ errorDescription: err.errors });
            }    // No results returned mean the object is not found
              if(data != null)
              {
                  if (data.length != 0)
                  {
                    var response = { 
                        code :'0000',
                        message:'all Institues list', 
                        content:data, 
                        inlineCount: data.length
                    };
                    res.json(response);
                  }else{
                    var response = { 
                        code :'0000',
                        message:'empty Institues list', 
                        content:data, 
                        inlineCount: data.length
                    };
                    res.json(response);
                  }
              }
            });
    }else if (req.user.role == 'cheick'){
      institute.find({"_id" : {"$in" : req.user.institutesids}},function(err,data){
        if (err) {
            res.statusCode = 500;
            return res.json({ errorDescription: err.errors });
        }    // No results returned mean the object is not found
          if(data != null)
          {
              if (data.length != 0)
              {
                var response = { 
                    code :'0000',
                    message:'all Institues list', 
                    content:data, 
                    inlineCount: data.length
                };
                res.json(response);
              }else{
                var response = { 
                    code :'0000',
                    message:'empty Institues list', 
                    content:data, 
                    inlineCount: data.length
                };
                res.json(response);
              }
          }
        });
    }else {
      var response = { 
        code :'0001',
        message:'No role provided for the current user'
    };
    res.json(response);
  }            
}

// add an institut
exports.addInstitute=function(req,res){
    var new_institut = new institute(req.body);
    new_institut.save(function(err, data){
        if (err) {
            res.statusCode = 500;
            return res.json({ errorDescription: err.errors });
        }    
          // No results returned mean the object is not found
          if(data != null)
          {
                var response = { 
                    code :'0000',
                    message:'Institue added with success', 
                    content: data
                };
                res.json(response);
         }
        });
}


exports.getInstituteById=function(req,res){
    institute.find({'_id':req.params.id},function(err,data){
        if (err) {
            console.log(err);
            res.statusCode = 500;
            return res.json({ errorDescription: err.errors });
        }    
          // No results returned mean the object is not found
          if(data != null)
          {
              if (data.length != 0)
            {   var response = { 
                code :'0000',
                message:'Institue by id', 
                content: data
            };
            console.log(response);
            res.json(response);
        }else {
                var response = { 
                    code :'0001',
                    message:`no institut found by this id :${req.params.id}`, 
                    content:data, 
                    inlineCount: data.length
                };
                 console.log(response);
                res.json(response);
              }
        }    
        });
}


exports.updateInstitute=function(req,res){
    institute.update({'_id':req.params.id}, req.body, function(err, data){
        if (err) {
            console.log(err);
            res.statusCode = 500;
            return res.json({ errorDescription: err.errors });
        }    
          if(data != null)
          {
                var response = { 
                    code :'0000',
                    message:'Institue updated with success'
                };
                console.log(response);
                res.json(response);
         }
    })
}


exports.deleteInstitute = function(req, res){
    institute.deleteOne({'_id':req.params.id}, function(err, data){
        if (err) {
            console.log(err);
            res.statusCode = 500;
            return res.json({ errorDescription: err.errors });
        }    
          if(data != null)
          {
                var response = { 
                    code :'0000',
                    message:'Institue deleted with success'
                };
                console.log(response);
                res.json(response);
         }
    })
}


exports.search = function(req, res){
  var response = []
  var async = require("async");
  var keyword = req.query.q;

  var query = { $or: [
            { Halaqaname: {  $regex: new RegExp(keyword), $options: 'i' } },
            { InstituteName: {  $regex: new RegExp(keyword), $options: 'i' }},
            { country: {  $regex: new RegExp(keyword), $options: 'i' }},
            { city : {  $regex: new RegExp(keyword), $options: 'i' }},
            { firstname: {  $regex: new RegExp(keyword), $options: 'i' } },
            { lastname: {  $regex: new RegExp(keyword), $options: 'i' } },
            { Classname: {  $regex: new RegExp(keyword), $options: 'i' } }
      ]};

    async.series([ 
     function(callback) { 
        halaqa.find(query,{'-_id':0,'__v':0},function(err, posts) {
          if (err) return res.status(500).json({error: "Internal Server Error"});
          if (req.user.role == 'cheick')
          {
            var CheickHalqats = posts.filter(halaqa => halaqa.CheikhId == req.user._id);
            callback(null, CheickHalqats);
          }else if(req.user.role == 'admin')
          {
            callback(null, posts);
          }
        }).limit(10);
      },
      function(callback) {
       institute.find(query,{'-_id':0,'__v':0},function(err, posts) {
          if (err) return res.status(500).json({error: "Internal Server Error"});
          if (req.user.role == 'cheick')
          {
            var CheickInstitutes = posts.filter(inst => req.user.institutesids.indexOf(inst._id) > -1);
            callback(null, CheickInstitutes);
          }else if(req.user.role == 'admin')
          {
            callback(null, posts);
          }
      }).limit(10);
       },
       function(callback) {
        classe.find(query,{'-_id':0,'__v':0},function(err, posts) {
          if (err) return res.status(500).json({error: "Internal Server Error"});
          if (req.user.role == 'cheick')
            {
              var CheickRelatedClasses = posts.filter(classObject => req.user.institutesids.indexOf(classObject.institutId) > -1);
              callback(null, CheickRelatedClasses);
            }else if(req.user.role == 'admin')
            {
              callback(null, posts);
            }
      }).limit(10);
      },
      function(callback) {
        student.find(query,{'-_id':0,'__v':0},function(err, posts) {
            if (err) return res.status(500).json({error: "Internal Server Error"});
            if (req.user.role == 'cheick')
            {
              var CheickStudents = posts.filter(student => student.createdBy == req.user._id);
              callback(null, CheickStudents);
            }else if(req.user.role == 'admin')
            {
              callback(null, posts);
            }
        }).limit(10);
    }
   ],
  function(err, results) {
  if(err){console.log(err)}
    else {
    res.status(200).json(results);
    console.log('resulta',results)  
  }
  });
  }


/*
getToken = function (headers) {
    console.log('inside the get token', headers);
    if (headers && headers.authorization) {
      var parted = headers.authorization.split(' ');
      console.log('token', parted);
      if (parted.length === 2) {
        return parted[1];
      } else {
        return null;
      }
    } else {
      return null;
    }
  };
  */