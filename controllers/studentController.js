// ToDo: add control

var student = require("../models/students");

// get all students
exports.getAllHalaqastudents = function(req, res) {
  student.find({halaqaId:req.params.id}, function(err, data) {
    if (err) {
      console.log("error", err);
      res.statusCode = 500;
      return res.json({ errorDescription: err.errors });
    } // No results returned mean the object is not found
    if (data != null) {
      if (data.length != 0) {
        var response = {
          code: "0000",
          message: "all students list",
          content: data,
          inlineCount: data.length
        };
        console.log(response);
        res.json(response);
      } else {
        var response = {
          code: "0000",
          message: "empty students list",
          content: data,
          inlineCount: data.length
        };
        console.log(response);
        res.json(response);
      }
    }
  });
};

exports.getStudentsByClass = function(req, res)
{
  var query =  { $and: [{ halaqaId: req.params.halaqaId },{ classId: req.params.classId }]};
  student.find(query, function(err, data) {
    if (err) {
      console.log("error", err);
      res.statusCode = 500;
      return res.json({ errorDescription: err.errors });
    } // No results returned mean the object is not found
    if (data != null) {
      if (data.length != 0) {
        var response = {
          code: "0000",
          message: "filtred students list",
          content: data,
          inlineCount: data.length
        };
        console.log(response);
        res.json(response);
      } else {
        var response = {
          code: "0000",
          message: "empty students list",
          content: data,
          inlineCount: data.length
        };
        console.log(response);
        res.json(response);
      }
    }
  });
}


// get student by id
exports.getStudentById = function(req, res) {
  student.find({ _id: req.params.id }, function(err, data) {
    if (err) {
      console.log("error", err);
      res.statusCode = 500;
      return res.json({ errorDescription: err.errors });
    }
    // No results returned mean the object is not found
    if (data != null) {
      if (data.length != 0) {
        var response = {
          code: "0000",
          message: "student by id",
          content: data
        };
        console.log(response);
        res.json(response);
      } else {
        var response = {
          code: "0001",
          message: `no student found by this id :${req.params.id}`,
          content: data,
          inlineCount: data.length
        };
        console.log(response);
        res.json(response);
      }
    }
  });
};

//add a student
exports.addStudent = function(req, res) {
  var new_student = new student(req.body);
  new_student.set("createdBy",req.user._id);
  new_student.save(function(err, data) {
    if (err) {
      console.log("error", err);
      res.statusCode = 500;
      return res.json({ errorDescription: err.errors });
    }
    // No results returned mean the object is not found
    if (data != null) {
      if (data.length != 0) {
        var response = {
          code: "0000",
          message: "Student added with success",
          content: data
        };
        console.log(response);
        res.json(response);
      }
    }
  });
};

// update a student
exports.updateStudent = function(req, res) {
  student.update({ _id: req.params.id }, req.body, function(err, data) {
    if (err) {
      console.log("error", err);
      res.statusCode = 500;
      return res.json({ errorDescription: err.errors });
    }
    // No results returned mean the object is not found
    if (data != null) {
      if (data.length != 0) {
        var response = {
          code: "0000",
          message: "Student updated with success"
        };
        console.log(response);
        res.json(response);
      }
    }
  });
};

// delete a student
exports.deleteStudent = function(req, res) {
  student.deleteOne({ _id: req.params.id }, function(err, data) {
    if (err) {
      console.log("error", err);
      res.statusCode = 500;
      return res.json({ errorDescription: err.errors });
    }
    // No results returned mean the object is not found
    if (data != null) {
      if (data.length != 0) {
        var response = {
          code: "0000",
          message: "Student deleted with success"
        };
        console.log(response);
        res.json(response);
      }
    }
  });
};
