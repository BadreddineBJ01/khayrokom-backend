// ToDo: add control
var jwt = require("jsonwebtoken");
var config = require("../config/DBConfig.json");
var UserModel = require("../models/users");
var passwordResetToken = require("../models/passwordResetToken");
// verify the user integrity
var auth = require("../services/auth");
var crypto = require('crypto');
var mailer = require('nodemailer');
var bcrypt = require('bcryptjs');


function createToken(user) {
  return jwt.sign({ id: user.id, email: user.email }, config.jwtSecret, {
    expiresIn: 86400 // 86400 expires in 24 hours
  });
}

exports.registerUser = (req, res) => {
  if (!req.body.email || !req.body.password) {
    var response = {
      code: "400",
      message: "You need to send email and password"
    };
    res.json(response);
  }
  UserModel.findOne({ email: req.body.email }, (err, user) => {
    if (err != null) {
      var response = {
        code: "400",
        message: err
      };
      return res.json(response);
    }

    if (user != null) {
      var response = {
        code: "400",
        message: "User with the current email already exists"
      };
      return res.json(response);
    }

    let newUser = UserModel(req.body);

    newUser.save((err, user) => {
      if (err) {
        var response = {
          code: "400",
          message: err
        };
        return res.json(response);
      }

      var response = {
        code: "0000",
        message: "User registred with success",
        content: user
      };
      return res.json(response);
    });
  });
};

exports.loginUser = (req, res) => {
  console.log("User request", req);
  if (!req.body.email || !req.body.password) {
    var response = {
      code: "400",
      message: "You need to send email and password"
    };
    return res.json(response);
  }

  UserModel.findOne({ email: req.body.email }, (err, user) => {
    if (err) {
      var response = {
        code: "400",
        message: err
      };
      return res.json(response);
    }

    if (!user) {
      var response = {
        code: "400",
        message: "The user does not exist"
      };
      return res.json(response);
    }
    if (req.body.password) {
      user.comparePassword(req.body.password, (err, isMatch) => {
        if (isMatch && !err) {
          var UserInfo = {
            firstname: user.firstname,
            lastname: user.lastname,
            email: user.email,
            phone: user.phone,
            role: user.role,
            userId: user._id
          };
          var response = {
            code: "0000",
            message: "email and password match correctly",
            content: { token: createToken(user), user: UserInfo }
          };
          console.log(response);
          return res.json(response);
        } else {
          var response = {
            code: "400",
            message: "The email and password don't match'"
          };
          console.log(response);
          return res.json(response);
        }
      });
    } else {
      var response = {
        code: "400",
        message: "Password is empty",
        content: { token: createToken(user), user: UserInfo }
      };
      console.log(response);
      return res.json(response);
    }
  });
};

// get all users
exports.getAllusers = function(req, res) {
  UserModel.find({}, function(err, data) {
    if (err) {
      console.log("error", err);
      res.statusCode = 500;
      return res.json({ errorDescription: err.errors });
    } // No results returned mean the object is not found
    if (data != null) {
      if (data.length != 0) {
        var response = {
          code: "0000",
          message: "all users list",
          content: data,
          inlineCount: data.length
        };
        console.log(response);
        return res.json(response);
      } else {
        var response = {
          code: "0000",
          message: "empty user list",
          content: data,
          inlineCount: data.length
        };
        console.log(response);
        return res.json(response);
      }
    }
  });
};

// get user by id
exports.getUserById = function(req, res) {
  UserModel.find({ _id: req.params.id }, function(err, data) {
    if (err) {
      console.log("error", err);
      res.statusCode = 500;
      return res.json({ errorDescription: err.errors });
    }
    // No results returned mean the object is not found
    if (data != null) {
      if (data.length != 0) {
        var response = {
          code: "0000",
          message: "user by id",
          content: data
        };
        console.log(response);
        return res.json(response);
      } else {
        var response = {
          code: "0001",
          message: `no user found by this id :${req.params.id}`,
          content: data,
          inlineCount: data.length
        };
        console.log(response);
        return res.json(response);
      }
    }
  });
};

//add a user
exports.addUser = function(req, res) {
  var new_user = new UserModel(req.body);
  new_user.save(function(err, data) {
    if (err) {
      console.log("error", err);
      res.statusCode = 500;
      return res.json({ errorDescription: err.errors });
    }
    // No results returned mean the object is not found
    if (data != null) {
      if (data.length != 0) {
        var response = {
          code: "0000",
          message: "User added with success",
          content: data
        };
        console.log(response);
        return res.json(response);
      }
    }
  });
};

// update a user
exports.updateUser = function(req, res) {
  UserModel.update({ _id: req.params.id }, req.body, function(err, data) {
    if (err) {
      console.log("error", err);
      res.statusCode = 500;
      return res.json({ errorDescription: err.errors });
    }
    // No results returned mean the object is not found
    if (data != null) {
      if (data.length != 0) {
        var response = {
          code: "0000",
          message: "User updated with success"
        };
        console.log(response);
        return res.json(response);
      }
    }
  });
};

// delete a user
exports.deleteUser = function(req, res) {
  UserModel.deleteOne({ _id: req.params.id }, function(err, data) {
    if (err) {
      console.log("error", err);
      res.statusCode = 500;
      return res.json({ errorDescription: err.errors });
    }
    // No results returned mean the object is not found
    if (data != null) {
      if (data.length != 0) {
        var response = {
          code: "0000",
          message: "Users deleted with success"
        };
        console.log(response);
        return res.json(response);
      }
    }
  });
};

 exports.ResetPassword = function(req, res) {
  if (!req.body.email) {
  return res
  .status(500)
  .json({ message: 'Email is required' });
  }
  const user =  UserModel.findOne({
  email:req.body.email
  },(err, user) => {
    if (err) {
      return res
  .status(409)
  .json({ message: 'Email does not exist, Please create an account if you do not have one' });
     }
    else if(!user)
    {
      return res
  .status(409)
  .json({ message: 'Email does not exist, Please create an account if you do not have one' });
    }else{
     var resettoken = new passwordResetToken(
       { _userId: user._id,
         resettoken: crypto.randomBytes(16).toString('hex') 
        });

     resettoken.save(function (err) {
     if (err) { return res.status(500).send({ msg: err.message }); }
     passwordResetToken.find({ _userId: user._id, 
      resettoken: 
      { $ne: resettoken.resettoken } }).deleteOne().exec();      
     res.status(200).json({ message: 'Reset Password successfully.' });
     var transporter = mailer.createTransport({
       service: 'Gmail',
       auth: {
         user: "iqraawairtaqi.khayrokom@gmail.com",
         pass: 'IQRAAiqraa123@*',
         Tls: {rejectUnauthorized: false}
       }
     });
     var mailOptions = {
     to: user.email,
     from: 'iqraawairtaqi.khayrokom@gmail.com',
     subject: 'Password Reset',
     text: 'You are receiving this because you have requested the reset of the password for your account.\n\n' +
     'Please click on the following link to complete the process:\n\n' +
     'https://khayrokombackendupdatedversion.herokuapp.com/#/auth/reset-password/resetpassword/'+ resettoken.resettoken + '\n\n' +
     'If you did not request this, please ignore this email and your password will remain unchanged.\n'
     }
     transporter.sendMail(mailOptions, (err, info) => {
     })
     })

    }
  });
  }


  exports.ValidPasswordToken = function(req, res) {
    if (!req.body.resettoken) {
    return res
    .status(500)
    .json({ message: 'Token is required' });
    }
    const user = passwordResetToken.findOne({
    resettoken: req.body.resettoken
    },(err,user) =>{
     if(err)
     {
        return res
        .status(409)
        .json({ message: 'Invalid URL' });
     }else if(user != null){
      UserModel.
      findOneAndUpdate({ _id: user._userId },{useFindAndModify: false})
      .then(() => {
      res.status(200).json({ message: 'Token verified successfully.' });
      }).catch((err) => {
      return res.status(500).send({ msg: err.message });
      });
     }
     else{
      return res
      .status(500)
      .json({ message: 'User not found'});
     }
    });
};

exports.NewPassword = function(req, res) {
  passwordResetToken.findOne({ resettoken: req.body.resettoken }, function (err, userToken, next) {
    if (!userToken) {
      return res
        .status(409)
        .json({ message: 'Token has expired' });
    }

    UserModel.findOne({
      _id: userToken._userId
    }, function (err, userEmail, next) {
      if (!userEmail) {
        return res
          .status(409)
          .json({ message: 'User does not exist' });
      }

      userEmail.password = req.body.confirmPassword;
      userEmail.save(function (err) {
        if (err) {
          return res
            .status(400)
            .json({ message: 'Password can not be resetted.' });
        } else {
          userToken.remove();
          return res
            .status(201)
            .json({ message: 'Password reset successfully' });
        }
      });
    });

  })
}

















