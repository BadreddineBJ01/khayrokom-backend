var classe = require("../models/classes");

// get all the classes
exports.getAllClasses = function(req, res) {
  classe.find({}, function(err, data) {
    if (err) {
      console.log("error", err);
      res.statusCode = 500;
      return res.json({ errorDescription: err.errors });
    } // No results returned mean the object is not found
    if (data != null) {
      if (data.length != 0) {
        var response = {
          code: "0000",
          message: "all class list",
          content: data,
          inlineCount: data.length
        };
        console.log(response);
        res.json(response);
      } else {
        var response = {
          code: "0000",
          message: "empty class list",
          content: data,
          inlineCount: data.length
        };
        console.log(response);
        res.json(response);
      }
    }
  });
};

exports.getAllInstitutClassesById = function(req, res) {
  classe.find({institutId: req.params.id}, function(err, data) {
    if (err) {
      console.log("error", err);
      res.statusCode = 500;
      return res.json({ errorDescription: err.errors });
    } // No results returned mean the object is not found
    if (data != null) {
      if (data.length != 0) {
        var response = {
          code: "0000",
          message: "Institut classes list",
          content: data,
          inlineCount: data.length
        };
        console.log(response);
        res.json(response);
      } else {
        var response = {
          code: "0000",
          message: "empty class list",
          content: data,
          inlineCount: data.length
        };
        console.log(response);
        res.json(response);
      }
    }
  });
};

// get class by id
exports.getClassbyId = function(req, res) {
  classe.find({ _id: req.params.id }, function(err, data) {
    if (err) {
      console.log("error", err);
      res.statusCode = 500;
      return res.json({ errorDescription: err.errors });
    } // No results returned mean the object is not found
    if (data != null) {
      if (data.length != 0) {
        var response = {
          code: "0000",
          message: "class by id",
          content: data
        };
        console.log(response);
        res.json(response);
      } else {
        var response = {
          code: "0001",
          message: `error could not find class by id ${req.params.id}`
        };
        console.log(response);
        res.json(response);
      }
    }
  });
};

// To Do : add the methods classbyInstitut, classBycheikId if needed

exports.addClasse = function(req, res) {
  var new_class = new classe(req.body);
  new_class.save(function(err, data) {
    if (err) {
      console.log("error", err);
      res.statusCode = 500;
      return res.json({ errorDescription: err.errors });
    }
    // No results returned mean the object is not found
    if (data != null) {
      if (data.length != 0) {
        var response = {
          code: "0000",
          message: "class added with succss",
          content: data
        };
        console.log(response);
        res.json(response);
      } else {
        var response = {
          code: "0001",
          message: "error adding the class"
        };
        console.log(response);
        res.json(response);
      }
    }
  });
};

// ToDo, add the control on the id.
exports.updateClasse = function(req, res) {
  classe.update({ _id: req.params.id }, req.body, function(err, data) {
    // No results returned mean the object is not found
    if (err) {
      console.log("error", err);
      res.statusCode = 500;
      return res.json({ errorDescription: err.errors });
    }
    if (data != null) {
      var response = {
        code: "0000",
        message: "class updated with success"
      };
      console.log(response);
      res.json(response);
    }
  });
};

// to do add the control on the id
exports.deleteclasse = function(req, res) {
  classe.deleteOne({ _id: req.params.id }, function(err, data) {
    if (err) {
      console.log("error", err);
      res.statusCode = 500;
      return res.json({ errorDescription: err.errors });
    }
    if (data != null) {
      var response = {
        code: "0000",
        message: "class deleted with success"
      };
      console.log(response);
      res.json(response);
    }
  });
};
