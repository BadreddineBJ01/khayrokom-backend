var achievement = require("../models/achievements");

// get all acheivements
exports.getAllStudentAchievement = function(req, res) {
  achievement.find({$query: {idstudent: req.params.id}}, function(err, data) {
    if (err) {
      console.log("error", err);
      res.statusCode = 500;
      return res.json({ errorDescription: err.errors });
    } // No results returned mean the object is not found
    if (data != null) {
      if (data.length != 0) {
        var response = {
          code: "0000",
          message: "all achievements list",
          content: data,
          inlineCount: data.length
        };
        console.log(response);
        res.json(response);
      } else {
        var response = {
          code: "0000",
          message: "empty achivements list",
          content: data,
          inlineCount: data.length
        };
        console.log(response);
        res.json(response);
      }
    }
  }).sort({ created_at : -1 });
};


exports.getAchivementByprogram = function(req, res)
{
  var keyWord = req.query.program; 
  var programFilter = {program: {  $regex: new RegExp(keyWord), $options: 'i' }};
  var query =  { $and: [ { idstudent: req.params.id }, programFilter ] };
  achievement.find({ $query: query}, function(err, data) {
    if (err) {
      console.log("error", err);
      res.statusCode = 500;
      return res.json({ errorDescription: err.errors });
    } // No results returned mean the object is not found
    if (data != null) {
      if (data.length != 0) {
        var response = {
          code: "0000",
          message: "filtred achievements list",
          content: data,
          inlineCount: data.length
        };
        console.log(response);
        res.json(response);
      } else {
        var response = {
          code: "0000",
          message: "empty achivements list",
          content: data,
          inlineCount: data.length
        };
        console.log(response);
        res.json(response);
      }
    }
  }).sort({ created_at : -1 });
}

// get achievement by id
exports.getAchievementById = function(req, res) {
  achievement.find({ _id: req.params.id }, function(err, data) {
    if (err) {
      console.log("error", err);
      res.statusCode = 500;
      return res.json({ errorDescription: err.errors });
    } // No results returned mean the object is not found
    if (data != null) {
      if (data.length != 0) {
        var response = {
          code: "0000",
          message: "achivement by id",
          content: data,
          inlineCount: data.length
        };
        console.log(response);
        res.json(response);
      } else {
        var response = {
          code: "0001",
          message: `no achivement found by this id :${req.params.id}`,
          content: data,
          inlineCount: data.length
        };
        console.log(response);
        res.json(response);
      }
    }
  });
};

// add an achievement
exports.addAchievement = function(req, res) {
  var new_achivement = new achievement(req.body);
  new_achivement.save(function(err, data) {
    if (err) {
      console.log("error", err);
      res.statusCode = 500;
      return res.json({ errorDescription: err.errors });
    } // No results returned mean the object is not found
    if (data != null) {
      if (data.length != 0) {
        var response = {
          code: "0000",
          message: "achivement added with success",
          content: data,
          inlineCount: data.length
        };
        console.log(response);
        res.json(response);
      } else {
        var response = {
          code: "0001",
          message: "error adding the achievemnt"
        };
        console.log(response);
        res.json(response);
      }
    }
  });
};

// update the achivement
// ToDo: test if the achievment to update exist or not in

exports.updateAchievement = function(req, res) {
  achievement.update({ _id: req.params.id }, req.body, function(err, data) {
    if (err) {
      console.log("error", err);
      res.statusCode = 500;
      return res.json({ errorDescription: err.errors });
    } // No results returned mean the object is not found
    if (data != null) {
      if (data.length != 0) {
        var response = {
          code: "0000",
          message: "achivement updated with success"
        };
        console.log(response);
        res.json(response);
      } else {
        var response = {
          code: "0001",
          message: "error updating the achievemnt"
        };
        console.log(response);
        res.json(response);
      }
    }
  });
};


// delete an achivement
// ToDo : test if the achivement exists
exports.deleteAchievement = function(req, res) {
  achievement.deleteOne({ _id: req.params.id }, function(err, data) {
    if (err) {
        console.log("error", err);
        res.statusCode = 500;
        return res.json({ errorDescription: err.errors });
      } // No results returned mean the object is not found
      if (data != null) {
          var response = {
            code: "0000",
            message: "achivement deleted with success",
          };
          console.log(response);
          res.json(response);
      }
    });
};
