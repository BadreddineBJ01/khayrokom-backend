var halaqa = require("../models/Halaqat");

// get all halaqat
exports.getAllHalaqat = function(req, res) {
  halaqa.find({}, function(err, data) {
    if (err) {
      console.log("error", err);
      res.statusCode = 500;
      return res.json({ errorDescription: err.errors });
    } // No results returned mean the object is not found
    if (data != null) {
      if (data.length != 0) {
        var response = {
          code: "0000",
          message: "all halaqat list",
          content: data,
          inlineCount: data.length
        };
        console.log(response);
        res.json(response);
      } else {
        var response = {
          code: "0000",
          message: "empty halaqa list",
          content: data,
          inlineCount: data.length
        };
        console.log(response);
        res.json(response);
      }
    }
  });
};

exports.getAllInstitutAndCheickHalaqatById = function(req, res) {
  halaqa.find({$and: [{InstitutId :req.params.InstitutId },{CheikhId : req.params.CheikhId}]}, function(err, data) {
    if (err) {
      console.log("error", err);
      res.statusCode = 500;
      return res.json({ errorDescription: err.errors });
    } // No results returned mean the object is not found
    if (data != null) {
      if (data.length != 0) {
        var response = {
          code: "0000",
          message: "Institut Halaqats list",
          content: data,
          inlineCount: data.length
        };
        console.log(response);
        res.json(response);
      } else {
        var response = {
          code: "0000",
          message: "empty Halaqats list",
          content: data,
          inlineCount: data.length
        };
        console.log(response);
        res.json(response);
      }
    }
  });
};

// get halaqa by id
exports.gethalaqabyId = function(req, res) {
  halaqa.find({ _id: req.params.id }, function(err, data) {
    if (err) {
      console.log("error", err);
      res.statusCode = 500;
      return res.json({ errorDescription: err.errors });
    } // No results returned mean the object is not found
    if (data != null) {
      if (data.length != 0) {
        var response = {
          code: "0000",
          message: "halaqa by id",
          content: data
        };
        console.log(response);
        res.json(response);
      } else {
        var response = {
          code: "0001",
          message: `error could not find halaqa by id ${req.params.id}`
        };
        console.log(response);
        res.json(response);
      }
    }
  });
};


exports.addHalaqa = function(req, res) {
  var new_halaqa = new halaqa(req.body);
  new_halaqa.save(function(err, data) {
    if (err) {
      console.log("error", err);
      res.statusCode = 500;
      return res.json({ errorDescription: err.errors });
    }
    // No results returned mean the object is not found
    if (data != null) {
      if (data.length != 0) {
        var response = {
          code: "0000",
          message: "halaqa added with succss",
          content: data
        };
        console.log(response);
        res.json(response);
      } else {
        var response = {
          code: "0001",
          message: "error adding the halaqa"
        };
        console.log(response);
        res.json(response);
      }
    }
  });
};

// ToDo, add the control on the id.
exports.updateHalaqa = function(req, res) {
  halaqa.update({ _id: req.params.id }, req.body, function(err, data) {
    // No results returned mean the object is not found
    if (err) {
      console.log("error", err);
      res.statusCode = 500;
      return res.json({ errorDescription: err.errors });
    }
    if (data != null) {
      var response = {
        code: "0000",
        message: "halaqa updated with success"
      };
      console.log(response);
      res.json(response);
    }
  });
};

// to do add the control on the id if it exists
exports.deleteHalaqa = function(req, res) {
  halaqa.deleteOne({ _id: req.params.id }, function(err, data) {
    if (err) {
      console.log("error", err);
      res.statusCode = 500;
      return res.json({ errorDescription: err.errors });
    }
    if (data != null) {
      var response = {
        code: "0000",
        message: "halaqa deleted with success"
      };
      console.log(response);
      res.json(response);
    }
  });
};
