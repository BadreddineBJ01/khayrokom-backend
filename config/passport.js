
var JwtStrategy = require('passport-jwt').Strategy,
ExtractJwt = require('passport-jwt').ExtractJwt;

// load up the user model
var User = require('../models/users');

// get db config file
var config = require('../config/DBConfig.json'); 


// passwport function for intercepting if the user is authorized or not
// if true, get the current, connected, authorized user id and data.
module.exports = function(passport) {
    
var opts = {};
opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
opts.secretOrKey = config.jwtSecret;
passport.use(new JwtStrategy(opts, function(jwt_payload, done) {
User.findOne({_id: jwt_payload.id}, function(err, user) {
      if (err) {
          return done(err, false);
      }
      if (user) {
          done(null, user);
      } else {
          done(null, false);
      }
  });
}));
};

 // "database":"mongodb+srv://khayrokom:khayrokom@cluster0-krqsb.mongodb.net/test?retryWrites=true&w=majority",
